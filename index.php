<?php


include_once(dirname(__FILE__) . '/inc/common.php');

include_once(dirname(__FILE__) . '/inc/icdb.php');
include_once(dirname(__FILE__) . '/inc/functions.php');


$wpdb = null;
$ready = false;
if (file_exists(dirname(__FILE__) . '/inc/config.php')) {
    include_once(dirname(__FILE__) . '/inc/config.php');
    try {
        $wpdb = new ICDB(UAP_DB_HOST, UAP_DB_HOST_PORT, UAP_DB_NAME, UAP_DB_USER, UAP_DB_PASSWORD, UAP_TABLE_PREFIX);
        create_tables();
      
        get_options();
        if (!empty($options['login']) && !empty($options['password']) && !empty($options['url']))
            $ready = true;
    } catch (Exception $e) {
        //die($e->getMessage());
    }
} 

if (!$ready) {
    //header('Location: ' . admin_url('install.php'));
    exit;
}
$is_logged = false;
$session_id = '';

if (isset($_SESSION['is_logged_in'])) {
    global $wpdb;
    /*if (isset($_COOKIE['uap-auth'])) {
        $session_id = preg_replace('/[^a-zA-Z0-9]/', '', $_COOKIE['uap-auth']);
        $session_details = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "sessions WHERE session_id = '" . $session_id . "' AND registered + valid_period > '" . time() . "'");
        if ($session_details) {
            $wpdb->query("UPDATE " . $wpdb->prefix . "sessions SET registered = '" . time() . "', ip = '" . $_SERVER['REMOTE_ADDR'] . "' WHERE session_id = '" . $session_id . "'");
            $is_logged = true;
        }
    }*/
    $social_id = $_SESSION['social_id'];
    $social_type = $_SESSION['social_type'];
    $q = $wpdb->get_row("SELECT * from users WHERE social_id='" . $social_id . "' AND social_type='" . $social_type . "' AND status='1'");
    $_SESSION['user_id'] = $q->id;
    $userinfo = getSocialUserInfo($_SESSION['user_id']);
    $is_logged = true;
}
 

if ($is_logged === false) {
    // echo "is_logged no";die;

    header('Location: ' . admin_url('login.php'));
    exit;
}

function getSocialUserInfo($user_id) {
    $wpdb = new ICDB(DB_HOST, DB_HOST_PORT, DB_NAME, DB_USER, DB_PASSWORD, '');
    $user_check = $wpdb->get_row("SELECT * FROM users WHERE id = '" . $user_id . "' AND status='1'");
    if (count($user_check) > 0)
        return $user_check;
    else
        return array();
}

include_once(dirname(__FILE__) . '/inc/plugins.php');
$page = array(
    'slug' => 'dashboard',
    'page-title' => 'Dashboard'
);
do_action('init');
do_action('admin_init');

if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
    $global_message .= $_SESSION['message'];
    $_SESSION['message'] = '';
}
do_action('admin_menu');
if (isset($_REQUEST['page'])) {
    foreach ($menu as $slug => $item) {
        if (array_key_exists('submenu', $item)) {
            $found = false;
            foreach ($item['submenu'] as $submenu_slug => $submenu_item) {
                if ($_REQUEST['page'] == $submenu_slug) {
                    $page = $submenu_item;
                    $page['slug'] = $submenu_slug;
                    $page['parent'] = $slug;
                    $found = true;
                    break;
                }
            }
            if ($found)
                break;
        } else if ($_REQUEST['page'] == $slug) {
            $page = $item;
            $page['slug'] = $slug;
            break;
        }
    }
}

do_action('admin_enqueue_scripts');
include_once(dirname(__FILE__) . '/inc/header.php');
//echo '<pre>';
//print_r($_SESSION);
//echo '</pre>';
?>
<!--<div class="page-title">
    <div class="title_left">
        <h3><?php echo $page['page-title']; ?></h3>
    </div>
</div>-->

<?php
if ($page['slug'] == 'dashboard') {
    if ($writeable) {
        ?>

		
<main id="dashboard">
    <div class="container">
        
        <div class="row">

                <div align="center" class="page-title">
				<img src="//suite.social/images/profile/guy.jpg" width="80px" alt="Profile Image"> 
                    <h4 style="color:#8ec657"><strong>Resell Social Suite Platforms!</strong></h4>
					<h4><i>"Hi, I'm Mr Grower, your Virtual Reseller Manager. I will help you resell Social Suite Platforms to your clients like Social Marketer, Social Promotions, Social Messenger, Social Reviews, Social Hub and more so you can grow your income."</i></h4>
					<a href="//suite.social/reseller" target="_blank" class="btn btn-success"> BUY CODES <i class="fa fa-money"></i></a>
					<!--<a href="admin/login.php?logout=true" class="btn btn-success"> LOGOUT <i class="fa fa-hand-o-right"></i></a>-->
					<a class="btn btn-gray" data-modal-id="how"> HOW IT WORKS! <i class="fa fa-question-circle"></i></a>
                </div>

        </div>

        <br><br>

        <div class="row">
		
            <div class="col-sm-4 text-center">
                <div class="select-options">
                    <div class="select-items">
                        <i id="suite" class="fa fa-bullhorn fa-5x"></i>
                        <h3>1. Create Campaigns</h3>
                        <p>If you would like to create a new reseller campaign then choose this option. </p>
                    </div>
					<a href="?page=codeshop-add-campaign" class="btn btn-gray btn-block"> SELECT <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>		

            <div class="col-sm-4 text-center">
                <div class="select-options">
                    <div class="select-items">
                        <i id="suite" class="fa fa-users fa-5x"></i>
                        <h3>2. View Campaigns </h3>
                        <p>If you would like to view all your running reseller campaigns then choose this option. </p>
                    </div>
                    <a href="?page=codeshop-campaigns" class="btn btn-gray btn-block"> SELECT <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>
			
            <div class="col-sm-4 text-center">
                <div class="select-options">
                    <div class="select-items">
                        <i id="suite" class="fa fa-ticket fa-5x"></i>
                        <h3>3. View or Add Codes</h3>
                        <p>If you would like to view or upload or view your purchased codes then choose this option.</p>
                    </div>
                    <a href="?page=codeshop-codes" class="btn btn-gray btn-block"> SELECT <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>			
			
            <div class="col-sm-4 text-center">
                <div class="select-options">
                    <div class="select-items">
                        <i id="suite" class="fa fa-cog fa-5x"></i>
                        <h3>4. Edit Settings</h3>
                        <p>If you would like to edit or view your payment settings etc then choose this option.</p>
                    </div>
                    <a href="?page=codeshop" class="btn btn-gray btn-block"> SELECT <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>	
			
            <div class="col-sm-4 text-center">
                <div class="select-options">
                    <div class="select-items">
                        <i id="suite" class="fa fa-file-code-o fa-5x"></i>
                        <h3>5. Embed on Site </h3>
                        <p>If you would like to embed the payment form on your site then choose this option.</p>
                    </div>
                    <a href="?page=codeshop-using" class="btn btn-gray btn-block"> SELECT <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>			

            <div class="col-sm-4 text-center">
                <div class="select-options">
                    <div class="select-items">
                        <i id="suite" class="fa fa-money fa-5x"></i>
                        <h3>6. View Transactions</h3>
                        <p>If you would like to view all your transactions then choose this option.</p>
                    </div>
                    <a href="?page=codeshop-transactions" class="btn btn-gray btn-block"> SELECT <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>					
					
        </div>
    </div>
</main>

<!-- ********** HOW ********** -->

<div id="how" class="modal-box">  
  
  <header>
    <a href="#" class="js-modal-close close">X</a>
    <h3>HOW IT WORKS</h3>
  </header>
  
  <div class="modal-body">
	<p>1. Login as a reseller and buy discounted campaign codes (this allows clients to use the platforms for 2 weeks at a time).</p>
	<p>2. Resell the codes to clients (you get 50-70% off usual cost so you can resell at any price you want).</p>
	<p>3. Your clients can enter the campaign code in the platform to activate their campaign so they can have full access.</p>
	<h3>HOW MUCH CAN YOU EARN?</h3>
	<p>For Social Suite Pro, campaign codes cost £30 each for resellers (70% off usual price) and last 2 weeks.</p>
	<!--<p><b>Resell Price:</b> £50 x <b>25 Clients:</b> = £1,250 - <b>Your Profit:</b> £937.50</p>
	<p><b>Resell Price:</b> £50 x <b>50 Clients:</b> = £2,500 - <b>Your Profit:</b> £1,875</p>
	<p><b>Resell Price:</b> £50 x <b>100 Clients:</b> = £5,000 - <b>Your Profit:</b> £3,750</p>-->
	
	<p><b>Resell Price:</b> £70 Margin x <b>25 Clients:</b> = </b> £1,750 Profit</p>
	<p><b>Resell Price:</b> £70 Margin x <b>50 Clients:</b> = </b> £3,500 Profit</p>
	<p><b>Resell Price:</b> £70 Margin x <b>100 Clients:</b> = </b> £7,000 Profit</p>	
	
	<h3>WHERE YOU CAN PROMOTE?</h3>
	<p>1. Your website, blog or social profiles/groups.</p>
	<p>2. Your email, SMS, WhatsApp or messenger contacts.</p>
	<p>3. Industry events (distribute cards).</p>	
  </div>
  
  <footer>
    <a href="//suite.social/reseller/" target="_blank" class="btn btn-success btn-block"> SEE WHAT YOU CAN SELL! <i class="fa fa-arrow-right"></i></a>
  </footer>  
  
</div>		
		
        <?php
    }
} else {
    if (!empty($page['function'])) {
        call_user_func_array($page['function'], array());
    }
}
include_once(dirname(__FILE__) . '/inc/footer.php');
?>

