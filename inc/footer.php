<?php
if (!defined('UAP_CORE')) die('What are you doing here?');
?>
		<div align="center" id="footer">	
			
			<!--  Copyright Line -->
			<div class="copy">&copy; <?php echo date('Y'); ?> - <a href="//suite.social">Social Suite</a> - All Rights Reserved. <a href="//suite.social/contact">Contact</a> I <a href="//suite.social/terms">Terms & Conditions</a> I <a href="//suite.social/privacy">Privacy Policy</a></div>
			<!--  End Copyright Line -->
	
		</div>

<!-- Contents End here  //.-->

<script>

window.opener.location.replace('index.php');
window.close();

</script>

	            <script type="text/javascript" src="//suite.social/assets/javascript/iframeResizer.min.js"></script> 
                <script type="text/javascript">
                    function popitup(url) {
                        var title = 'Popup Login';
                        var w = 800;
                        var h = 600;
                        var left = (screen.width / 2) - (w / 2);
                        var top = (screen.height / 2) - (h / 2);
                        newwindow = window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
                        if (window.focus) {
                            newwindow.focus()
                        }
                        return false;
                    }	

			/*
			 * If you do not understand what the code below does, then please just use the
			 * following call in your own code.
			 *
			 *   iFrameResize({log:true});
			 *
			 * Once you have it working, set the log option to false.
			 */

			iFrameResize({
				log                     : false,                  // Enable console logging
				enablePublicMethods     : true,                  // Enable methods within iframe hosted page
				enableInPageLinks       : true,
				resizedCallback         : function(messageData){ // Callback fn when resize is received
					$('p#callback').html(
						'<b>Frame ID:</b> '    + messageData.iframe.id +
						' <b>Height:</b> '     + messageData.height +
						' <b>Width:</b> '      + messageData.width +
						' <b>Event type:</b> ' + messageData.type
					);
				},
				messageCallback         : function(messageData){ // Callback fn when message is received
					$('p#callback').html(
						'<b>Frame ID:</b> '    + messageData.iframe.id +
						' <b>Message:</b> '    + messageData.message
					);
					alert(messageData.message + ' (' + messageData.iframe.id + ')' );
				},
				closedCallback         : function(id){ // Callback fn when iFrame is closed
					$('p#callback').html(
						'<b>IFrame (</b>'    + id +
						'<b>) removed from page.</b>'
					);
				}
			});
			
		jQuery(".updated, .error").each(function(){
			if (jQuery(this).hasClass("error")) jQuery("#global-message-container").append("<div class='global-message global-message-danger'>"+jQuery(this).html()+"</div>");
			else jQuery("#global-message-container").append("<div class='global-message global-message-success'>"+jQuery(this).html()+"</div>");
			jQuery(this).remove();
		});
		var adjust_content_panel = function() {
			var content_height = jQuery(window).innerHeight() - jQuery("#footer-panel").innerHeight() - jQuery("#top-panel").innerHeight() - 2;
			if (content_height <= 0) content_height = 320;
			jQuery("#content-panel").css({"min-height" : content_height+"px"});
		};
		jQuery("body").show();
		adjust_content_panel();
		jQuery(window).resize(function(){adjust_content_panel();});
					
                </script>

        <!-- JS -->
	<script src="js/jquery-ui.min.js"></script>
	<script src="js/color-picker.min.js"></script>
	<script>var thickboxL10n = {"next":"Next >","prev":"< Prev","image":"Image","of":"of","close":"Close","noiframes":"This feature requires inline frames. You have iframes disabled or your browser does not support them.","loadingAnimation":"images\/loadingAnimation.gif"};</script>
	<script src="js/thickbox.js"></script>
	<script src="js/custom.js"></script>
	<script src="js/admin.js"></script>
	<script>var ajax_handler = "<?php echo admin_url('ajax.php'); ?>";</script>
	
        <script src="//suite.social/assets/javascript/bootstrap.min.js"></script>
        <script src="//suite.social/assets/javascript/moment.js"></script>
        <script src="//suite.social/assets/javascript/bootstrap-datetimepicker.min.js"></script>
        <script src="//suite.social/assets/javascript/plugins.js"></script>
        <script src="//suite.social/assets/javascript/main.js"></script>
        <script src="//suite.social/assets/javascript/add.js"></script>

    </body>
</html>
