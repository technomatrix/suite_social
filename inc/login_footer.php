<?php
if (!defined('UAP_CORE'))
    die('What are you doing here?');
?>
<div align="center" id="footer">	

    <!--  Copyright Line -->
    <div class="copy">&copy; <?php echo date('Y'); ?> - <a href="//suite.social">Social Suite</a> - All Rights Reserved. <a href="//suite.social/contact">Contact</a> I <a href="//suite.social/terms">Terms & Conditions</a> I <a href="//suite.social/privacy">Privacy Policy</a></div>
    <!--  End Copyright Line -->

</div>

<!-- Contents End here  //.-->

<script>

window.opener.location.replace('index.php');
window.close();

</script>

<script type="text/javascript">
    function popuplogin(url)
    {
        var w = 800;
        var h = 600;
        var title = 'Social login';
        var left = (screen.width / 2) - (w / 2);
        var top = (screen.height / 2) - (h / 2);
        window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    }
	
	
</script>

<!-- JS -->
<script src="js/login.js"></script>
<script>var login_handler = "<?php echo admin_url('login.php'); ?>";</script>
<script src="//suite.social/src/js/bootstrap.min.js"></script>
<script src="//suite.social/src/js/moment.js"></script>
<script src="//suite.social/src/js/bootstrap-datetimepicker.min.js"></script>
<script src="//suite.social/src/js/plugins.js"></script>
<script src="//suite.social/src/js/main.js"></script>
<script src="//suite.social/src/js/add.js"></script>

</body>
</html>
