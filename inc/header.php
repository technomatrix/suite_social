<?php
if (!defined('UAP_CORE')) die('What are you doing here?');
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <!--<html manifest="cache.appcache" class="no-js"> <!--<![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>

    <!-- Title -->
    <title>Social Suite - All-In-One Social Media Automation</title>

    <!-- Meta Data -->
    <meta name="title" content="Social Suite - All-In-One Social Media Automation">
    <meta name="description" content="All-In-One Social Media Management, Marketing and Monitoring Platform -  GROW your traffic, customers and sales 24-7, 365 days a year!">
    <meta name="keywords" content="Blog Management, Blog Marketing, Facebook Management, Facebook Marketing, Flickr Management, Flickr Marketing, Google+ Management, Google+ Marketing, Instagram Management, Instagram Marketing, Linkedin Management, Linkedin Marketing, Periscope Management, Periscope Marketing, Pinterest Management, Pinterest Marketing, Reddit Management, Reddit Marketing, Snapchat Management, Snapchat Marketing, Social Media Automation, Social Media Bot, Social Media Dashboard, Social Media Groups, Social Media Hub, Social Media Management, Social Media Manager, Social Media Marketer, Social Media Marketing, Social Media Monitoring, Social Media Poster, Social Media Promotion, Social Media Publisher, Social Media Publishing, Social Media Reports, Social Media Scheduler, Social Media Stream, Social Media Training, Social Media Wall, Soundcloud Management, Soundcloud Marketing, StumbleUpon Management, StumbleUpon Marketing, Tumblr Management, Tumblr Marketing, Twitter Management, Twitter Marketing, Vimeo Management, Vimeo Marketing, Vk Management, Vk Marketing, WhatsApp Management, WhatsApp Marketing, Wordpress Management, Wordpress Marketing, XING Management, XING Marketing, YouTube Management, YouTube Marketing">
    <meta name="robots" content="index, follow">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="language" content="English">
    <meta name="revisit-after" content="14 days">
    <meta name="author" content="Suite.social">	
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />	
		
	<!-- Google Plus -->
	<!-- Update your html tag to include the itemscope and itemtype attributes. -->
	<!-- html itemscope itemtype="//schema.org/{CONTENT_TYPE}" -->
	<meta itemprop="name" content="Social Suite - All-In-One Social Media Automation">
	<meta itemprop="description" content="All-In-One Social Media Management, Marketing and Monitoring Platform -  GROW your traffic, customers and sales 24-7, 365 days a year!">
	<meta itemprop="image" content="//suite.social/images/thumb/suite.jpg">
	
	<!-- Twitter -->
	<meta name="twitter:card" content="Social Suite - All-In-One Social Media Automation">
	<meta name="twitter:site" content="@socialgrower">
	<meta name="twitter:title" content="Social Suite - All-In-One Social Media Automation">
	<meta name="twitter:description" content="All-In-One Social Media Management, Marketing and Monitoring Platform -  GROW your traffic, customers and sales 24-7, 365 days a year!">
	<meta name="twitter:creator" content="@socialgrower">
	<meta name="twitter:image:src" content="//suite.social/images/thumb/suite.jpg">
	<meta name="twitter:player" content="">
	
	<!-- Open Graph General (Facebook & Pinterest) -->
	<meta property="og:url" content="//suite.social">
	<meta property="og:title" content="Social Suite - All-In-One Social Media Automation">
	<meta property="og:description" content="All-In-One Social Media Management, Marketing and Monitoring Platform -  GROW your traffic, customers and sales 24-7, 365 days a year!">
	<meta property="og:site_name" content="Social Suite">
	<meta property="og:image" content="//suite.social/images/thumb/suite.jpg">
	<meta property="fb:admins" content="126878864054794">
	<meta property="fb:app_id" content="1382960475264672">
	<meta property="og:type" content="product">
	<meta property="og:locale" content="en_UK">

	<!-- Open Graph Article (Facebook & Pinterest) -->
	<meta property="article:author" content="126878864054794">
	<meta property="article:section" content="Marketing">
	<meta property="article:tag" content="Marketing">		
	
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="//suite.social/images/favicon/favicon.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="//suite.social/images/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="//suite.social/images/favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="144x144" href="//suite.social/images/favicon/apple-touch-icon.png">
	
    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
	<!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">-->
    <link rel="stylesheet" href="//suite.social/src/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="//suite.social/src/css/icon.css">	
	
    <!-- CSS -->
    <link rel="stylesheet" href="//suite.social/src/css/bootstrap.min.css">
    <link rel="stylesheet" href="//suite.social/src/css/bootstrap-datetimepicker.min.css">	
    <link rel="stylesheet" href="//suite.social/src/css/normalize.css">
	<link rel="stylesheet" href="//suite.social/src/css/reseller.css">
    <link rel="stylesheet" href="//suite.social/src/css/responsive.css">	
	<link rel="stylesheet" href="//suite.social/src/css/social-buttons.css">
	
	<link href="css/thickbox.css" rel="stylesheet">	

    <!-- JQuery -->
    <script src="//suite.social/src/js/jquery.min.js"></script>
	<script type="text/javascript" src="//suite.social/src/js/iframeResizer.min.js"></script> 
    <script>window.jQuery || document.write('<script src="//suite.social/src/js/jquery-1.11.2.min.js"><\/script>')</script>

    <!-- Prefixing JS -->
    <script src="//suite.social/src/js/modernizr-2.8.3.min.js"></script>
    <script src="//suite.social/src/js/html5-3.6-respond-1.4.2.min.js"></script>

<style type="text/css">
html {
	position: relative;
	min-height: 100%;
}	

body {
	margin: 0 0 30px; /* Height of the footer */
	overflow-x: hidden;
}	

h3 {
    color: #8ec657;
}

#footer {
	position: absolute;
	bottom: 0;
	width: 100%;
	height: 30px;
    color: #fff;
	text-align: center;
    overflow: hidden; 	
}

.col-md-3 {
    padding-right: 0px; 
    padding-left: 0px;
}

col-md-12 {
    padding-right: 0px; 
    padding-left: 0px;
}

input, textarea, select {
    font-size: 18px;
    color: black;
}

.well {
    background-color: #262626;
    margin-top: 30px;
    border: 1px solid #616161;
}

.panel-heading {
    background-color: #484848;
    background-image: -moz-linear-gradient(top, #4b4b4b, #434343);
    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#4b4b4b), to(#434343));
    background-image: -webkit-linear-gradient(top, #4b4b4b, #434343);
    background-image: -o-linear-gradient(top, #4b4b4b, #434343);
    background-image: linear-gradient(to bottom, #4b4b4b, #434343);
    background-repeat: repeat-x;
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff4b4b4b', endColorstr='#ff434343', GradientType=0);
    box-shadow: 0 1px 0 0 #000000, 0 5px 4px -4px #191919;
    -moz-box-shadow: 0 1px 0 0 #000000, 0 5px 4px -4px #191919;
    -webkit-box-shadow: 0 1px 0 0 #000000, 0 5px 4px -4px #191919;
    border-bottom: 1px solid #616161;
    text-shadow: 0 1px 0 none;
    height: 35px;
    line-height: 35px;
    position: relative;
    padding: 0 15px 0 15px;
}

.panel-success {
    border-color: #616161;
    margin: 20px;
    box-shadow: 0 1px 0 0 #000000, 0 5px 4px -4px #191919;
    -moz-box-shadow: 0 1px 0 0 #000000, 0 5px 4px -4px #191919;
    -webkit-box-shadow: 0 1px 0 0 #000000, 0 5px 4px -4px #191919;
}

.panel-success>.panel-heading {
    color: #fff;
    border-color: #616161;
}

.table-bordered {
    border: 1px solid #616161;
}

.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
    border-bottom: 1px solid #616161;
}

.table-bordered>tbody>tr>td {
    border: 1px solid #616161;
}

.row {
    margin-right: auto;
    margin-left: auto;
}

.thumbnail {
    display: inline;
    background-color: #1f1f1f;
    border: 0px solid #ddd;
}

.modal-body {
overflow-x: hidden;
}

.panel-title {
padding-top: 10px;
}

.modal-content {
    background-color: #1f1f1f;
}

.modal-header {
    border-bottom: 1px solid #616161;
}

.modal-footer {
    border-top: 1px solid #616161;
}

.close {
    color: #ccc;
}

.modal.in .modal-dialog {
    width: 95%;
}

div.select-items {
     height: 300px;
}

.wrap {
    padding-left: 50px;
	padding-right: 50px;
}

.widefat {
    color: #000;
    width: 100%;
}

td, th {
    padding: 15px;
}

em {
    color: #999;
}

pre {
    padding: 5px;
    margin: 10px 0 10px;
}

</style>

		<script type="text/javascript">
			/*
			 * If you do not understand what the code below does, then please just use the
			 * following call in your own code.
			 *
			 *   iFrameResize({log:true});
			 *
			 * Once you have it working, set the log option to false.
			 */

			iFrameResize({
				log                     : true,                  // Enable console logging
				enablePublicMethods     : true,                  // Enable methods within iframe hosted page
				enableInPageLinks       : true,
				resizedCallback         : function(messageData){ // Callback fn when resize is received
					$('p#callback').html(
						'<b>Frame ID:</b> '    + messageData.iframe.id +
						' <b>Height:</b> '     + messageData.height +
						' <b>Width:</b> '      + messageData.width +
						' <b>Event type:</b> ' + messageData.type
					);
				},
				messageCallback         : function(messageData){ // Callback fn when message is received
					$('p#callback').html(
						'<b>Frame ID:</b> '    + messageData.iframe.id +
						' <b>Message:</b> '    + messageData.message
					);
					alert(messageData.message + ' (' + messageData.iframe.id + ')' );
				},
				closedCallback         : function(id){ // Callback fn when iFrame is closed
					$('p#callback').html(
						'<b>IFrame (</b>'    + id +
						'<b>) removed from page.</b>'
					);
				}
			});		
		
		
			//MDN PolyFil for IE8 (This is not needed if you use the jQuery version)
			if (!Array.prototype.forEach){
				Array.prototype.forEach = function(fun /*, thisArg */){
				"use strict";
				if (this === void 0 || this === null || typeof fun !== "function") throw new TypeError();
				
				var
				t = Object(this),
				len = t.length >>> 0,
				thisArg = arguments.length >= 2 ? arguments[1] : void 0;

				for (var i = 0; i < len; i++)
				if (i in t)
					fun.call(thisArg, t[i], i, t);
				};
			}

$(function(){

var appendthis =  ("<div class='modal-overlay js-modal-close'></div>");

  $('a[data-modal-id]').click(function(e) {
    e.preventDefault();
    $("body").append(appendthis);
    $(".modal-overlay").fadeTo(500, 0.7);
    //$(".js-modalbox").fadeIn(500);
    var modalBox = $(this).attr('data-modal-id');
    $('#'+modalBox).fadeIn($(this).data());
  });  
  
  
$(".js-modal-close, .modal-overlay").click(function() {
  $(".modal-box, .modal-overlay").fadeOut(500, function() {
    $(".modal-overlay").remove();
  });
});
 
$(window).resize();
 
});			
			
		</script>	
	
</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="//browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- Content Start here  -->

<header id="header">

    <nav class="navbar navbar-default">

        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="//suite.social/reseller/"><img src="//suite.social/images/logo/reseller.png" height="35px" alt="Logo"></a>

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-collapse">

                <ul class="nav navbar-nav navbar-right navbar-top">
                                      
					<li><a href="//suite.social/reseller/" title="Homepage" style="cursor:pointer;">Wizard</a></li>					
					<li><a href="?page=codeshop-add-campaign" title="Create" style="cursor:pointer;">Create</a></li>					
					<li><a href="?page=codeshop-campaigns" title="View" style="cursor:pointer;">View</a></li>
					<li><a href="?page=codeshop-add-code" title="Codes" style="cursor:pointer;">Codes</a></li>
					<li><a href="?page=codeshop" title="Settings" style="cursor:pointer;">Settings</a></li>
					<li><a href="?page=codeshop-using" title="Embed" style="cursor:pointer;">Embed</a></li>
					<li><a href="?page=codeshop-transactionse" title="Transactions" style="cursor:pointer;">Transactions</a></li>					
					<li><a href="login.php?logout=true" title="Logout" style="cursor:pointer;">Logout</a></li>
					<!--<li><a href="//suite.social/about" target="_blank" title="About" style="cursor:pointer;">About</a></li>-->
					<li><a href="//suite.social/#contact" target="_blank" title="Contact Us" style="cursor:pointer;">Contact</a></li>					

                </ul>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

</header>

