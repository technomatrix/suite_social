<?php
//ob_start();
//session_start();
define('UAP_DB_HOST', 'localhost');
define('UAP_DB_HOST_PORT', '3306');
define('UAP_DB_USER', 'root');
define('UAP_DB_PASSWORD', 'mysql');
define('UAP_DB_NAME', 'suite_pay_new');
define('UAP_TABLE_PREFIX', 'uap_');
define('DB_NAME', 'suite_pay_new');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** MySQL hostname port */
define('DB_HOST_PORT', '3306');

define('SITE_URL', 'http://localhost/suite_social/');

define('FB_APP_ID', '');
define('FB_APP_SECRET', '');
define('FB_APP_SCOPE', 'email');
define('FB_APP_REDIRECT', SITE_URL . 'inc/social.php?type=facebook');

define('LI_APP_ID', '');
define('LI_APP_SECRET', '');
define('LI_APP_SCOPE', 'r_basicprofile r_emailaddress');
define('LI_APP_CALLBACK', SITE_URL . 'inc/social.php?type=linkedin');

define('G_CLIENT_ID', '');
define('G_CLIENT_SECRET', '');
define('G_APP_CALLBACK', SITE_URL . 'inc/social.php?type=google');

?>