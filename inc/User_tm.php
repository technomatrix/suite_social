<?php


class User_tm extends DbConnect
{
    public function __construct()
    {
        parent::__construct();
    }

    public function insertNormal()
    {
        $succes=false;

        $full_name = $_REQUEST['full_name'];
        $username = $full_name.uniqid();
        $email = $_REQUEST['email'];

//use of prepared statement is recommended
        $stmt = $this->wpdb->prepare("INSERT INTO users (full_name, username, email,user_type)
  VALUES (:full_name, :username, :email,:user_type)");
        $stmt->bindParam(':full_name', $full_name);
        $stmt->bindParam(':username', $username);
        $stmt->bindParam(':email', $email);

        // for passwrd 1 more hashing --todo

        $stmt->execute();
        return true;
//
    }


}
