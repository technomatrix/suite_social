<?php

error_reporting(E_ALL);
session_start();
include_once 'config.php';
include_once 'icdb.php';
include_once 'social/facebook/facebook.php';
include_once 'social/google/Google_Client.php';
include_once 'social/google/contrib/Google_Oauth2Service.php';
include_once 'social/linkedin/http.php';
include_once 'social/linkedin/oauth_client.php';
ini_set("error_log", "error.log");
$icdb = new ICDB(DB_HOST, DB_HOST_PORT, DB_NAME, DB_USER, DB_PASSWORD, '');

$type = isset($_GET['type']) ? $_GET['type'] : '';
if ($type == 'facebook') {
//    $facebook = new Facebook(array(
//        'appId' => FB_APP_ID,
//        'secret' => FB_APP_SECRET
//    ));
    //$fbuser = $facebook->getUser();


    if (isset($_GET['code'])) {
        $token = curlHttpRequest("https://graph.facebook.com/oauth/access_token?client_id=" . FB_APP_ID . "&redirect_uri=" . FB_APP_REDIRECT . "&client_secret=" . FB_APP_SECRET . "&code=" . $_GET['code']);

        $token = json_decode($token, true);
        $token = isset($token['access_token']) ? $token['access_token'] : '';
        //$token = explode("&", str_replace("access_token=", "", $token));
        //$token = $token[0];
        $request = curlHttpRequest("https://graph.facebook.com/me?fields=email,first_name,last_name,birthday&access_token=" . $token);
        $userProfile = json_decode($request, true);
        $social_id = $userProfile['id'];
        $social_type = 'facebook';
        $firstname = $userProfile['first_name'];
        $lastname = $userProfile['last_name'];
        $full_name = $firstname . ' ' . $lastname;
        $email = $userProfile['email'];

        $data = array(
            'social_id' => $social_id,
            'social_type' => $social_type,
            'first_name' => $firstname,
            'last_name' => $lastname,
            'email' => $email,
            'user_type' => 'User',
            'status' => '1'
        );
        $_SESSION['is_logged_in'] = 1;
        $_SESSION['social_id'] = $social_id;
        $_SESSION['social_type'] = $social_type;
        $_SESSION['social_name'] = $full_name;

        if (checkSocialUserExist($social_id)) {
            $query = "UPDATE users SET full_name='" . $full_name . "',email='" . $email . "',social_type='" . $social_type . "',user_type='User',status='1' WHERE social_id='" . $social_id . "'";
            $icdb->query($query);
        } else {
            $query = "INSERT INTO users SET social_id='" . $social_id . "',full_name='" . $full_name . "',email='" . $email . "',social_type='" . $social_type . "',user_type='User',status='1'";
            $icdb->query($query);
        }
        header("Location: " . SITE_URL . '');
        exit;
    }

#Auth URL
    $url = "https://www.facebook.com/dialog/oauth?client_id=" . FB_APP_ID . "&redirect_uri=" . FB_APP_REDIRECT . "&scope=email,user_birthday&response_type=code";
    header("Location: " . $url);
    exit;
    /* if ($fbuser) {
      try {
      $userProfile = $facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');
      $social_id = $userProfile['id'];
      $social_type = 'facebook';
      $firstname = $userProfile['first_name'];
      $lastname = $userProfile['last_name'];
      $full_name = $firstname . ' ' . $lastname;
      $email = $userProfile['email'];

      $data = array(
      'social_id' => $social_id,
      'social_type' => $social_type,
      'first_name' => $firstname,
      'last_name' => $lastname,
      'email' => $email,
      'user_type' => 'User',
      'status' => '1'
      );
      $_SESSION['is_logged_in'] = 1;
      $_SESSION['social_id'] = $social_id;
      $_SESSION['social_type'] = $social_type;
      $_SESSION['social_name'] = $full_name;

      if (checkSocialUserExist($social_id)) {
      $query = "UPDATE users SET full_name='" . $full_name . "',email='" . $email . "',social_type='" . $social_type . "',user_type='User',status='1' WHERE social_id='" . $social_id . "'";
      $icdb->query($query);
      } else {
      $query = "INSERT INTO users SET social_id='" . $social_id . "',full_name='" . $full_name . "',email='" . $email . "',social_type='" . $social_type . "',user_type='User',status='1'";
      $icdb->query($query);
      }
      header("Location: " . SITE_URL . '');
      } catch (Exception $ex) {
      echo $ex->getMessage();
      //            $facebook->destroySession();
      //            session_destroy();
      //            header("Location: http://suite.social/reseller/new/login.php");
      }
      } else {
      $fbuser = '';
      $fb_login_url = $facebook->getLoginUrl(array('redirect_uri' => FB_APP_REDIRECT, 'scope' => FB_APP_SCOPE));
      header("Location: " . $fb_login_url);
      } */
}

if ($type == 'linkedin') {
    $client = new oauth_client_class;
    $client->client_id = LI_APP_ID;
    $client->client_secret = LI_APP_SECRET;
    $client->debug = false;
    $client->debug_http = true;
    $client->redirect_uri = LI_APP_CALLBACK;
    $application_line = __LINE__;

    if (strlen($client->client_id) == 0 || strlen($client->client_secret) == 0)
        die('Please go to LinkedIn Apps page https://www.linkedin.com/secure/developer?newapp= , ' .
                        'create an application, and in the line ' . $application_line .
                        ' set the client_id to Consumer key and client_secret with Consumer secret. ' .
                        'The Callback URL must be ' . $client->redirect_uri) . ' Make sure you enable the ' .
                'necessary permissions to execute the API calls your application needs.';
    $client->scope = LI_APP_SCOPE;
    $success = $client->Initialize();
    $success = $client->Process();
    if (strlen($client->authorization_error)) {
        
    } elseif (strlen($client->access_token)) {
        $success = $client->CallAPI(
                'http://api.linkedin.com/v1/people/~:(id,email-address,first-name,last-name,location,picture-url,public-profile-url,formatted-name)', 'GET', array(
            'format' => 'json'
                ), array('FailOnAccessError' => true), $user);

        $social_id = $user->id;
        $social_type = 'linkedin';
        $full_name = $user->formattedName;
        $email = $user->emailAddress;

        $_SESSION['is_logged_in'] = 1;
        $_SESSION['social_id'] = $social_id;
        $_SESSION['social_type'] = $social_type;
        $_SESSION['social_name'] = $full_name;

        if (checkSocialUserExist($social_id)) {
            $query = "UPDATE users SET full_name='" . $full_name . "',email='" . $email . "',social_type='" . $social_type . "',user_type='User',status='1' WHERE social_id='" . $social_id . "'";
            $icdb->query($query);
        } else {
            $query = "INSERT INTO users SET social_id='" . $social_id . "',full_name='" . $full_name . "',email='" . $email . "',social_type='" . $social_type . "',user_type='User',status='1'";
            $icdb->query($query);
        }
        header("Location: " . SITE_URL . '');
    }
}

if ($type == 'google') {
    $gClient = new Google_Client();
    //$gClient->setApplicationName('Social Suite');
    $gClient->setClientId(G_CLIENT_ID);
    $gClient->setClientSecret(G_CLIENT_SECRET);
    $gClient->setRedirectUri(G_APP_CALLBACK);
    $google_oauthV2 = new Google_Oauth2Service($gClient);

    if (isset($_GET['code'])) {
        $gClient->authenticate();
        $_SESSION['google_access_token'] = $gClient->getAccessToken();
        header("Location: " . G_APP_CALLBACK);
    }
    if (isset($_SESSION['google_access_token']) && !empty($_SESSION['google_access_token'])) {
        $gClient->setAccessToken($_SESSION['google_access_token']);
    }

    if ($gClient->getAccessToken()) {
        $userProfile = $google_oauthV2->userinfo->get();
        $social_id = $userProfile['id'];
        $social_type = 'google';
        $full_name = $userProfile['name'];
        $firstname = $userProfile['given_name'];
        $lastname = $userProfile['family_name'];
        $email = $userProfile['email'];

        $_SESSION['is_logged_in'] = 1;
        $_SESSION['social_id'] = $social_id;
        $_SESSION['social_type'] = $social_type;
        $_SESSION['social_name'] = $full_name;

        if (checkSocialUserExist($social_id)) {
            $query = "UPDATE users SET full_name='" . $full_name . "',email='" . $email . "',social_type='" . $social_type . "',user_type='User',status='1' WHERE social_id='" . $social_id . "'";
            $icdb->query($query);
        } else {
            $query = "INSERT INTO users SET social_id='" . $social_id . "',full_name='" . $full_name . "',email='" . $email . "',social_type='" . $social_type . "',user_type='User',status='1'";
            $icdb->query($query);
        }
        header("Location: " . SITE_URL . '');
    } else {
        $login_url = $gClient->createAuthUrl();
        header('Location: ' . $login_url);
    }
}

function checkSocialUserExist($social_id) {
    $icdb = new ICDB(DB_HOST, DB_HOST_PORT, DB_NAME, DB_USER, DB_PASSWORD, '');
    $user_check = $icdb->get_row("SELECT * FROM users WHERE social_id = '" . $social_id . "' AND status='1'");
    if (count($user_check) > 0)
        return true;
    else
        return false;
}

function curlHttpRequest($url, $method = "get", $request_fields = array()) {

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_MAXREDIRS, 10);

    if ($method == "post") {
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request_fields);
    } else if ($method == "get") {
        curl_setopt($ch, CURLOPT_HTTPGET, true);
    } else if ($method == "del") {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    }

    return curl_exec($ch);
}
