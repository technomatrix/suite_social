<?php
/*
Plugin Name: Code Shop
Plugin URI: http://halfdata.com/milkyway/code-shop.html
Description: Sell coupons/vouchers/licesnse codes with ease.
Version: 2.21
Author: Halfdata, Inc.
Author URI: http://codecanyon.net/user/halfdata?ref=halfdata
*/

define('CODESHOP_RECORDS_PER_PAGE', '20');
define('CODESHOP_VERSION', 2.21);
register_activation_hook(__FILE__, array("codeshop_class", "install"));

class codeshop_class {
	var $options;
	var $error;
	var $info;
	var $currency_list;
	var $default_campaign;
	var $displayed = false;
	
	var $paypal_currency_list = array("USD", "AUD", "BRL", "CAD", "CHF", "CZK", "DKK", "EUR", "GBP", "HKD", "HUF", "ILS", "JPY", "MXN", "MYR", "NOK", "NZD", "PHP", "PLN", "RUB", "SEK", "SGD", "THB", "TRY", "TWD");
	var $payza_currency_list = array("AUD", "BGN", "CAD", "CHF", "CZK", "DKK", "EEK", "EUR", "GBP", "HKD", "HUF", "INR", "LTL", "MYR", "MKD", "NOK", "NZD", "PLN", "RON", "SEK", "SGD", "USD", "ZAR");
	var $skrill_currency_list = array("EUR","TWD","USD","THB","GBP","CZK","HKD","HUF","SGD","SKK","JPY","EEK","CAD","BGN","AUD","PLN","CHF","ISK","DKK","INR","SEK","LVL","NOK","KRW","ILS","ZAR","MYR","RON","NZD","HRK","TRY","LTL","AED","JOD","MAD","OMR","QAR","RSD","SAR","TND");
	var $interkassa_currency_list = array("USD", "EUR", "GBP", "RUB", "UAH");
	var $egopay_currency_list = array("USD", "EUR");
	var $authnet_currency_list = array("USD", "EUR", "GBP");
	var $bitpay_currency_list = array("USD", "EUR", "GBP", "AUD", "CAD", "CHF", "CNY", "RUB", "DKK", "HKD", "PLN", "SGD", "THB", "BTC");
	var $blockchain_currency_list = array("BTC", "USD", "ISK", "HKD", "TWD", "CHF", "EUR", "DKK", "CLP", "CAD", "CNY", "THB", "AUD", "SGD", "KRW", "JPY", "PLN", "GBP", "SEK", "NZD", "BRL", "RUB");
	var $stripe_currency_list = array("AED", "AFN", "ALL", "AMD", "ANG", "AOA", "ARS", "AUD", "AWG", "AZN", "BAM", "BBD", "BDT", "BGN", "BIF", "BMD", "BND", "BOB", "BRL", "BSD", "BWP", "BZD", "CAD", "CDF", "CHF", "CLP", "CNY", "COP", "CRC", "CVE", "CZK", "DJF", "DKK", "DOP", "DZD", "EEK", "EGP", "ETB", "EUR", "FJD", "FKP", "GBP", "GEL", "GIP", "GMD", "GNF", "GTQ", "GYD", "HKD", "HNL", "HRK", "HTG", "HUF", "IDR", "ILS", "INR", "ISK", "JMD", "JPY", "KES", "KGS", "KHR", "KMF", "KRW", "KYD", "KZT", "LAK", "LBP", "LKR", "LRD", "LSL", "LTL", "LVL", "MAD", "MDL", "MGA", "MKD", "MNT", "MOP", "MRO", "MUR", "MVR", "MWK", "MXN", "MYR", "MZN", "NAD", "NGN", "NIO", "NOK", "NPR", "NZD", "PAB", "PEN", "PGK", "PHP", "PKR", "PLN", "PYG", "QAR", "RON", "RSD", "RUB", "RWF", "SAR", "SBD", "SCR", "SEK", "SGD", "SHP", "SLL", "SOS", "SRD", "STD", "SVC", "SZL", "THB", "TJS", "TOP", "TRY", "TTD", "TWD", "TZS", "UAH", "UGX", "USD", "UYU", "UZS", "VND", "VUV", "WST", "XAF", "XCD", "XOF", "XPF", "YER", "ZAR", "ZMW");
	
	function __construct() {
		if (function_exists('load_plugin_textdomain')) {
			load_plugin_textdomain('codeshop', false, dirname(plugin_basename(__FILE__)).'/languages/');
		}
		$this->currency_list = array_unique(array_merge($this->paypal_currency_list, $this->payza_currency_list, $this->interkassa_currency_list, $this->egopay_currency_list, $this->skrill_currency_list, $this->bitpay_currency_list, $this->stripe_currency_list, $this->blockchain_currency_list));
		sort($this->currency_list);
		$this->currency_list = array_unique(array_merge(array("USD"), $this->currency_list));
		
		$this->options = array (
			"version" => CODESHOP_VERSION,
			"enable_paypal" => "off",
			"paypal_id" => "",
			"paypal_address" => "off",
			"enable_payza" => "off",
			"payza_id" => "",
			"payza_sandbox" => "off",
			"enable_interkassa" =>"off",
			"interkassa_checkout_id" => "",
			"interkassa_secret_key" => "",
			"enable_authnet" => "off",
			"authnet_login" => "",
			"authnet_sandbox" => "off",
			"authnet_key" => "",
			"authnet_md5hash" => "",
			"enable_skrill" => "off",
			"skrill_id" => "",
			"skrill_secret_word" => "",
			"enable_egopay" => "off",
			"egopay_store_id" => "",
			"egopay_store_pass" => "",
			"enable_bitpay" => "off",
			"bitpay_key" => "",
			"bitpay_speed" => "medium",
			"enable_blockchain" => "off",
			"blockchain_api_key" => "",
			"blockchain_xpub" => "",
			"blockchain_secret" => "",
			"blockchain_confirmations" => 1,
			"enable_stripe" => "on",
			"stripe_secret" => "",
			"stripe_publishable" => "",
//			"admin_email" => "alerts@".str_replace("www.", "", $_SERVER["SERVER_NAME"]),
//			"from_name" => get_bloginfo("name"),
//			"from_email" => "noreply@".str_replace("www.", "", $_SERVER["SERVER_NAME"])
			"admin_email" => "",
			"from_name" => "",
			"from_email" => ""
		);

		$this->default_campaign = array (
			"title" => __('Default Campaign', 'codeshop'),
			"intro" => '',
			"terms" => '',
			"price" => '5.00',
			"currency" => 'USD',
			"allowed_sales" => 0,
			"is_flexible" => 0,
			"no_codes" => __('We are sorry. No more campaign codes available.', 'codeshop'),
			"success_email" => __('Dear {payer_name}.', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you about completed payment.', 'codeshop').PHP_EOL.__('Your personal campaign code is:', 'codeshop').' {code}'.PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.get_bloginfo('name'),
			"failed_email" => __('Dear {payer_name}.', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you about non-completed payment.', 'codeshop').PHP_EOL.__('Payment status is:', 'codeshop').' {payment_status}'.PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.get_bloginfo('name'),
			"success_url" => defined('UAP_CORE') ? '' : get_bloginfo('url'),
			"failed_url" => defined('UAP_CORE') ? '' : get_bloginfo('url')
		);

		if (!empty($_COOKIE["codeshop_error"])) {
			$this->error = stripslashes($_COOKIE["codeshop_error"]);
			setcookie("codeshop_error", "", time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
		}
		if (!empty($_COOKIE["codeshop_info"])) {
			$this->info = stripslashes($_COOKIE["codeshop_info"]);
			setcookie("codeshop_info", "", time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
		}

		//if (defined('WP_ALLOW_MULTISITE')) $this->install();
		$this->get_options();
		
		if (is_admin()) {
			if ($this->check_options() !== true) add_action('admin_notices', array(&$this, 'admin_warning'));
			add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts'));
			add_action('admin_menu', array(&$this, 'admin_menu'));
			add_action('init', array(&$this, 'admin_request_handler'));
			add_action('wp_ajax_codeshop_continue', array(&$this, "codeshop_continue"));
			add_action('wp_ajax_nopriv_codeshop_continue', array(&$this, "codeshop_continue"));
			add_action('wp_ajax_codeshop_getbitpayurl', array(&$this, "codeshop_getbitpayurl"));
			add_action('wp_ajax_nopriv_codeshop_getbitpayurl', array(&$this, "codeshop_getbitpayurl"));
			add_action('wp_ajax_codeshop_getblockchainaddress', array(&$this, "codeshop_getblockchainaddress"));
			add_action('wp_ajax_nopriv_codeshop_getblockchainaddress', array(&$this, "codeshop_getblockchainaddress"));
			add_action('wp_ajax_codeshop_charge', array(&$this, "codeshop_charge"));
			add_action('wp_ajax_nopriv_codeshop_charge', array(&$this, "codeshop_charge"));
			add_action('wp_ajax_codeshop_sendcode', array(&$this, "codeshop_sendcode"));
			add_action('wp_ajax_nopriv_codeshop_sendcode', array(&$this, "codeshop_sendcode"));
			add_action('wp_ajax_codeshop-remote-init', array(&$this, "remote_init"));
			add_action('wp_ajax_nopriv_codeshop-remote-init', array(&$this, "remote_init"));
		} else {
			add_action("wp", array(&$this, "front_wp"));
			add_shortcode('codeshop', array(&$this, "shortcode_handler"));
		}
		add_action("init", array(&$this, "front_init"));
	}

	function admin_enqueue_scripts() {
		wp_enqueue_script("jquery");
		if (isset($_GET['page']) && ($_GET['page'] == 'codeshop-transactions' || $_GET['page'] == 'codeshop' || $_GET['page'] == 'codeshop-codes')) {
			wp_enqueue_script("thickbox");
			wp_enqueue_style("thickbox");
		}
		wp_enqueue_style('codeshop', plugins_url('/css/admin.css', __FILE__), array(), CODESHOP_VERSION);
	}

	static function install () {
		global $wpdb;

		$table_name = $wpdb->prefix."cs_campaigns";
		if($wpdb->get_var("SHOW TABLES LIKE '".$table_name."'") != $table_name) {
			$sql = "CREATE TABLE ".$table_name." (
				id int(11) NULL AUTO_INCREMENT,
				title varchar(255) COLLATE utf8_unicode_ci NULL,
				intro text COLLATE utf8_unicode_ci NULL,
				terms text COLLATE utf8_unicode_ci NULL,
				price float NULL,
				currency varchar(15) COLLATE utf8_unicode_ci NULL,
				is_flexible int(11) NULL DEFAULT '0',
				allowed_sales int(11) NULL DEFAULT '0',
				no_codes text COLLATE utf8_unicode_ci NULL,
				success_email text COLLATE utf8_unicode_ci NULL,
				failed_email text COLLATE utf8_unicode_ci NULL,
				success_url varchar(255) COLLATE utf8_unicode_ci NULL,
				failed_url varchar(255) COLLATE utf8_unicode_ci NULL,
				created int(11) NULL,
				blocked int(11) NULL DEFAULT '0',
				deleted int(11) NULL DEFAULT '0',
				UNIQUE KEY  id (id)
			);";
			$wpdb->query($sql);
		}
		$table_name = $wpdb->prefix."cs_codes";
		if($wpdb->get_var("SHOW TABLES LIKE '".$table_name."'") != $table_name) {
			$sql = "CREATE TABLE ".$table_name." (
				id int(11) NULL AUTO_INCREMENT,
				campaign_id int(11) NULL,
				block_str varchar(31) COLLATE utf8_unicode_ci NULL,
				code text COLLATE utf8_unicode_ci NULL,
				sales int(11) NULL DEFAULT '0',
				created int(11) NULL,
				blocked int(11) NULL DEFAULT '0',
				deleted int(11) NULL DEFAULT '0',
				UNIQUE KEY  id (id)
			);";
			$wpdb->query($sql);
		}
		$wpdb->query("ALTER TABLE ".$table_name." CHANGE code code TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL");
		$table_name = $wpdb->prefix."cs_transactions";
		if($wpdb->get_var("SHOW TABLES LIKE '".$table_name."'") != $table_name) {
			$sql = "CREATE TABLE ".$table_name." (
				id int(11) NULL AUTO_INCREMENT,
				campaign_id int(11) NULL,
				code_id int(11) NULL,
				payer_name varchar(255) COLLATE utf8_unicode_ci NULL,
				payer_email varchar(255) COLLATE utf8_unicode_ci NULL,
				gross float NULL,
				currency varchar(15) COLLATE utf8_unicode_ci NULL,
				payment_status varchar(63) COLLATE utf8_unicode_ci NULL,
				transaction_type varchar(63) COLLATE utf8_unicode_ci NULL,
				txn_id varchar(255) COLLATE utf8_unicode_ci NULL,
				details text COLLATE utf8_unicode_ci NULL,
				created int(11) NULL,
				deleted int(11) NULL DEFAULT '0',
				UNIQUE KEY id (id)
			);";
			$wpdb->query($sql);
		}
	}

	function get_options() {
		$exists = get_option('codeshop_version');
		if ($exists) {
                    $userid=isset($_SESSION['user_id'])?$_SESSION['user_id']:'';
			foreach ($this->options as $key => $value) {
				$this->options[$key] = get_option('codeshop_'.$key, $this->options[$key],$userid);
			}
		}
		if ($this->options['blockchain_secret'] == '') $this->options['blockchain_secret'] = $this->random_string(16);
	}
	function user_get_options($uid) {
		$exists = get_option('codeshop_version');
		if ($exists) {
			foreach ($this->options as $key => $value) {
				$this->options[$key] = get_option('codeshop_'.$key, '',$uid);
			}
		}
		if ($this->options['blockchain_secret'] == '') $this->options['blockchain_secret'] = $this->random_string(16);
	}

	function update_options() {
		//if (current_user_can('manage_options')) {
			foreach ($this->options as $key => $value) {
				update_option('codeshop_'.$key, $value,$_SESSION['user_id']);
			}
		//}
	}

	function populate_options() {
		foreach ($this->options as $key => $value) {
			if (isset($_POST['codeshop_'.$key])) {
				$this->options[$key] = stripslashes($_POST['codeshop_'.$key]);
			}
		}
	}

	function check_options() {
		$errors = array();
		//if ($this->options['enable_payza'] != "on" && $this->options['enable_paypal'] != "on" && $this->options['enable_interkassa'] != "on" && $this->options['enable_authnet'] != "on" && $this->options['enable_egopay'] != "on" && $this->options['enable_skrill'] != "on" && $this->options['enable_bitpay'] != "on" && $this->options['enable_stripe'] != "on") $errors[] = __('Select at least one payment method', 'codeshop');
		if ($this->options['enable_paypal'] == "on") {
			if ((!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,19})$/i", $this->options['paypal_id']) && !preg_match("/^([A-Z0-9]+)$/i", $this->options['paypal_id'])) || strlen($this->options['paypal_id']) == 0) $errors[] = __('PayPal ID must be valid e-mail address or Merchant ID', 'codeshop');
		}
		if ($this->options['enable_payza'] == "on") {
			if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,19})$/i", $this->options['payza_id']) || strlen($this->options['payza_id']) == 0) $errors[] = __('Payza ID must be valid e-mail address', 'codeshop');
		}
		if ($this->options['enable_interkassa'] == "on") {
			if (strlen($this->options['interkassa_checkout_id']) < 3) $errors[] = __('InterKassa Checkout ID is required', 'codeshop');
			if (strlen($this->options['interkassa_secret_key']) < 3) $errors[] = __('InterKassa Secret Key is required', 'codeshop');
		}
		if ($this->options['enable_authnet'] == "on") {
			if (strlen($this->options['authnet_login']) < 3) $errors[] = __('Authorize.Net API Login ID is required', 'codeshop');
			if (strlen($this->options['authnet_key']) < 1) $errors[] = __('Authorize.Net Transaction Key is required', 'codeshop');
			if (strlen($this->options['authnet_md5hash']) < 1) $errors[] = __('Authorize.Net MD5 Hash is required', 'codeshop');
		}
		if ($this->options['enable_egopay'] == "on") {
			if (strlen($this->options['egopay_store_id']) < 1) $errors[] = __('EgoPay Store ID is required', 'codeshop');
			if (strlen($this->options['egopay_store_pass']) < 1) $errors[] = __('EgoPay Store Pass is required', 'codeshop');
		}
		if ($this->options['enable_skrill'] == "on") {
			if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,19})$/i", $this->options['skrill_id']) || strlen($this->options['skrill_id']) == 0) $errors[] = __('Skrill ID must be valid e-mail address', 'codeshop');
			if (strlen($this->options['skrill_secret_word']) < 1) $errors[] = __('Skrill Secret Word is required', 'codeshop');
		}
		if ($this->options['enable_bitpay'] == "on") {
			if (strlen($this->options['bitpay_key']) < 1) $errors[] = __('BitPay API Key is required', 'codeshop');
		}
		if ($this->options['enable_stripe'] == "on") {
			if (strlen($this->options['stripe_secret']) < 1) $errors[] = __('Stripe Secret Key is required', 'codeshop');
			if (strlen($this->options['stripe_publishable']) < 1) $errors[] = __('Stripe Publishable Key is required', 'codeshop');
		}
		if ($this->options['enable_blockchain'] == "on") {
			if (strlen($this->options['blockchain_api_key']) < 5) $errors[] =  __('Blockchain Receive Payment API Key is invalid', 'codeshop');
			if (strlen($this->options['blockchain_xpub']) < 5) $errors[] =  __('Blockchain Extended Public Key (xPub) is invalid', 'codeshop');
			if (strlen($this->options['blockchain_secret']) < 12) $errors[] =  __('Bitcoin Secret is too short', 'codeshop');
		}
		if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,19})$/i", $this->options['admin_email']) || strlen($this->options['admin_email']) == 0) $errors[] = __('E-mail for notifications must be valid e-mail address', 'codeshop');
		if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,19})$/i", $this->options['from_email']) || strlen($this->options['from_email']) == 0) $errors[] = __('Sender e-mail must be valid e-mail address', 'codeshop');
		if (strlen($this->options['from_name']) < 3) $errors[] = __('Sender name is too short', 'codeshop');
		if (empty($errors)) return true;
		return $errors;
	}

	function admin_menu() {
		add_menu_page(
			"Code Shop"
			, "Code Shop"
			, "add_users"
			, "codeshop"
			, array(&$this, 'admin_settings')
		);
		add_submenu_page(
			"codeshop"
			, __('Settings', 'codeshop')
			, __('Settings', 'codeshop')
			, "add_users"
			, "codeshop"
			, array(&$this, 'admin_settings')
		);
		add_submenu_page(
			"codeshop"
			, __('Campaigns', 'codeshop')
			, __('Campaigns', 'codeshop')
			, "add_users"
			, "codeshop-campaigns"
			, array(&$this, 'admin_campaigns')
		);
		add_submenu_page(
			"codeshop"
			, __('Add Campaign', 'codeshop')
			, __('Add Campaign', 'codeshop')
			, "add_users"
			, "codeshop-add-campaign"
			, array(&$this, 'admin_add_campaign')
		);
		add_submenu_page(
			"codeshop"
			, __('Codes', 'codeshop')
			, __('Codes', 'codeshop')
			, "add_users"
			, "codeshop-codes"
			, array(&$this, 'admin_codes')
		);
		add_submenu_page(
			"codeshop"
			, __('Add Code', 'codeshop')
			, __('Add Code', 'codeshop')
			, "add_users"
			, "codeshop-add-code"
			, array(&$this, 'admin_add_code')
		);
		add_submenu_page(
			"codeshop"
			, __('Transactions', 'codeshop')
			, __('Transactions', 'codeshop')
			, "add_users"
			, "codeshop-transactions"
			, array(&$this, 'admin_transactions')
		);
		if (defined('UAP_CORE')) {
			add_submenu_page(
				"codeshop"
				, __('How To Use', 'codeshop')
				, __('How To Use', 'codeshop')
				, "manage_options"
				, "codeshop-using"
				, array(&$this, 'admin_using')
			);
		}
	}

	function admin_settings() {
		global $wpdb;
		$message = "";
		$errors = array();
		if (!empty($this->error)) $message = '<div class="error"><p>'.$this->error.'</p></div>';
		else {
			$errors = $this->check_options();
			if (is_array($errors)) echo '<div class="error"><p>'.__('The following error(s) exists:', 'codeshop').'<br />- '.implode("<br />- ", $errors).'</p></div>';
			else if (!empty($this->info)) $message = '<div class="updated"><p>'.$this->info.'</p></div>';
		}
		echo '
		<div class="wrap codeshop">
			<h2>'.__('Settings', 'codeshop').'</h2>
			'.$message.'
			<form enctype="multipart/form-data" method="post" style="margin: 0px" action="'.admin_url('admin.php').'">
			<div class="postbox-container" style="width: 100%;">
				<div class="metabox-holder">
					<div class="meta-box-sortables ui-sortable">
						<div class="postbox codeshop_postbox">
							<!--<div class="handlediv" title="Click to toggle"><br></div>-->
							<h3 class="hndle" style="cursor: default;"><span>'.__('General Settings', 'codeshop').'</span></h3>
							<div class="inside">
								<table class="codeshop_useroptions">
									<tr>
										<th>'.__('E-mail for notifications', 'codeshop').':</th>
										<td><input type="text" id="codeshop_admin_email" name="codeshop_admin_email" value="'.htmlspecialchars($this->options['admin_email'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter e-mail address. All alerts/notifications about completed/failed payments are sent to this e-mail address.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Sender name', 'codeshop').':</th>
										<td><input type="text" id="codeshop_from_name" name="codeshop_from_name" value="'.htmlspecialchars($this->options['from_name'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter sender name. All alerts/notifications are sent using this name as "FROM:" header value.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Sender e-mail', 'codeshop').':</th>
										<td><input type="text" id="codeshop_from_email" name="codeshop_from_email" value="'.htmlspecialchars($this->options['from_email'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter sender e-mail. All alerts/notifications are sent using this e-mail as "FROM:" header value.', 'codeshop').'</em></td>
									</tr>
								</table>
								<div class="alignright">
								<input type="hidden" name="action" value="codeshop-update-options" />
								<input type="hidden" name="codeshop_version" value="'.CODESHOP_VERSION.'" />
								<input type="submit" class="codeshop_button btn btn-primary" name="submit" value="'.__('Update Settings', 'codeshop').'">
								</div>
							</div>
						</div>
						<hr>
						<div class="postbox codeshop_postbox">
							<!--<div class="handlediv" title="Click to toggle"><br></div>-->
							<h3 class="hndle" style="cursor: default;"><span>'.__('PayPal Settings', 'codeshop').'</span></h3>
							<div class="inside">
								<table class="codeshop_useroptions">
									<tr>
										<th>'.__('Enable', 'codeshop').':</th>
										<td><input type="checkbox" id="codeshop_enable_paypal" name="codeshop_enable_paypal" '.($this->options['enable_paypal'] == "on" ? 'checked="checked"' : '').'"> '.__('Accept payments via PayPal', 'codeshop').'<br /><em>'.__('Please tick checkbox if you would like to accept payments via PayPal.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('PayPal ID', 'codeshop').':</th>
										<td><input type="text" id="codeshop_paypal_id" name="codeshop_paypal_id" value="'.htmlspecialchars($this->options['paypal_id'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter valid PayPal e-mail or <a href="https://www.paypal.com/webapps/customerpropage/summary.view" traget="_blank">Merchant ID</a>, all payments are sent to this account.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Enable shipping address', 'codeshop').':</th>
										<td><input type="checkbox" id="codeshop_paypal_address" name="codeshop_paypal_address" '.($this->options['paypal_address'] == "on" ? 'checked="checked"' : '').'> '.__('Include shipping address into transaction', 'codeshop').'<br /><em>'.__('Activate this option if you plan to send physical copy of your code/voucher (CD, DVD, etc.). Shipping address is taken from payer PayPal account. You can view shipping address at transaction details page.', 'codeshop').'</em></td>
									</tr>
								</table>
								<div class="alignright">
									<input type="submit" class="codeshop_button btn btn-primary" name="submit" value="'.__('Update Settings', 'codeshop').'">
								</div>
							</div>
						</div>
						<hr>
						<div class="postbox codeshop_postbox">
							<!--<div class="handlediv" title="Click to toggle"><br></div>-->
							<h3 class="hndle" style="cursor: default;"><span>'.__('Payza Settings', 'codeshop').'</span></h3>
							<div class="inside">
								<table class="codeshop_useroptions">
									<tr><th colspan="2">'.__('IMPORTANT! Set "IPN Status" as "Enabled" on <a target="_blank" href="https://secure.payza.com/ManageIPN.aspx">Payza IPN Setup</a> page.', 'codeshop').'</th></tr>
									<tr>
										<th>'.__('Enable', 'codeshop').':</th>
										<td><input type="checkbox" id="codeshop_enable_payza" name="codeshop_enable_payza" '.($this->options['enable_payza'] == "on" ? 'checked="checked"' : '').(!in_array('curl', get_loaded_extensions()) ? ' disabled="disabled"' : '').'> '.__('Accept payments via Payza', 'codeshop').'<br /><em>'.__('Please tick checkbox if you would like to accept payments via Payza.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Payza ID', 'codeshop').':</th>
										<td><input type="text" id="codeshop_payza_id" name="codeshop_payza_id" value="'.htmlspecialchars($this->options['payza_id'], ENT_QUOTES).'" class="widefat"'.(!in_array('curl', get_loaded_extensions()) ? ' disabled="disabled"' : '').'><br /><em>'.__('Please enter valid Payza e-mail, all payments are sent to this account.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Sandbox mode', 'codeshop').':</th>
										<td><input type="checkbox" id="codeshop_payza_sandbox" name="codeshop_payza_sandbox" '.($this->options['payza_sandbox'] == "on" ? 'checked="checked"' : '').'> '.__('Enable Payza sandbox mode', 'codeshop').'<br /><em>'.__('Please tick checkbox if you would like to test Payza service.', 'codeshop').'</em></td>
									</tr>
								</table>
								<div class="alignright">
									<input type="submit" class="codeshop_button btn btn-primary" name="submit" value="'.__('Update Settings', 'codeshop').'">
								</div>
							</div>
						</div>
						<hr>
						<div class="postbox codeshop_postbox">
							<!--<div class="handlediv" title="Click to toggle"><br></div>-->
							<h3 class="hndle" style="cursor: default;"><span>'.__('Skrill (Moneybookers) Settings', 'codeshop').'</span></h3>
							<div class="inside">
								<table class="codeshop_useroptions">
									<tr>
										<th>'.__('Enable', 'codeshop').':</th>
										<td><input type="checkbox" id="codeshop_enable_skrill" name="codeshop_enable_skrill" '.($this->options['enable_skrill'] == "on" ? 'checked="checked"' : '').'> '.__('Accept payments via Skrill (Moneybookers)', 'codeshop').'<br /><em>'.__('Please tick checkbox if you would like to accept payments via Skrill (Moneybookers).', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Skrill ID', 'codeshop').':</th>
										<td><input type="text" id="codeshop_skrill_id" name="codeshop_skrill_id" value="'.htmlspecialchars($this->options['skrill_id'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter valid Skrill (Moneybookers) e-mail, all payments are sent to this account.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Secret Word', 'codeshop').':</th>
										<td><input type="text" id="codeshop_skrill_secret_word" name="codeshop_skrill_secret_word" value="'.htmlspecialchars($this->options['skrill_secret_word'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter valid Skrill Secret Word. You must set it on <a target="_blank" href="https://www.moneybookers.com/app/propage.pl?view=merchant_tools">merchant tools</a> page.', 'codeshop').'</em></td>
									</tr>
								</table>
								<div class="alignright">
									<input type="submit" class="codeshop_button btn btn-primary" name="submit" value="'.__('Update Settings', 'codeshop').'">
								</div>
							</div>
						</div>
						<hr>
						<div class="postbox codeshop_postbox">
							<!--<div class="handlediv" title="Click to toggle"><br></div>-->
							<h3 class="hndle" style="cursor: default;"><span>'.__('InterKassa 2.0 Settings', 'codeshop').'</span></h3>
							<div class="inside">
								<table class="codeshop_useroptions">
									<tr><th colspan="2">'.__('IMPORTANT! Register your checkout on <a target="_blank" href="http://new.interkassa.com/account/checkout">My Checkouts</a> page.', 'codeshop').'</th></tr>
									<tr>
										<th>'.__('Enable', 'codeshop').':</th>
										<td><input type="checkbox" id="codeshop_enable_interkassa" name="codeshop_enable_interkassa" '.($this->options['enable_interkassa'] == "on" ? 'checked="checked"' : '').'> '.__('Accept payments via InterKassa', 'codeshop').'<br /><em>'.__('Please tick checkbox if you would like to accept payments via InterKassa.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Checkout ID', 'codeshop').':</th>
										<td><input type="text" id="codeshop_interkassa_checkout_id" name="codeshop_interkassa_checkout_id" value="'.htmlspecialchars($this->options['interkassa_checkout_id'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter valid InterKassa Checkout ID. Ex.: 5294dadebf4efc4f543309eb', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Secret/Sign Key', 'codeshop').':</th>
										<td><input type="text" id="codeshop_interkassa_secret_key" name="codeshop_interkassa_secret_key" value="'.htmlspecialchars($this->options['interkassa_secret_key'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter Secret/Sign Key. You can get it on <a target="_blank" href="http://new.interkassa.com/account/checkout">checkout settings</a> page.', 'codeshop').'</em></td>
									</tr>
								</table>
								<div class="alignright">
									<input type="submit" class="paiddownoads_button btn btn-primary" name="submit" value="'.__('Update Settings', 'codeshop').'">
								</div>
								<br class="clear">
							</div>
						</div>
						<hr>
						<div class="postbox codeshop_postbox">
							<!--<div class="handlediv" title="Click to toggle"><br></div>-->
							<h3 class="hndle" style="cursor: default;"><span>'.__('Authorize.Net Settings', 'codeshop').'</span></h3>
							<div class="inside">
								<table class="codeshop_useroptions">
									<tr>
										<th>'.__('Enable', 'codeshop').':</th>
										<td><input type="checkbox" id="codeshop_enable_authnet" name="codeshop_enable_authnet" '.($this->options['enable_authnet'] == "on" ? 'checked="checked"' : '').'> '.__('Accept payments via Authorize.Net', 'codeshop').'<br /><em>'.__('Please tick checkbox if you would like to accept payments via Authorize.Net.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('API Login ID', 'codeshop').':</th>
										<td><input type="text" id="codeshop_authnet_login" name="codeshop_authnet_login" value="'.htmlspecialchars($this->options['authnet_login'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter valid Authorize.Net login.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Transaction Key', 'codeshop').':</th>
										<td><input type="text" id="codeshop_authnet_key" name="codeshop_authnet_key" value="'.htmlspecialchars($this->options['authnet_key'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter Transaction Key. If you do not know Transaction Key, go to your Authorize.Net account settings and click "API Login ID and Transaction Key".', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('MD5 Hash', 'codeshop').':</th>
										<td><input type="text" id="codeshop_authnet_md5hash" name="codeshop_authnet_md5hash" value="'.htmlspecialchars($this->options['authnet_md5hash'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter MD5 Hash. If you do not know Transaction Key, go to your Authorize.Net account settings and click "MD5-Hash".', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Sandbox mode', 'codeshop').':</th>
										<td><input type="checkbox" id="codeshop_authnet_sandbox" name="codeshop_authnet_sandbox" '.($this->options['authnet_sandbox'] == "on" ? 'checked="checked"' : '').'> '.__('Enable Authorize.Net sandbox mode', 'codeshop').'<br /><em>'.__('Please tick checkbox if you would like to test Authorize.Net service.', 'codeshop').'</em></td>
									</tr>
								</table>
								<div class="alignright">
									<input type="submit" class="codeshop_button btn btn-primary" name="submit" value="'.__('Update Settings', 'codeshop').'">
								</div>
							</div>
						</div>
						<hr>
						<div class="postbox codeshop_postbox">
							<!--<div class="handlediv" title="Click to toggle"><br></div>-->
							<h3 class="hndle" style="cursor: default;"><span>'.__('EgoPay Settings', 'codeshop').'</span></h3>
							<div class="inside">
								<table class="codeshop_useroptions">
									<tr><th colspan="2">'.__('IMPORTANT! Register your store on <a target="_blank" href="https://www.egopay.com/store/list">EgoPay Stores</a> page.', 'codeshop').' '.__('Set', 'codeshop').' <span class="label-success" style="padding: 0 5px;">'.(defined('UAP_CORE') ? esc_html(admin_url('do.php').'?codeshop-ipn=egopay') : esc_html(get_bloginfo('url').'/?codeshop-ipn=egopay')).'</span> '.__('as <span class="label-success" style="padding: 0 5px;">CallBack URL</span>.', 'codeshop').'</th></tr>
									<tr>
										<th>'.__('Enable', 'codeshop').':</th>
										<td><input type="checkbox" id="codeshop_enable_egopay" name="codeshop_enable_egopay" '.($this->options['enable_egopay'] == "on" ? 'checked="checked"' : '').'> '.__('Accept payments via EgoPay', 'codeshop').'<br /><em>'.__('Please tick checkbox if you would like to accept payments via EgoPay.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Store ID', 'codeshop').':</th>
										<td><input type="text" id="codeshop_egopay_store_id" name="codeshop_egopay_store_id" value="'.htmlspecialchars($this->options['egopay_store_id'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter valid EgoPay Store ID. You can get this parameter <a href="https://www.egopay.com/store/list" target="_blank">here</a>.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Store Pass', 'codeshop').':</th>
										<td><input type="text" id="codeshop_egopay_store_pass" name="codeshop_egopay_store_pass" value="'.htmlspecialchars($this->options['egopay_store_pass'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter valid EgoPay Store Pass. You can get this parameter <a href="https://www.egopay.com/store/list" target="_blank">here</a>.', 'codeshop').'</em></td>
									</tr>
								</table>
								<div class="alignright">
									<input type="submit" class="codeshop_button btn btn-primary" name="submit" value="'.__('Update Settings', 'codeshop').'">
								</div>
							</div>
						</div>
						<hr>
						<div class="postbox codeshop_postbox">
							<!--<div class="handlediv" title="Click to toggle"><br></div>-->
							<h3 class="hndle" style="cursor: default;"><span>'.__('BitPay Settings', 'codeshop').'</span></h3>
							<div class="inside">
								<table class="codeshop_useroptions">
									<tr>
										<th>'.__('Enable', 'codeshop').':</th>
										<td><input type="checkbox" id="codeshop_enable_bitpay" name="codeshop_enable_bitpay" '.($this->options['enable_bitpay'] == "on" ? 'checked="checked"' : '').'> '.__('Accept payments via BitPay', 'codeshop').'<br /><em>'.__('Please tick checkbox if you would like to accept payments via BitPay.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('API Key', 'codeshop').':</th>
										<td><input type="text" id="codeshop_bitpay_key" name="codeshop_bitpay_key" value="'.htmlspecialchars($this->options['bitpay_key'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter valid API Key. You can get this parameter <a href="https://bitpay.com/api-keys" target="_blank">here</a>.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Transaction speed', 'codeshop').':</th>
										<td>
											<select id="codeshop_bitpay_speed" name="codeshop_bitpay_speed">
												<option value="high"'.($this->options['bitpay_speed'] == 'high' ? ' selected="selected"' : '').'>'.__('Immediate', 'codeshop').'</option>
												<option value="medium"'.($this->options['bitpay_speed'] == 'medium' ? ' selected="selected"' : '').'>'.__('After 1 block confirmation', 'codeshop').'</option>
												<option value="low"'.($this->options['bitpay_speed'] == 'low' ? ' selected="selected"' : '').'>'.__('After 6 block confirmations', 'codeshop').'</option>
											</select>
											<br /><em>'.__('Please set how fast an invoice is considered to be "confirmed".', 'codeshop').'</em></td>
									</tr>
								</table>
								<div class="alignright">
									<input type="submit" class="codeshop_button btn btn-primary" name="submit" value="'.__('Update Settings', 'codeshop').'">
								</div>
							</div>
						</div>
						<hr>
						<div class="postbox codeshop_postbox">
							<!--<div class="handlediv" title="Click to toggle"><br></div>-->
							<h3 class="hndle" style="cursor: default;"><span>'.__('Blockchain.info Settings', 'codeshop').'</span></h3>
							<div class="inside">
								<table class="codeshop_useroptions">
									<tr>
										<th>'.__('Enable', 'codeshop').':</th>
										<td><input type="checkbox" id="codeshop_enable_blockchain" name="codeshop_enable_blockchain" '.($this->options['enable_blockchain'] == "on" ? 'checked="checked"' : '').'> '.__('Accept bitcoins (through Blockchain.info API)', 'codeshop').'<br /><em>'.__('Please tick checkbox if you would like to accept bitcoins (using Blockchain.info API).', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Receive Payments API Key', 'codeshop').':</th>
										<td>
											<input type="text" id="codeshop_blockchain_api_key" name="codeshop_blockchain_api_key" value="'.htmlspecialchars($this->options['blockchain_api_key'], ENT_QUOTES).'" class="widefat">
											<br /><em>'.__('Please apply for an API key at <a href="https://api.blockchain.info/v2/apikey/request/" target="_blank">https://api.blockchain.info/v2/apikey/request/</a>. This API key is only for our Receive Payments API.', 'codeshop').'</em>
										</td>
									</tr>
									<tr>
										<th>'.__('xPub', 'codeshop').':</th>
										<td>
											<input type="text" id="codeshop_blockchain_xpub" name="codeshop_blockchain_xpub" value="'.htmlspecialchars($this->options['blockchain_xpub'], ENT_QUOTES).'" class="widefat">
											<br /><em>'.__('Please put Extended Public Key (xPub). This parameter is located in your Wallet: Settings -> Accounts & Addresses -> Show xPub.', 'codeshop').'</em>
										</td>
									</tr>
									<tr>
										<th>'.__('Secret', 'codeshop').':</th>
										<td>
											<input type="text" id="codeshop_blockchain_secret" name="codeshop_blockchain_secret" value="'.htmlspecialchars($this->options['blockchain_secret'], ENT_QUOTES).'" class="widefat">
											<br /><em>'.__('Please enter random alphanumeric string.', 'codeshop').'</em>
										</td>
									</tr>
									<tr>
										<th>'.__('Confirmations', 'codeshop').':</th>
										<td>
											<select id="codeshop_blockchain_confirmations" name="codeshop_blockchain_confirmations">';
		for ($i=0; $i<7; $i++) {
			echo '
												<option value="'.$i.'"'.($this->options['blockchain_confirmations'] == $i ? ' selected="selected"' : '').'>'.$i.'</option>';
		}
		echo '									
											</select>
											<br /><em>'.__('Please set the number of confirmations of transaction.', 'codeshop').'</em></td>
									</tr>
								</table>
								<div class="alignright">
									<input type="submit" class="codeshop_button btn btn-primary" name="submit" value="'.__('Update Settings', 'codeshop').'">
								</div>
							</div>
						</div>
						<hr>
						<div class="postbox codeshop_postbox">
							<!--<div class="handlediv" title="Click to toggle"><br></div>-->
							<h3 class="hndle" style="cursor: default;"><span>'.__('Stripe Settings', 'codeshop').'</span></h3>
							<div class="inside">
								<table class="codeshop_useroptions">
									<tr>
										<th>'.__('Enable', 'codeshop').':</th>
										<td><input type="checkbox" id="codeshop_enable_stripe" name="codeshop_enable_stripe" '.($this->options['enable_stripe'] == "on" ? 'checked="checked"' : '').'> '.__('Accept payments via Stripe', 'codeshop').'<br /><em>'.__('Please tick checkbox if you would like to accept payments via Stripe.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Secret Key', 'codeshop').':</th>
										<td><input type="text" id="codeshop_stripe_secret" name="codeshop_stripe_secret" value="'.htmlspecialchars($this->options['stripe_secret'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter valid Secret Key. You can get this parameter <a href="https://manage.stripe.com/account/apikeys" target="_blank">here</a>.', 'codeshop').'</em></td>
									</tr>
									<tr>
										<th>'.__('Publishable Key', 'codeshop').':</th>
										<td><input type="text" id="codeshop_stripe_publishable" name="codeshop_stripe_publishable" value="'.htmlspecialchars($this->options['stripe_publishable'], ENT_QUOTES).'" class="widefat"><br /><em>'.__('Please enter valid Publishable Key. You can get this parameter <a href="https://manage.stripe.com/account/apikeys" target="_blank">here</a>.', 'codeshop').'</em></td>
									</tr>
								</table>
								<div class="alignright">
									<input type="submit" class="codeshop_button btn btn-primary" name="submit" value="'.__('Update Settings', 'codeshop').'">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</div>
		<hr><br>';
	}

	function admin_campaigns() {
		global $wpdb;

		if (isset($_GET["s"])) $search_query = trim(stripslashes($_GET["s"]));
		else $search_query = "";
		
		$tmp = $wpdb->get_row("SELECT COUNT(*) AS total FROM ".$wpdb->prefix."cs_campaigns WHERE deleted = '0' AND user_id='".$_SESSION['user_id']."' ".((strlen($search_query) > 0) ? " AND title LIKE '%".addslashes($search_query)."%'" : ""), ARRAY_A);
		$total = $tmp["total"];
		$totalpages = ceil($total/CODESHOP_RECORDS_PER_PAGE);
		if ($totalpages == 0) $totalpages = 1;
		if (isset($_GET["p"])) $page = intval($_GET["p"]);
		else $page = 1;
		if ($page < 1 || $page > $totalpages) $page = 1;
		$switcher = $this->page_switcher(admin_url('admin.php').'?page=codeshop-campaigns'.((strlen($search_query) > 0) ? "&s=".rawurlencode($search_query) : ""), $page, $totalpages);

		$sql = "SELECT t1.*, t2.total_sales, t2.total_codes FROM ".$wpdb->prefix."cs_campaigns t1 LEFT JOIN (SELECT campaign_id, SUM(sales) AS total_sales, COUNT(*) AS total_codes FROM ".$wpdb->prefix."cs_codes WHERE  user_id='".$_SESSION['user_id']."'  AND deleted = '0' GROUP BY campaign_id) t2 ON t2.campaign_id = t1.id  WHERE t1.user_id='".$_SESSION['user_id']."' AND t1.deleted = '0'".((strlen($search_query) > 0) ? " AND t1.title LIKE '%".addslashes($search_query)."%'" : "")." ORDER BY t1.created DESC LIMIT ".(($page-1)*CODESHOP_RECORDS_PER_PAGE).", ".CODESHOP_RECORDS_PER_PAGE;
		$rows = $wpdb->get_results($sql, ARRAY_A);
		if (!empty($this->error)) $message = "<div class='error'><p>".$this->error."</p></div>";
		else if (!empty($this->info)) $message = "<div class='updated'><p>".$this->info."</p></div>";
		else $message = '';

		echo '
			<div class="wrap admin_codeshop_wrap">
				<h2>'.__('Campaigns', 'codeshop').'</h2>
				'.$message.'
				<form action="'.admin_url('admin.php').'" method="get" class="uap-filter-form codeshop-filter-form">
				<input type="hidden" name="page" value="codeshop-campaigns" />
				<label>'.__('Search:', 'codeshop').'</label>
				<div style="width:100%"><p><input type="text" name="s" value="'.htmlspecialchars($search_query, ENT_QUOTES).'" style="width: 310px;" class="form-control"></p>
				<input style="float:left; margin-right: 8px" type="submit" class="btn btn-success action" value="'.__('Search', 'codeshop').'" />
				'.((strlen($search_query) > 0) ? '<input type="button" class="btn btn-success action" value="'.__('Reset search results', 'codeshop').'" onclick="window.location.href=\''.admin_url('admin.php').'?page=codeshop-campaigns\';" />' : '').'
				</div>
				</form>
				<div class="codeshop_buttons"><a class="btn btn-gray" href="'.admin_url('admin.php').'?page=codeshop-add-campaign">'.__('Create Campaign', 'codeshop').'</a></div>
				<div class="codeshop_pageswitcher">'.$switcher.'</div>
				<br>
				<table class="codeshop_records table table-bordered table-condensed table-striped">
				<tr>
					<th>'.__('Title', 'codeshop').'</th>
					<th style="width: 230px;">'.__('Shortcode', 'codeshop').'</th>
					<th style="width: 80px;">'.__('Price', 'codeshop').'</th>
					<th style="width: 80px;">'.__('Sales', 'codeshop').'</th>
					<th style="width: 80px;">'.__('Codes', 'codeshop').'</th>
					<th style="width: 90px;">'.__('Action', 'codeshop').'</th>
				</tr>';
		if (sizeof($rows) > 0) {
			foreach ($rows as $row) {
				if (strlen($row['intro']) > 300) $intro = substr($row['intro'], 0, 280).' ...';
				else $intro = $row['intro'];
				echo '
				<tr>
					<td><strong>'.esc_html($row['title']).'</strong>'.($row['blocked'] == 1 ? ' <span class="codeshop-badge codeshop-badge-blocked">Blocked</span>' : '').(!empty($intro) ? '<br /><em style="font-size: 12px; line-height: 1; color: #888;">'.esc_html($intro).'</em>' : '').'</td>
					<td><input type="text" class="widefat" onclick="this.focus();this.select();" readonly="readonly" value="'.(defined('UAP_CORE') ? esc_html('<div align="center" class="codeshop" data-id="'.$row['id'].'" data-uid="'.$_SESSION['user_id'].'"></div>') : esc_html('[codeshop id="'.$row['id'].'"]')).'"></td>
					<td style="text-align: right;">'.($row['currency'] == 'BTC' ? number_format($row['price'], 4, ".", "") : number_format($row['price'], 2, ".", "")).($row['is_flexible'] == 1 ? '+' : '').' '.$row['currency'].'</td>
					<td style="text-align: right;">'.intval($row['total_sales']).'</td>
					<td style="text-align: right;">'.intval($row['total_codes']).'</td>
					<td style="text-align: center;">
						<a href="'.admin_url('admin.php').'?page=codeshop-add-campaign&id='.$row['id'].'" title="'.__('Edit campaign details', 'codeshop').'"><img src="'.plugins_url('/images/edit.png', __FILE__).'" alt="'.__('Edit campaign details', 'codeshop').'" border="0"></a>
						<a href="'.admin_url('admin.php').'?page=codeshop-codes&cid='.$row['id'].'" title="'.__('Codes', 'codeshop').'"><img src="'.plugins_url('/images/codes.png', __FILE__).'" alt="'.__('Codes', 'codeshop').'" border="0"></a>
						'.($row['blocked'] == 0 ? '<a href="'.admin_url('admin.php').'?action=codeshop-block-campaign&id='.$row['id'].'" title="'.__('Block campaign', 'codeshop').'"><img src="'.plugins_url('/images/block.png', __FILE__).'" alt="'.__('Block campaign', 'codeshop').'" border="0"></a>' : '<a href="'.admin_url('admin.php').'?action=codeshop-unblock-campaign&id='.$row['id'].'" title="'.__('Unblock campaign', 'codeshop').'"><img src="'.plugins_url('/images/unblock.png', __FILE__).'" alt="'.__('Unblock campaign', 'codeshop').'" border="0"></a>').'
						<a href="'.admin_url('admin.php').'?action=codeshop-delete-campaign&id='.$row['id'].'" title="'.__('Delete campaign', 'codeshop').'" onclick="return codeshop_continueOperation();"><img src="'.plugins_url('/images/delete.png', __FILE__).'" alt="'.__('Delete campaign', 'codeshop').'" border="0"></a>
					</td>
				</tr>';
			}
		} else {
			echo '
				<tr><td colspan="6" style="padding: 20px; text-align: center;">'.((strlen($search_query) > 0) ? __('No results found for', 'codeshop').' "<strong>'.htmlspecialchars($search_query, ENT_QUOTES).'</strong>"' : __('List is empty.', 'codeshop')).'</td></tr>';
		}
		echo '
				</table>
				<p><div class="codeshop_buttons"><a class="btn btn-gray" href="'.admin_url('admin.php').'?page=codeshop-add-campaign">'.__('Create Campaign', 'codeshop').'</a></div></p>
				<div class="codeshop_pageswitcher">'.$switcher.'</div>
				<div class="codeshop_legend">
				<p><strong>'.__('Reference:', 'codeshop').'</strong></p>
					<p><img src="'.plugins_url('/images/edit.png', __FILE__).'" alt="'.__('Edit campaign details', 'codeshop').'" border="0"> '.__('Edit campaign details', 'codeshop').'</p>
					<p><img src="'.plugins_url('/images/codes.png', __FILE__).'" alt="'.__('Codes', 'codeshop').'" border="0"> '.__('Show codes', 'codeshop').'</p>
					<p><img src="'.plugins_url('/images/block.png', __FILE__).'" alt="'.__('Block campaign', 'codeshop').'" border="0"> '.__('Block campaign', 'campaign').'</p>
					<p><img src="'.plugins_url('/images/unblock.png', __FILE__).'" alt="'.__('Unblock campaign', 'codeshop').'" border="0"> '.__('Unblock campaign', 'codeshop').'</p>
					<p><img src="'.plugins_url('/images/delete.png', __FILE__).'" alt="'.__('Delete campaign', 'codeshop').'" border="0"> '.__('Delete campaign', 'codeshop').'</p>
				</div>
			</div>
			<script type="text/javascript">
				function codeshop_continueOperation() {
					var answer = confirm("Do you really want to continue?")
					if (answer) return true;
					else return false;
				}
			</script>';
	}

	function admin_add_campaign() {
		global $wpdb;

		unset($id);
		$status = "";
		if (isset($_GET["id"]) && !empty($_GET["id"])) {
			$id = intval($_GET["id"]);
			$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE id = '".$id."' AND deleted = '0'", ARRAY_A);
			if (!$campaign_details) unset($id);
		}
		$errors = true;
		if (!empty($this->error)) $message = "<div class='error'><p>".$this->error."</p></div>";
		else if (!empty($this->info)) $message = "<div class='updated'><p>".$this->info."</p></div>";
		else $message = '';

		echo '
		<div class="wrap codeshop">
			<!--<h2>'.(!empty($id) ? __('Edit Campaign', 'codeshop') : __('Create Campaign', 'codeshop')).'</h2>-->
			'.$message.'
			<form enctype="multipart/form-data" method="post" style="margin: 0px" action="'.admin_url('admin.php').'">
			<div class="postbox-container" style="width: 100%;">
				<div class="metabox-holder">
					<div class="meta-box-sortables ui-sortable">
						<div class="postbox codeshop_postbox">
							<!--<div class="handlediv" title="Click to toggle"><br></div>-->
							<h3 class="hndle" style="cursor: default;"><span>'.(!empty($id) ? __('Edit Campaign Details', 'codeshop') : __('Create Campaign', 'codeshop')).'</span></h3>
							<div class="inside">
								<table class="codeshop_useroptions">
									<tr>
										<th>'.__('Title', 'codeshop').':</th>
										<td>
											<input type="text" name="codeshop_title" id="codeshop_title" value="'.(!empty($campaign_details) ? esc_html($campaign_details['title']) : esc_html($this->default_campaign['title'])).'" class="widefat">
											<br /><em>'.__('Please enter page title.', 'codeshop').'</em>
										</td>
									</tr>
									<tr>
										<th>'.__('Description', 'codeshop').':</th>
										<td>
											<textarea class="widefat" id="codeshop_intro" name="codeshop_intro" style="height: 120px;">'.(!empty($campaign_details) ? esc_html($campaign_details['intro']) : esc_html($this->default_campaign['intro'])).'</textarea><br />
											<em>'.__('Please enter page description. HTML allowed. Payment form is inserted below this content. Allowed keywords: {price}, {currency}.', 'codeshop').'</em>
										</td>
									</tr> 
									<tr>
										<th>'.__('Terms & Conditions', 'codeshop').':</th>
										<td>
											<textarea class="widefat" id="codeshop_terms" name="codeshop_terms" style="height: 120px;">'.(!empty($campaign_details) ? esc_html($campaign_details['terms']) : esc_html($this->default_campaign['terms'])).'</textarea><br />
											<em>'.__('Buyers must agree with Terms & Conditions to purchase the code. Leave this field blank if you do not need Terms & Conditions box to be shown.', 'codeshop').'</em>
										</td>
									</tr> 
									<tr>
										<th>'.__('Price', 'codeshop').':</th>
										<td>
											<input type="text" name="codeshop_price" id="codeshop_price" value="'.(!empty($campaign_details) ? ($campaign_details['currency'] == 'BTC' ? number_format($campaign_details['price'], 4, '.', '') : number_format($campaign_details['price'], 2, '.', '')) : ($campaign_details['currency'] == 'BTC' ? number_format($this->default_campaign['price'], 4, '.', '') : number_format($this->default_campaign['price'], 2, '.', ''))).'" style="width: 80px; text-align: right;">
											<select name="codeshop_currency" id="codeshop_currency" onchange="codeshop_supportedmethods();">';
		if (!empty($campaign_details)) {
			foreach ($this->currency_list as $currency) {
				echo '
												<option value="'.$currency.'"'.($currency == $campaign_details['currency'] ? ' selected="selected"' : '').'>'.$currency.'</option>';
			}
		} else {
			foreach ($this->currency_list as $currency) {
				echo '
												<option value="'.$currency.'"'.($currency == $this->default_campaign['currency'] ? ' selected="selected"' : '').'>'.$currency.'</option>';
			}
		}
		echo '								
											</select>
											<label id="codeshop_supported"></label>
											<br /><em>'.__('Set price of the code.', 'codeshop').'</em>
										</td>
									</tr>
									<!--<tr>
										<th>'.__('Flexible', 'codeshop').':</th>
										<td>
											<input type="checkbox" id="codeshop_is_flexible" name="codeshop_is_flexible" '.(!empty($campaign_details) ? ($campaign_details['is_flexible'] == 1 ? 'checked="checked"' : '') : ($this->default_campaign['is_flexible'] == 1 ? 'checked="checked"' : '')).'"> '.__('Flexible price', 'codeshop').'
											<br /><em>'.__('Please tick checkbox if you wish your buyers to set their own price for the code. In this case the value (set above) will be considered as minimum price limit.', 'codeshop').'</em>
										</td>
									</tr>
									<tr>
										<th>'.__('Allowed sales', 'codeshop').':</th>
										<td>
											<input type="text" name="codeshop_allowed_sales" id="codeshop_allowed_sales" value="'.(!empty($campaign_details) ? intval($campaign_details['allowed_sales']) : /*intval($this->default_campaign['allowed_sales'])*/1).'" style="width: 80px; text-align: right;">
											<br /><em>'.__('How many times each code can be sold. Set 0 (zero) to sell each code unlimited times.', 'codeshop').'</em>
										</td>
									</tr>-->
<tr>
        <th>'.__('Minimum Codes', 'codeshop').':</th>
        <td>
                <input type="text" name="codeshop_minimum_codes" id="codeshop_minimum_codes" value="'.(!empty($campaign_details) ? intval($campaign_details['minimum_codes']) : intval($this->default_campaign['minimum_codes'])).'" style="width: 80px; text-align: right;">
                <br /><em>'.__('(How many codes minimum user has to buy, e.g. 3)', 'codeshop').'</em>
        </td>
</tr>
<tr>
        <th>'.__('Maximum codes', 'codeshop').':</th>
        <td>
                <input type="text" name="codeshop_maximum_codes" id="codeshop_maximum_codes" value="'.(!empty($campaign_details) ? intval($campaign_details['maximum_codes']) : intval($this->default_campaign['maximum_codes'])).'" style="width: 80px; text-align: right;">
                <br /><em>'.__('How many codes maximum user can buy, e.g. 10 - the maximum is calculated based on number of codes available, for example, if only 5 codes available, then maximum is 5 etc.', 'codeshop').'</em>
        </td>
</tr>
									<tr>
										<th>'.__('"No Code" message', 'codeshop').':</th>
										<td>
											<input type="text" name="codeshop_no_codes" id="codeshop_no_codes" value="'.(!empty($campaign_details) ? esc_html($campaign_details['no_codes']) : esc_html($this->default_campaign['no_codes'])).'" class="widefat">
											<br /><em>'.__('Please enter the message which appears if there are now available codes.', 'codeshop').'</em>
										</td>
									</tr>
									<tr>
										<th>'.__('Successful payment confirmation', 'codeshop').':</th>
										<td>
											<textarea id="codeshop_success_email" name="codeshop_success_email" class="widefat" style="height: 120px;">'.(!empty($campaign_details) ? esc_html($campaign_details['success_email']) : esc_html($this->default_campaign['success_email'])).'</textarea>
											<br /><em>'.__('Thanksgiving e-mail message. You can use the following keywords: {payer_name}, {payer_email}, {amount}, {currency}, {campaign_title}, {code}.', 'codeshop').'</em>
										</td>
									</tr>
									<tr>
										<th>'.__('Failed payment confirmation', 'codeshop').':</th>
										<td>
											<textarea id="codeshop_failed_email" name="codeshop_failed_email" class="widefat" style="height: 120px;">'.(!empty($campaign_details) ? esc_html($campaign_details['failed_email']) : esc_html($this->default_campaign['failed_email'])).'</textarea>
											<br /><em>'.__('This e-mail message is sent to payer in case of failed/pending payment. You can use the following keywords: {payer_name}, {payer_email}, {payment_status}.', 'codeshop').'</em>
										</td>
									</tr>
									<tr>
										<th>'.__('Success URL', 'codeshop').':</th>
										<td>
											<input type="text" name="codeshop_success_url" id="codeshop_success_url" value="'.(!empty($campaign_details) ? esc_html($campaign_details['success_url']) : esc_html($this->default_campaign['success_url'])).'" class="widefat">
											<br /><em>'.__('Please enter success URL. All paeyrs will be redirected to this URL after successful payment.', 'codeshop').'</em>
										</td>
									</tr>
									<tr>
										<th>'.__('Failed URL', 'codeshop').':</th>
										<td>
											<input type="text" name="codeshop_failed_url" id="codeshop_failed_url" value="'.(!empty($campaign_details) ? esc_html($campaign_details['failed_url']) : esc_html($this->default_campaign['failed_url'])).'" class="widefat">
											<br /><em>'.__('Please enter failed URL. All paeyrs will be redirected to this URL in case of failed/cancelled payment.', 'codeshop').'</em>
										</td>
									</tr>
								</table>
								<div class="alignright">
								<input type="hidden" name="action" value="codeshop-update-campaign" />
								'.(!empty($campaign_details) ? '<input type="hidden" name="codeshop_id" value="'.$id.'" />' : '').'
								<input type="submit" class="codeshop_button btn btn-success" name="submit" value="'.__('Submit details', 'codeshop').'">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
			<script type="text/javascript">
				function codeshop_supportedmethods() {
					var paypal_currencies = new Array("'.implode('", "', $this->paypal_currency_list).'");
					var payza_currencies = new Array("'.implode('", "', $this->payza_currency_list).'");
					var egopay_currencies = new Array("'.implode('", "', $this->egopay_currency_list).'");
					var skrill_currencies = new Array("'.implode('", "', $this->skrill_currency_list).'");
					var bitpay_currencies = new Array("'.implode('", "', $this->bitpay_currency_list).'");
					var blockchain_currencies = new Array("'.implode('", "', $this->blockchain_currency_list).'");
					var stripe_currencies = new Array("'.implode('", "', $this->stripe_currency_list).'");
					var authnet_currencies = new Array("'.implode('", "', $this->authnet_currency_list).'");
					var interkassa_currencies = new Array("'.implode('", "', $this->interkassa_currency_list).'");
					var currency = jQuery("#codeshop_currency").val();
					var supported = "";
					if (currency == "USD") supported = supported + "<span style=\'padding: 4px; margin-right:4px\' class=\'label-success\'>Authorize.Net</span>";
					if (jQuery.inArray(currency, bitpay_currencies) >= 0) supported = supported + "<span style=\'padding: 4px; margin-right:4px\' class=\'label-success\'>BitPay</span>";
					if (jQuery.inArray(currency, blockchain_currencies) >= 0) supported = supported + "<span style=\'padding: 4px; margin-right:4px\' class=\'label-success\'>Blockchain.info</span>";
					if (jQuery.inArray(currency, egopay_currencies) >= 0) supported = supported + "<span style=\'padding: 4px; margin-right:4px\' class=\'label-success\'>EgoPay</span>";
					if (jQuery.inArray(currency, interkassa_currencies) >= 0) supported = supported +"<span style=\'padding: 4px; margin-right:4px\' class=\'label-success\'>InterKassa</span>";
					if (jQuery.inArray(currency, paypal_currencies) >= 0) supported = supported + "<span style=\'padding: 4px; margin-right:4px\' class=\'label-success\'>PayPal</span>";
					if (jQuery.inArray(currency, payza_currencies) >= 0) supported = supported + "<span style=\'padding: 4px; margin-right:4px\' class=\'label-success\'>Payza</span>";
					if (jQuery.inArray(currency, skrill_currencies) >= 0) supported = supported + "<span style=\'padding: 4px; margin-right:4px\' class=\'label-success\'>Skrill</span>";
					if (jQuery.inArray(currency, stripe_currencies) >= 0) supported = supported + "<span style=\'padding: 4px; margin-right:4px\' class=\'label-success\'>Stripe</span>";

					jQuery("#codeshop_supported").html(supported);
				}
				codeshop_supportedmethods();
			</script>			
		</div>';
	}

	function admin_codes() {
		global $wpdb;

		if (isset($_GET["s"])) $search_query = trim(stripslashes($_GET["s"]));
		else $search_query = "";
		if (isset($_GET["cid"])) {
			$campaign_id = intval($_GET["cid"]);
			$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE deleted = '0' AND id = '".$campaign_id."'", ARRAY_A);
			if (!$campaign_details) $campaign_id = 0;
		} else $campaign_id = 0;

		$tmp = $wpdb->get_row("SELECT COUNT(*) AS total FROM ".$wpdb->prefix."cs_codes WHERE deleted = '0' AND user_id='".$_SESSION['user_id']."' ".($campaign_id > 0 ? " AND campaign_id = '".$campaign_id."'" : "").((strlen($search_query) > 0) ? " AND code LIKE '%".addslashes($search_query)."%'" : ""), ARRAY_A);
		$total = $tmp["total"];
		$totalpages = ceil($total/CODESHOP_RECORDS_PER_PAGE);
		if ($totalpages == 0) $totalpages = 1;
		if (isset($_GET["p"])) $page = intval($_GET["p"]);
		else $page = 1;
		if ($page < 1 || $page > $totalpages) $page = 1;
		$switcher = $this->page_switcher(admin_url('admin.php').'?page=codeshop-codes'.($campaign_id > 0 ? "&cid=".$campaign_id : "").((strlen($search_query) > 0) ? "&s=".rawurlencode($search_query) : ""), $page, $totalpages);

		$sql = "SELECT t1.*, t2.title AS campaign_title, t2.deleted AS campaign_deleted, t2.allowed_sales FROM ".$wpdb->prefix."cs_codes t1 LEFT JOIN ".$wpdb->prefix."cs_campaigns t2 ON t1.campaign_id = t2.id WHERE t1.deleted = '0' AND t1.user_id='".$_SESSION['user_id']."' ".($campaign_id > 0 ? " AND t1.campaign_id = '".$campaign_id."'" : "").((strlen($search_query) > 0) ? " AND t1.code LIKE '%".addslashes($search_query)."%'" : "")." ORDER BY t1.created DESC LIMIT ".(($page-1)*CODESHOP_RECORDS_PER_PAGE).", ".CODESHOP_RECORDS_PER_PAGE;
		$rows = $wpdb->get_results($sql, ARRAY_A);
		if (!empty($this->error)) $message = "<div class='error'><p>".$this->error."</p></div>";
		else if (!empty($this->info)) $message = "<div class='updated'><p>".$this->info."</p></div>";
		else $message = '';

		echo '
			<div class="wrap admin_codeshop_wrap">
				<div id="icon-edit-pages" class="icon32"><br /></div><h2>'.__('Codes', 'codeshop').($campaign_id > 0 ? ' <span class="label-success">'.esc_html($campaign_details['title']).'</span>' : '').'</h2><br />
				'.$message.'
				<form action="'.admin_url('admin.php').'" method="get" class="uap-filter-form codeshop-filter-form">
				<input type="hidden" name="page" value="codeshop-codes" />
				<label>'.__('Search:', 'codeshop').'</label>
				<div style="width:100%"><p><input type="text" name="s" value="'.htmlspecialchars($search_query, ENT_QUOTES).'" style="width: 300px;" class="form-control"></p>
				<input style="float:left; margin-right: 8px" type="submit" class="btn btn-success action" value="'.__('Search', 'codeshop').'" />
				'.((strlen($search_query) > 0) ? '<input type="button" class="btn btn-success action" value="'.__('Reset search results', 'codeshop').'" onclick="window.location.href=\''.admin_url('admin.php').'?page=codeshop-codes\';" />' : '').'
				</div>
				</form>
				<div class="codeshop_buttons"><a class="btn btn-gray" href="'.admin_url('admin.php').'?page=codeshop-add-code'.($campaign_id > 0 ? '&cid='.$campaign_id : '').'">'.__('Add New Codes', 'codeshop').'</a></div>
				<div class="codeshop_pageswitcher">'.$switcher.'</div>
				<br>
				<table class="codeshop_records table table-bordered table-condensed table-striped">
				<tr>
					<th>'.__('Campaign', 'codeshop').'</th>
					<th>'.__('Code', 'codeshop').'</th>
					<th>'.__('Owners', 'codeshop').'</th>
					<th style="width: 80px;">'.__('Sales', 'codeshop').'</th>
					<th style="width: 70px;">'.__('Action', 'codeshop').'</th>
				</tr>';
		if (sizeof($rows) > 0) {
			foreach ($rows as $row) {
				$sql = "SELECT * FROM ".$wpdb->prefix."cs_transactions WHERE code_id = '".$row["id"]."' AND (payment_status = 'Completed' OR payment_status = 'Success' OR payment_status = 'confirmed' OR payment_status = 'complete') AND deleted = '0'";
				$owners = $wpdb->get_results($sql, ARRAY_A);
				$total_owners = sizeof($owners);
				if ($total_owners > 1) $owner = '<a href="'.admin_url('admin.php').'?action=codeshop-owners&id='.$row['id'].'" class="thickbox" title="'.esc_html($row['campaign_title']).'">'.__('Show All', 'codeshop').'</a>';
				else if ($total_owners == 1) $owner = $owners[0]['payer_email'];
				else $owner = '-';
				echo '
				<tr>
					<td>'.esc_html($row['campaign_title']).($row['campaign_deleted'] ? ' <span class="codeshop-lable-info">DEL</span>' : '').'</td>
					<td><strong>'.esc_html($row['code']).'</strong>'.($row['blocked'] == 1 ? ' <span class="codeshop-badge codeshop-badge-blocked">Blocked</span>' : '').'</td>
					<td>'.$owner.'</td>
					<td style="text-align: right;">'.intval($row['sales']).'</td>
					<td style="text-align: center;">
						<a href="'.admin_url('admin.php').'?page=codeshop-transactions&cid='.$row['id'].'" title="'.__('Payment transactions', 'codeshop').'"><img src="'.plugins_url('/images/transactions.png', __FILE__).'" alt="'.__('Payment transactions', 'codeshop').'" border="0"></a>
						'.($row['blocked'] == 0 ? '<a href="'.admin_url('admin.php').'?action=codeshop-block-code&id='.$row['id'].($campaign_id > 0 ? '&cid='.$campaign_id : '').'" title="'.__('Block code', 'codeshop').'"><img src="'.plugins_url('/images/block.png', __FILE__).'" alt="'.__('Block code', 'codeshop').'" border="0"></a>' : '<a href="'.admin_url('admin.php').'?action=codeshop-unblock-code&id='.$row['id'].($campaign_id > 0 ? '&cid='.$campaign_id : '').'" title="'.__('Unblock code', 'codeshop').'"><img src="'.plugins_url('/images/unblock.png', __FILE__).'" alt="'.__('Unblock code', 'codeshop').'" border="0"></a>').'
						<a href="'.admin_url('admin.php').'?action=codeshop-delete-code&id='.$row['id'].($campaign_id > 0 ? '&cid='.$campaign_id : '').'" title="'.__('Delete code', 'codeshop').'" onclick="return codeshop_continueOperation();"><img src="'.plugins_url('/images/delete.png', __FILE__).'" alt="'.__('Delete code', 'codeshop').'" border="0"></a>
					</td>
				</tr>';
			}
		} else {
			echo '
				<tr><td colspan="5" style="padding: 20px; text-align: center;">'.((strlen($search_query) > 0) ? __('No results found for', 'codeshop').' "<strong>'.htmlspecialchars($search_query, ENT_QUOTES).'</strong>"' : __('List is empty.', 'codeshop')).'</td></tr>';
		}
		echo '
				</table>
				<p><div class="codeshop_buttons"><a class="btn btn-gray" href="'.admin_url('admin.php').'?page=codeshop-add-code'.($campaign_id > 0 ? '&cid='.$campaign_id : '').'">'.__('Add New Codes', 'codeshop').'</a></div></p>
				<div class="codeshop_pageswitcher">'.$switcher.'</div>
				<div class="codeshop_legend">
				<p><strong>'.__('Reference:', 'codeshop').'</strong></p>
					<p><img src="'.plugins_url('/images/transactions.png', __FILE__).'" alt="'.__('Payment transactions', 'codeshop').'" border="0"> '.__('Show payment transactions', 'codeshop').'</p>
					<p><img src="'.plugins_url('/images/block.png', __FILE__).'" alt="'.__('Block code', 'codeshop').'" border="0"> '.__('Block code', 'codeshop').'</p>
					<p><img src="'.plugins_url('/images/unblock.png', __FILE__).'" alt="'.__('Unblock code', 'codeshop').'" border="0"> '.__('Unblock code', 'codeshop').'</p>
					<p><img src="'.plugins_url('/images/delete.png', __FILE__).'" alt="'.__('Delete code', 'codeshop').'" border="0"> '.__('Delete code', 'codeshop').'</p>
				</div>
			</div>
			<script type="text/javascript">
				function codeshop_continueOperation() {
					var answer = confirm("Do you really want to continue?")
					if (answer) return true;
					else return false;
				}
			</script>';
	}

	function admin_add_code() {
		global $wpdb;

		$campaigns = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE user_id='".$_SESSION['user_id']."' AND deleted = '0'", ARRAY_A);

		if (isset($_GET["cid"])) {
			$campaign_id = intval($_GET["cid"]);
			$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE deleted = '0' AND id = '".$campaign_id."'", ARRAY_A);
			if (!$campaign_details) $campaign_id = 0;
		} else $campaign_id = 0;

		if (!empty($this->error)) $message = "<div class='error'><p>".$this->error."</p></div>";
		else if (!empty($this->info)) $message = "<div class='updated'><p>".$this->info."</p></div>";
		else $message = '';

		echo '
		<div class="wrap codeshop">
		<!--<h2>'.__('Add Codes', 'codeshop').'</h2>-->	
			'.$message.'
			<form enctype="multipart/form-data" method="post" style="margin: 0px" action="'.admin_url('admin.php').'">
			<div class="postbox-container" style="width: 100%;">
				<div class="metabox-holder">
					<div class="meta-box-sortables ui-sortable">
						<div class="postbox codeshop_postbox">
							<!--<div class="handlediv" title="Click to toggle"><br></div>-->
							<h3 class="hndle" style="cursor: default;"><span>'.__('Add Codes', 'codeshop').'</span></h3>
							<div class="inside">
								<table class="codeshop_useroptions">
									<tr>
										<th>'.__('Title', 'codeshop').':</th>
										<td>
											<select id="codeshop_campaign_id" name="codeshop_campaign_id">';
		if (sizeof($campaigns) > 0) {
			foreach ($campaigns as $campaign) {
				echo '
												<option value="'.$campaign['id'].'"'.($campaign['id'] == $campaign_id ? ' selected="selected"' : '').'>'.esc_html($campaign['title']).' ('.($campaign["currency"] == 'BTC' ? number_format($campaign["price"], 4, ".", "") : number_format($campaign["price"], 2, ".", "")).' '.$campaign['currency'].' '.__('per code', 'codeshop').')</option>';
			}
		} else {
			echo '
												<option value="0">== '.__('No campaigns found. Create at least one campaign.', 'codeshop').' ==</option>';
		}
		echo '
											</select>
											<br /><em>'.(sizeof($campaigns) > 0 ? __('Select code campaign.', 'codeshop') : __('Please create at least one code campaign.', 'codeshop')).'</em>
										</td>
									</tr>
									<tr>
										<th>'.__('Codes', 'codeshop').':</th>
										<td>
											<textarea class="widefat" id="codeshop_codes" name="codeshop_codes" style="height: 200px;"></textarea>
											<br /><em>'.__('Please enter list of codes. <strong>Important! One code per line.</strong>', 'codeshop').'</em>
										</td>
									</tr> 
								</table>
								<div class="alignright">
								<input type="hidden" name="action" value="codeshop-add-code" />
								'.($campaign_id > 0 ? '<input type="hidden" name="codeshop_cid" value="'.$campaign_id.'" />' : '').'
								<input type="submit" class="codeshop_button btn btn-success" name="submit" value="'.__('Add New Codes', 'codeshop').'">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</div>';
	}

	function admin_transactions() {
		global $wpdb;

		if (!empty($this->error)) $message = "<div class='error'><p>".$this->error."</p></div>";
		else if (!empty($this->info)) $message = "<div class='updated'><p>".$this->info."</p></div>";
		else $message = '';

		if (isset($_GET["s"])) $search_query = trim(stripslashes($_GET["s"]));
		else $search_query = "";
		if (isset($_GET["cid"])) {
			$code_id = intval($_GET["cid"]);
			$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE deleted = '0' AND id = '".$code_id."'", ARRAY_A);
			if (!$code_details) $code_id = 0;
		} else $code_id = 0;
		
		$tmp = $wpdb->get_row("SELECT COUNT(*) AS total FROM ".$wpdb->prefix."cs_transactions WHERE deleted = '0'".($code_id > 0 ? " AND code_id = '".$code_id."'" : "").((strlen($search_query) > 0) ? " AND (payer_name LIKE '%".addslashes($search_query)."%' OR payer_email LIKE '%".addslashes($search_query)."%')" : ""), ARRAY_A);
		$total = $tmp["total"];
		$totalpages = ceil($total/CODESHOP_RECORDS_PER_PAGE);
		if ($totalpages == 0) $totalpages = 1;
		if (isset($_GET["p"])) $page = intval($_GET["p"]);
		else $page = 1;
		if ($page < 1 || $page > $totalpages) $page = 1;
		$switcher = $this->page_switcher(admin_url('admin.php').'?page=codeshop-transactions'.((strlen($search_query) > 0) ? "&s=".rawurlencode($search_query) : "").($code_id > 0 ? "&cid=".$code_id : ""), $page, $totalpages);

		$sql = "SELECT t1.*, t2.code AS code FROM ".$wpdb->prefix."cs_transactions t1 LEFT JOIN ".$wpdb->prefix."cs_codes t2 ON t1.code_id = t2.id WHERE t1.user_id='".$_SESSION['user_id']."' AND t1.deleted = '0'".($code_id > 0 ? " AND t1.code_id = '".$code_id."'" : "").((strlen($search_query) > 0) ? " AND (t1.payer_name LIKE '%".addslashes($search_query)."%' OR t1.payer_email LIKE '%".addslashes($search_query)."%')" : "")." ORDER BY t1.created DESC LIMIT ".(($page-1)*CODESHOP_RECORDS_PER_PAGE).", ".CODESHOP_RECORDS_PER_PAGE;
                
		$rows = $wpdb->get_results($sql, ARRAY_A);

		echo '
			<div class="wrap admin_codeshop_wrap">
				<div id="icon-edit-pages" class="icon32"><br /></div><h2>'.__('Transactions', 'codeshop').($code_id > 0 ? ' <span class="label-success">'.esc_html($code_details['code']).'</span>' : '').'</h2><br />
				'.$message.'
				<form action="'.admin_url('admin.php').'" method="get" class="uap-filter-form codeshop-filter-form">
				<input type="hidden" name="page" value="codeshop-transactions" />
				'.($code_id > 0 ? '<input type="hidden" name="cid" value="'.$code_id.'" />' : '').'
				<label>'.__('Search:', 'codeshop').'</label>
				<p><input type="text" name="s" value="'.htmlspecialchars($search_query, ENT_QUOTES).'" style="width: 200px;" class="form-control"></p>
				<input type="submit" class="btn btn-success action" value="'.__('Search', 'codeshop').'" />
				'.((strlen($search_query) > 0) ? '<input type="button" class="btn btn-success action" value="'.__('Reset search results', 'codeshop').'" onclick="window.location.href=\''.admin_url('admin.php').'?page=codeshop-transactions'.($code_id > 0 ? '&cid='.$code_id : '').'\';" />' : '').'
				</form>
				<div class="codeshop_pageswitcher">'.$switcher.'</div>
				<br>
				<table class="codeshop_records table table-bordered table-condensed table-striped">
				<tr>
					<th>'.__('Code', 'codeshop').'</th>
					<th>'.__('Payer', 'codeshop').'</th>
					<th style="width: 100px;">'.__('Amount', 'codeshop').'</th>
					<th style="width: 120px;">'.__('Status', 'codeshop').'</th>
					<th style="width: 130px;">'.__('Created', 'codeshop').'</th>
					<th style="width: 20px;"></th>
				</tr>';
		if (sizeof($rows) > 0) {
			foreach ($rows as $row) {
				echo '
				<tr>
					<td>'.esc_html($row['code']).'</td>
					<td>'.esc_html($row['payer_name']).'<br /><em>'.esc_html($row['payer_email']).'</em></td>
					<td style="text-align: right;">'.($row['currency'] == 'BTC' ? number_format($row['gross'], 4, ".", "") : number_format($row['gross'], 2, ".", "")).' '.$row['currency'].'</td>
					<td><a href="'.admin_url('admin.php').'?action=codeshop-transactiondetails&id='.$row['id'].'" class="thickbox" title="Transaction Details">'.$row["payment_status"].'</a><br /><em>'.$row["transaction_type"].'</em></td>
					<td>'.date("Y-m-d H:i", $row["created"]).'</td>
					<td style="text-align: center;">
						<a href="'.admin_url('admin.php').'?action=codeshop-delete-txn&id='.$row['id'].'" title="'.__('Delete transaction', 'codeshop').'" onclick="return codeshop_continueOperation();"><img src="'.plugins_url('/images/delete.png', __FILE__).'" alt="'.__('Delete transaction', 'codeshop').'" border="0"></a>
					</td>
					
				</tr>';
			}
		} else {
			echo '
				<tr><td colspan="6" style="padding: 20px; text-align: center;">'.((strlen($search_query) > 0) ? __('No results found for', 'codeshop').' "<strong>'.htmlspecialchars($search_query, ENT_QUOTES).'</strong>"' : __('List is empty.', 'codeshop')).'</td></tr>';
		}
		echo '
				</table>
				<div class="codeshop_pageswitcher">'.$switcher.'</div>
			</div>
			<script type="text/javascript">
				function codeshop_continueOperation() {
					var answer = confirm("Do you really want to continue?")
					if (answer) return true;
					else return false;
				}
			</script>';
	}

	function admin_using() {
		global $wpdb;
		$remote_snippet = '<script id="codeshop-remote" src="'.plugins_url('/js/remote.js', __FILE__).'?ver='.CODESHOP_VERSION.'" data-handler="'.admin_url('admin-ajax.php').'"></script>';
		echo '
		<div class="wrap codeshop">
			<h2>How To Use</h2>
			<div class="codeshop_postbox">
				<h3>Embedding Code Shop onto website</h3>
				<p>To embed Code Shop onto any website you need perform the following steps:</p>
				<ol>
				<li>Make sure that your website loads jQuery version 1.8 or higher.</li>
				<li>Make sure that your website has DOCTYPE. If not, add the following line as a first line of HTML-document:
				<pre>&lt;!DOCTYPE html&gt;</pre>
				</li>
				<li><strong>Copy the following JS-snippet and paste it into your website. You need paste it at the end of <code>&lt;body&gt;</code> section (above closing <code>&lt;/body&gt;</code> tag).</strong>
				<input style="margin-top:10px;margin-bottom:10px" class="widefat copy-container" readonly="readonly" onclick="this.focus();this.select();" value="'.esc_html($remote_snippet).'" /></li>
				<li>That\'s it. Integration finished. :-)</li>
				</ol>
				<h3>Using Code Shop</h3>
				<p>Copy campaign\'s shortcode and paste it where payment form must be. Shortcodes can be found on <a href="'.admin_url('admin.php').'?page=codeshop-campaigns">Campaigns</a> page.</p>
			</div>
		</div>';
	}
	
	function admin_request_handler() {
		global $wpdb;
                $user_id = isset($_SESSION['user_id'])?$_SESSION['user_id']:0;
		if (!empty($_POST['action'])) {
			switch($_POST['action']) {
				case 'codeshop-update-options':
					$this->populate_options();
					if (isset($_POST["codeshop_enable_paypal"])) $this->options['enable_paypal'] = "on";
					else $this->options['enable_paypal'] = "off";
					if (isset($_POST["codeshop_enable_payza"])) $this->options['enable_payza'] = "on";
					else $this->options['enable_payza'] = "off";
					if (isset($_POST["codeshop_payza_sandbox"])) $this->options['payza_sandbox'] = "on";
					else $this->options['payza_sandbox'] = "off";
					if (isset($_POST["codeshop_enable_skrill"])) $this->options['enable_skrill'] = "on";
					else $this->options['enable_skrill'] = "off";
					if (isset($_POST["codeshop_enable_interkassa"])) $this->options['enable_interkassa'] = "on";
					else $this->options['enable_interkassa'] = "off";
					if (isset($_POST["codeshop_paypal_address"])) $this->options['paypal_address'] = "on";
					else $this->options['paypal_address'] = "off";
					if (isset($_POST["codeshop_enable_authnet"])) $this->options['enable_authnet'] = "on";
					else $this->options['enable_authnet'] = "off";
					if (isset($_POST["codeshop_authnet_sandbox"])) $this->options['authnet_sandbox'] = "on";
					else $this->options['authnet_sandbox'] = "off";
					if (isset($_POST["codeshop_enable_egopay"])) $this->options['enable_egopay'] = "on";
					else $this->options['enable_egopay'] = "off";
					if (isset($_POST["codeshop_enable_bitpay"])) $this->options['enable_bitpay'] = "on";
					else $this->options['enable_bitpay'] = "off";
					if (isset($_POST["codeshop_enable_blockchain"])) $this->options['enable_blockchain'] = "on";
					else $this->options['enable_blockchain'] = "off";
					if (isset($_POST["codeshop_enable_stripe"])) $this->options['enable_stripe'] = "on";
					else $this->options['enable_stripe'] = "off";
					$errors = $this->check_options();
					if ($errors === true) {
						$this->update_options();
						setcookie("codeshop_info", __('Settings successfully <strong>updated</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop');
						die();
					} else {
						$this->update_options();
						$message = "";
						if (is_array($errors)) $message = __('The following error(s) occured:', 'codeshop').'<br />- '.implode('<br />- ', $errors);
						setcookie("codeshop_error", $message, time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop');
						die();
					}
					break;

				case 'codeshop-update-campaign':
					unset($id);
					if (isset($_POST["codeshop_id"]) && !empty($_POST["codeshop_id"])) {
						$id = intval($_POST["codeshop_id"]);
						$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE id = '".$id."' AND deleted = '0'", ARRAY_A);
						if (!$campaign_details) unset($id);
					}
					$title = trim(stripslashes($_POST["codeshop_title"]));
					if (empty($title)) $title = $this->default_campaign['title'];
					$intro = trim(stripslashes($_POST["codeshop_intro"]));
					$terms = trim(stripslashes($_POST["codeshop_terms"]));
					$no_codes = trim(stripslashes($_POST["codeshop_no_codes"]));
					if (isset($_POST["codeshop_is_flexible"])) $is_flexible = 1;
					else $is_flexible = 0;
					$success_email = trim(stripslashes($_POST["codeshop_success_email"]));
					$failed_email = trim(stripslashes($_POST["codeshop_failed_email"]));
					$price = trim(stripslashes($_POST["codeshop_price"]));
					$currency = trim(stripslashes($_POST["codeshop_currency"]));
					$price = floatval($price);
					if ($price < 0) $price = 0;
					$price = $currency == 'BTC' ? number_format($price, 4, '.', '') : number_format($price, 2, '.', '');
					$allowed_sales = intval(trim($_POST["codeshop_allowed_sales"]));
					$minimum_codes = intval(trim($_POST["codeshop_minimum_codes"]));
					$maximum_codes = intval(trim($_POST["codeshop_maximum_codes"]));
					$success_url = trim(stripslashes($_POST["codeshop_success_url"]));
					if (!preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $success_url) || strlen($success_url) == 0) $success_url = defined('UAP_CORE') ? '' : get_bloginfo('url');
					$failed_url = trim(stripslashes($_POST["codeshop_failed_url"]));
					if (!preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $failed_url) || strlen($failed_url) == 0) $failed_url = defined('UAP_CORE') ? '' : get_bloginfo('url');
					
					if (!empty($id)) {
						$sql = "UPDATE ".$wpdb->prefix."cs_campaigns SET 
							user_id = '".esc_sql($user_id)."', 
							title = '".esc_sql($title)."', 
							intro = '".esc_sql($intro)."', 
							terms = '".esc_sql($terms)."', 
							price = '".$price."',
							currency = '".$currency."',
							is_flexible = '".$is_flexible."',
							allowed_sales = '".$allowed_sales."',
							minimum_codes = '".$minimum_codes."',
							maximum_codes = '".$maximum_codes."',
							no_codes = '".esc_sql($no_codes)."',
							success_email = '".esc_sql($success_email)."',
							failed_email = '".esc_sql($failed_email)."',
							success_url = '".esc_sql($success_url)."',
							failed_url = '".esc_sql($failed_url)."'
							WHERE id = '".$id."'";
						if ($wpdb->query($sql) !== false) {
							setcookie("codeshop_info", __('Campaign details successfully <strong>updated</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
							header('Location: '.admin_url('admin.php').'?page=codeshop-campaigns');
							exit;
						} else {
							setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
							header('Location: '.admin_url('admin.php').'?page=codeshop-add-campaign'.(!empty($id) ? '&id='.$id : ''));
							exit;
						}
					} else {
						$str_id = $this->random_string(16);
						$sql = "INSERT INTO ".$wpdb->prefix."cs_campaigns (
							user_id,title, intro, terms, price, currency, is_flexible, allowed_sales, minimum_codes, maximum_codes,no_codes, success_email, failed_email, success_url, failed_url, created, blocked, deleted) VALUES (
							'".esc_sql($user_id)."',
							'".esc_sql($title)."',
							'".esc_sql($intro)."',
							'".esc_sql($terms)."',
							'".$price."',
							'".$currency."',
							'".$is_flexible."',
							'".$allowed_sales."',
							'".$minimum_codes."',
							'".$maximum_codes."',
							'".esc_sql($no_codes)."',
							'".esc_sql($success_email)."',
							'".esc_sql($failed_email)."',
							'".esc_sql($success_url)."',
							'".esc_sql($failed_url)."',
							'".time()."',
							'0', '0'
							)";
						if ($wpdb->query($sql) !== false) {
							setcookie("codeshop_info", __('New campaign successfully <strong>created</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
							header('Location: '.admin_url('admin.php').'?page=codeshop-campaigns');
							exit;
						} else {
							setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
							header('Location: '.admin_url('admin.php').'?page=codeshop-add-campaign'.(!empty($id) ? '&id='.$id : ''));
							exit;
						}
					}
					break;

				case 'codeshop-add-code':
					unset($id);
					if (isset($_POST["codeshop_cid"])) {
						$cid = intval($_GET["cid"]);
						$tmp = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE deleted = '0' AND id = '".$cid."'", ARRAY_A);
						if (!$tmp) $cid = 0;
					} else $cid = 0;

					$errors = array();
					$campaign_id = intval($_POST['codeshop_campaign_id']);
					$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE id = '".$campaign_id."' AND user_id='".$user_id."' AND deleted = '0'", ARRAY_A);
					if (!$campaign_details) $errors[] = __('Invalid campaign', 'codeshop');

					$codes = explode("\n", trim(stripslashes($_POST['codeshop_codes'])));
					$valid_codes = array();
					foreach($codes as $code) {
						$code = trim($code);
						if (strlen($code) > 0 && strlen($code) < 65535) {
							$valid_codes[] = $code;
						}
					}
					if (empty($valid_codes)) $errors[] = __('Submit at least one valid code', 'codeshop');
					
					if (sizeof($errors) > 0) {
						$message = "";
						$message = __('The following error(s) occured:', 'codeshop').'<br />- '.implode('<br />- ', $errors);
						setcookie("codeshop_error", $message, time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-add-code'.($cid > 0 ? '&cid='.$cid : ''));
						exit;
					} else {
						$block_str = $this->random_string(16);
						$added = 0;
						foreach($valid_codes as $code) {
							$sql = "INSERT INTO ".$wpdb->prefix."cs_codes (
								user_id,
								campaign_id,
								block_str,
								code,
								sales,
								created,
								blocked,
								deleted
								) VALUES (
								'".$user_id."', 
								'".$campaign_id."', 
								'".esc_sql($block_str)."', 
								'".esc_sql($code)."', 
								'0', '".time()."', '0', '0')";
							if (!$wpdb->query($sql)) {
								setcookie("codeshop_error", $added.' '.($added == 1 ? __('code', 'codeshop') : __('codes', 'codeshop')).' '.__('<strong>added</strong> to campaign', 'codeshop').' <strong>'.esc_html($campaign_details['title']).'</strong>', time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
								header('Location: '.admin_url('admin.php').'?page=codeshop-codes'.($cid > 0 ? '&cid='.$cid : ''));
								exit;
							}
							$added++;
						}
						setcookie("codeshop_info", $added.' '.($added == 1 ? __('code', 'codeshop') : __('codes', 'codeshop')).' '.__('<strong>added</strong> to campaign', 'codeshop').' <strong>'.esc_html($campaign_details['title']).'</strong>', time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-codes'.($cid > 0 ? '&cid='.$cid : ''));
						exit;
					}
					break;

				default:
					break;
			}
		}
		if (!empty($_GET['action'])) {
			switch($_GET['action']) {
				case 'codeshop-delete-campaign':
					$id = intval($_GET["id"]);
					$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE id = '".$id."' AND deleted = '0'", ARRAY_A);
					if (!$campaign_details) {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-campaigns');
						die();
					}
					$sql = "UPDATE ".$wpdb->prefix."cs_campaigns SET deleted = '1' WHERE id = '".$id."'";
					if ($wpdb->query($sql) !== false) {
						setcookie("codeshop_info", __('Campaign successfully <strong>deleted</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-campaigns');
						die();
					} else {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-campaigns');
						die();
					}
					break;
				case 'codeshop-block-campaign':
					$id = intval($_GET["id"]);
					$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE id = '".$id."' AND deleted = '0'", ARRAY_A);
					if (!$campaign_details) {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-campaigns');
						die();
					}
					$sql = "UPDATE ".$wpdb->prefix."cs_campaigns SET blocked = '1' WHERE id = '".$id."'";
					if ($wpdb->query($sql) !== false) {
						setcookie("codeshop_info", __('Campaign successfully <strong>blocked</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-campaigns');
						die();
					} else {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-campaigns');
						die();
					}
					break;
				case 'codeshop-unblock-campaign':
					$id = intval($_GET["id"]);
					$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE id = '".$id."' AND deleted = '0'", ARRAY_A);
					if (!$campaign_details) {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-campaigns');
						die();
					}
					$sql = "UPDATE ".$wpdb->prefix."cs_campaigns SET blocked = '0' WHERE id = '".$id."'";
					if ($wpdb->query($sql) !== false) {
						setcookie("codeshop_info", __('Campaign successfully <strong>unblocked</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-campaigns');
						die();
					} else {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-campaigns');
						die();
					}
					break;
				case 'codeshop-delete-code':
					if (isset($_GET["cid"])) {
						$campaign_id = intval($_GET["cid"]);
						$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE deleted = '0' AND id = '".$campaign_id."'", ARRAY_A);
						if (!$campaign_details) $campaign_id = 0;
					} else $campaign_id = 0;
					$id = intval($_GET["id"]);
					$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE id = '".$id."' AND deleted = '0'", ARRAY_A);
					if (intval($code_details["id"]) == 0) {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-codes'.($campaign_id > 0 ? '&cid='.$campaign_id : ''));
						die();
					}
					$sql = "UPDATE ".$wpdb->prefix."cs_codes SET deleted = '1' WHERE id = '".$id."'";
					if ($wpdb->query($sql) !== false) {
						setcookie("codeshop_info", __('Code successfully <strong>deleted</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-codes'.($campaign_id > 0 ? '&cid='.$campaign_id : ''));
						die();
					} else {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-codes'.($campaign_id > 0 ? '&cid='.$campaign_id : ''));
						die();
					}
					break;
				case 'codeshop-block-code':
					if (isset($_GET["cid"])) {
						$campaign_id = intval($_GET["cid"]);
						$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE deleted = '0' AND id = '".$campaign_id."'", ARRAY_A);
						if (!$campaign_details) $campaign_id = 0;
					} else $campaign_id = 0;
					$id = intval($_GET["id"]);
					$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE id = '".$id."' AND deleted = '0'", ARRAY_A);
					if (intval($code_details["id"]) == 0) {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-codes'.($campaign_id > 0 ? '&cid='.$campaign_id : ''));
						die();
					}
					$sql = "UPDATE ".$wpdb->prefix."cs_codes SET blocked = '1' WHERE id = '".$id."'";
					if ($wpdb->query($sql) !== false) {
						setcookie("codeshop_info", __('Code successfully <strong>blocked</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-codes'.($campaign_id > 0 ? '&cid='.$campaign_id : ''));
						die();
					} else {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-codes'.($campaign_id > 0 ? '&cid='.$campaign_id : ''));
						die();
					}
					break;
				case 'codeshop-unblock-code':
					if (isset($_GET["cid"])) {
						$campaign_id = intval($_GET["cid"]);
						$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE deleted = '0' AND id = '".$campaign_id."'", ARRAY_A);
						if (!$campaign_details) $campaign_id = 0;
					} else $campaign_id = 0;
					$id = intval($_GET["id"]);
					$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE id = '".$id."' AND deleted = '0'", ARRAY_A);
					if (intval($code_details["id"]) == 0) {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-codes'.($campaign_id > 0 ? '&cid='.$campaign_id : ''));
						die();
					}
					$sql = "UPDATE ".$wpdb->prefix."cs_codes SET blocked = '0' WHERE id = '".$id."'";
					if ($wpdb->query($sql) !== false) {
						setcookie("codeshop_info", __('Code successfully <strong>unblocked</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-codes'.($campaign_id > 0 ? '&cid='.$campaign_id : ''));
						die();
					} else {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-codes'.($campaign_id > 0 ? '&cid='.$campaign_id : ''));
						die();
					}
					break;
				case 'codeshop-delete-txn':
					if (isset($_GET["cid"])) {
						$code_id = intval($_GET["cid"]);
						$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE deleted = '0' AND id = '".$code_id."'", ARRAY_A);
						if (!$code_details) $code_id = 0;
					} else $code_id = 0;
					$id = intval($_GET["id"]);
					$txn_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_transactions WHERE id = '".$id."' AND deleted = '0'", ARRAY_A);
					if (intval($txn_details["id"]) == 0) {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-transactions');
						die();
					}
					$sql = "UPDATE ".$wpdb->prefix."cs_transactions SET deleted = '1' WHERE id = '".$id."'";
					if ($wpdb->query($sql) !== false) {
						setcookie("codeshop_info", __('Transaction successfully <strong>deleted</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-transactions'.($code_id > 0 ? '&cid='.$code_id : ''));
						die();
					} else {
						setcookie("codeshop_error", __('Service is <strong>not available</strong>.', 'codeshop'), time()+30, "/", ".".str_replace("www.", "", $_SERVER["SERVER_NAME"]));
						header('Location: '.admin_url('admin.php').'?page=codeshop-transactions'.($code_id > 0 ? '&cid='.$code_id : ''));
						die();
					}
					break;
				case 'codeshop-transactiondetails':
					if (isset($_GET["id"]) && !empty($_GET["id"])) {
						$id = intval($_GET["id"]);
						$transaction_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_transactions WHERE id = '".$id."' AND deleted = '0'", ARRAY_A);
						if (intval($transaction_details["id"]) != 0) {
							echo '
<html>
<head>
	<title>'.__('Transaction Details', 'codeshop').'</title>
</head>
<body>
	<table style="width: 100%;">';
							$details = explode("&", $transaction_details["details"]);
							foreach ($details as $param) {
								$data = explode("=", $param, 2);
								echo '
		<tr>
			<td style="width: 170px; font-weight: bold;">'.esc_html($data[0]).'</td>
			<td>'.esc_html(urldecode($data[1])).'</td>
		</tr>';
							}
							echo '
	</table>						
</body>
</html>';
						} else echo __('No data found!', 'codeshop');
					} else echo __('No data found!', 'codeshop');
					die();
					break;
					
				case 'codeshop-owners':
					if (isset($_GET["id"]) && !empty($_GET["id"])) {
						$id = intval($_GET["id"]);
						$owners = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."cs_transactions WHERE code_id = '".$id."' AND (payment_status = 'Completed' OR payment_status = 'Success' OR payment_status = 'confirmed' OR payment_status = 'complete') AND deleted = '0'", ARRAY_A);
						if (sizeof($owners) > 0) {
							echo '
<html>
<head>
	<title>'.__('Owners of the code', 'codeshop').'</title>
</head>
<body>
	<table style="width: 100%;">';
							foreach ($owners as $owner) {
								echo '
		<tr>
			<td><strong>'.esc_html($owner['payer_name']).'</strong></td>
			<td>'.esc_html($owner['payer_email']).'</td>
		</tr>';
							}
							echo '
	</table>						
</body>
</html>';
						} else echo __('No data found!', 'codeshop');
					} else echo __('No data found!', 'codeshop');
					die();
					break;
				
				default:
					break;
					
			}
		}
	}

	function admin_warning() {
		echo '
		<div class="updated"><p><strong>Code Shop</strong> plugin is almost ready. You must do some <a href="'.admin_url('admin.php').'?page=codeshop">settings</a> to make it work properly.</p></div>';
	}

	function front_header() {
		echo '
		<script>
			var codeshop_action = "'.admin_url('admin-ajax.php').'";
			var codeshop_stripe_publishable = "'.esc_html($this->options['stripe_publishable']).'";
		</script>';
	}

	function front_footer() {
		echo '
<script>
if (typeof codeshop_init == "function") { 
	codeshop_init();
} else {
	jQuery(document).ready(function(){codeshop_init();});
}
</script>';
	}

	function front_enqueue_scripts() {
		wp_enqueue_script('jquery');
		wp_enqueue_script('codeshop', plugins_url('/js/script.js', __FILE__), array(), CODESHOP_VERSION);
		wp_enqueue_style('codeshop', plugins_url('/css/style.css', __FILE__), array(), CODESHOP_VERSION);
		if ($this->options['enable_stripe'] == 'on') wp_enqueue_script('stripe-checkout', 'https://checkout.stripe.com/v2/checkout.js');
	}

	function front_wp() {
		global $wpdb, $post;
		add_action('wp_enqueue_scripts', array(&$this, 'front_enqueue_scripts'));
		add_action('wp_head', array(&$this, 'front_header'), 15);
		add_action('wp_footer', array(&$this, "front_footer"));
	}

	function front_init() {
		global $wpdb;
		$campaign_title = 'Unknown';
		if (isset($_GET['codeshop-ipn']) && $_GET['codeshop-ipn'] == 'payza') {
			$token = "token=".urlencode($_POST['token']);
			$response = '';
			$ch = curl_init();
			//curl_setopt($ch, CURLOPT_URL, ($this->options['payza_sandbox'] == 'on' ? "https://sandbox.payza.com/sandbox/IPN2.ashx" : "https://secure.payza.com/ipn2.ashx"));
			//curl_setopt($ch, CURLOPT_URL, ("https://sandbox.payza.com/sandbox/IPN2.ashx"));
			curl_setopt($ch, CURLOPT_URL, ("https://secure.payza.com/ipn2.ashx"));
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $token);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_TIMEOUT, 60);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec($ch);
			if(strlen($response) > 0) {
				if(urldecode($response) == "INVALID TOKEN") {
					//the token is not valid
				} else {
					$response = urldecode($response);
					$aps = explode("&", $response);
					$info = array();
					foreach ($aps as $ap) {
						$ele = explode("=", $ap);
						$info[$ele[0]] = $ele[1];
					}

					$item_number = intval(str_replace("ID", "", $info['ap_itemcode']));
					$uid = $info['ap_uid'];
					$item_name = $info['ap_itemname'];
					$payment_status = $info['ap_status'];
					$transaction_type = $info['ap_transactiontype'];
					$txn_id = $info['ap_referencenumber'];
					$seller_id = $info['ap_merchant'];
					$payer_id = $info['ap_custemailaddress'];
					$gross_total = $info['ap_totalamount'];
					$mc_currency = $info['ap_currency'];
					$payer_name = $info['ap_custfirstname'].' '.$info['ap_custlastname'];
					$payer_email = $info['apc_1'];

					if ($payment_status == "Success") {
						$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE id = '".intval($item_number)."'", ARRAY_A);
						if (!$campaign_details) $payment_status = "Unrecognized";
						else {
							$campaign_title = $campaign_details["title"];
							if (strtolower($seller_id) != strtolower($this->options['payza_id'])) $payment_status = "Unrecognized";
							else {
								if (floatval($gross_total) < floatval($campaign_details["price"]) || $mc_currency != $campaign_details["currency"]) $payment_status = "Unrecognized";
							}
						}
					}
					$code_id = 0;
					if ($payment_status == "Success") {
						$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE deleted = '0' AND blocked = '0' AND campaign_id = '".$item_number."'".(intval($campaign_details['allowed_sales']) > 0 ? " AND sales < '".intval($campaign_details['allowed_sales'])."'" : "")." ORDER BY sales ASC", ARRAY_A);
						if (!$code_details) {
							$payment_status = 'No codes available';
						} else {
							$payment_status = 'Completed';
							$code_id = $code_details['id'];
							$wpdb->query("UPDATE ".$wpdb->prefix."cs_codes SET sales = sales + 1 WHERE id = '".$code_id."'");
						}
					}
					$sql = "INSERT INTO ".$wpdb->prefix."cs_transactions (
						user_id,campaign_id, code_id, payer_name, payer_email, gross, currency, payment_status, transaction_type, txn_id, details, created) VALUES (
						'".$uid."',
						'".$item_number."',
						'".$code_id."',
						'".esc_sql($payer_name)."',
						'".esc_sql($payer_id)."',
						'".floatval($gross_total)."',
						'".$mc_currency."',
						'".$payment_status."',
						'Payza: ".$transaction_type."',
						'".$txn_id."',
						'".esc_sql($response)."',
						'".time()."'
					)";
					$wpdb->query($sql);
					
					if ($payment_status == "Completed") {
						$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{code}", "{transaction_date}", "{gateway}");
						$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_details['title'], $code_details['code'], date("Y-m-d H:i:s")." (server time)", "Payza");
						$body = str_replace($tags, $vals, $campaign_details['success_email']);
						$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
						$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
						$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
						wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
						$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}.', 'codeshop').PHP_EOL.__('Purchased code: {code}.', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
						$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
						$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
						$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
						//wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
						wp_mail(get_option('admin_email','',$uid), $campaign_details['title'], $body, $mail_headers);
					} else {
						$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{payment_status}", "{transaction_date}", "{gateway}");
						$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_title, $payment_status, date("Y-m-d H:i:s")." (server time)", "Payza");
						$body = str_replace($tags, $vals, $campaign_details['failed_email']);
						$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
						$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
						$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
						wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
						$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}. This is non-completed payment.', 'codeshop').PHP_EOL.__('Payment status: {payment_status}', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
						$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
						$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
						$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
						//wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
						wp_mail(get_option('admin_email','',$uid), $campaign_details['title'], $body, $mail_headers);
					}
				}
			}
			exit;
		} else if (isset($_GET['codeshop-ipn']) && $_GET['codeshop-ipn'] == 'egopay') {
			if (empty($_POST['product_id'])) die();
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "https://www.egopay.com/payments/request");
			curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_5_6; en-us) AppleWebKit/525.27.1 (KHTML, like Gecko) Version/3.2.1 Safari/525.27.1");
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, 
				"product_id=".urlencode($_POST['product_id'])
				."&security_password=".urlencode($this->options['egopay_store_pass'])
				."&store_id=".urlencode($this->options['egopay_store_id'])
				."&v=1.1"
			);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_TIMEOUT, 20);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$response = curl_exec($ch);
			curl_close($ch);                
			if ($response == 'INVALID') die();
			$info = array();
			parse_str($response, $info);	

			$item_number = intval($info['cf_1']);
			$payer_email = $info['cf_2'];
			$payment_status = $info['sStatus'];
			$transaction_type = $info['sType'];
			$txn_id = $info['sId'];
			$payer_id = $info['sEmail'];
			$gross_total = $info['fAmount'];
			$mc_currency = $info['sCurrency'];
			$payer_name = $info['sEmail'];
			if ($payment_status == 'TEST SUCCESS') $payment_status = "Completed";
			$campaign_title = 'Unknown';
			if ($payment_status == "Completed") {
				if (!$campaign_details) $payment_status = "Unrecognized";
				else {
					$campaign_title = $campaign_details["title"];
					if (floatval($gross_total) < floatval($campaign_details["price"]) || $mc_currency != $campaign_details["currency"]) $payment_status = "Unrecognized";
				}
			}
			$code_id = 0;
			if ($payment_status == "Completed") {
				$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE deleted = '0' AND blocked = '0' AND campaign_id = '".$item_number."'".(intval($campaign_details['allowed_sales']) > 0 ? " AND sales < '".intval($campaign_details['allowed_sales'])."'" : "")." ORDER BY sales ASC", ARRAY_A);
				if (!$code_details) {
					$payment_status = 'No codes available';
				} else {
					$payment_status = 'Completed';
					$code_id = $code_details['id'];
					$wpdb->query("UPDATE ".$wpdb->prefix."cs_codes SET sales = sales + 1 WHERE id = '".$code_id."'");
				}
			}
			$sql = "INSERT INTO ".$wpdb->prefix."cs_transactions (
				campaign_id, code_id, payer_name, payer_email, gross, currency, payment_status, transaction_type, txn_id, details, created) VALUES (
				'".$item_number."',
				'".$code_id."',
				'".esc_sql($payer_name)."',
				'".esc_sql($payer_id)."',
				'".floatval($gross_total)."',
				'".$mc_currency."',
				'".$payment_status."',
				'EgoPay: ".$transaction_type."',
				'".$txn_id."',
				'".esc_sql($response)."',
				'".time()."'
			)";
			$wpdb->query($sql);
			
			if ($payment_status == "Completed") {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{code}", "{transaction_date}", "{gateway}");
				$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_details['title'], $code_details['code'], date("Y-m-d H:i:s")." (server time)", "EgoPay");
				$body = str_replace($tags, $vals, $campaign_details['success_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}.', 'codeshop').PHP_EOL.__('Purchased code: {code}.', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
			} else {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{payment_status}", "{transaction_date}", "{gateway}");
				$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_title, $payment_status, date("Y-m-d H:i:s")." (server time)", "EgoPay");
				$body = str_replace($tags, $vals, $campaign_details['failed_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}. This is non-completed payment.', 'codeshop').PHP_EOL.__('Payment status: {payment_status}', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
			}
			exit;
		} else if (isset($_GET['codeshop-ipn']) && $_GET['codeshop-ipn'] == 'skrill') {
			if (empty($_POST['pay_to_email']) || empty($_POST['pay_from_email'])) die();

			$response = "";
			foreach ($_POST as $key => $value) {
				$value = urlencode(stripslashes($value));
				$response .= "&".$key."=".$value;
			}

			$item_number = intval($_POST['campaign_id']);
			$txn_id = stripslashes($_POST['mb_transaction_id']);
			$seller_id = stripslashes($_POST['pay_to_email']);
			$payer_id = stripslashes($_POST['pay_from_email']);
			$payer_email = stripslashes($_POST['payer_email']);
			$gross_total = stripslashes($_POST['amount']);
			$mc_currency = stripslashes($_POST['currency']);
			$payment_status = stripslashes($_POST['status']);
			$transaction_type = "payment";
			$md5sig = stripslashes($_POST['md5sig']);
			$payer_name = stripslashes($_POST['pay_from_email']);

			if ($payment_status == 2) $payment_status = 'Completed';
			else if ($payment_status == 0) $payment_status = 'Pending';
			else if ($payment_status == -1) $payment_status = 'Cancelled';
			else if ($payment_status == -2) $payment_status = 'Failed';
			else if ($payment_status == -3) $payment_status = 'Chargeback';
			
			$hash = strtoupper(md5($_POST['merchant_id'].$_POST['transaction_id'].strtoupper(md5($this->options['skrill_secret_word'])).$_POST['mb_amount'].$_POST['mb_currency'].$_POST['status']));

			$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE id = '".intval($item_number)."'", ARRAY_A);
			if (!$campaign_details) $payment_status = "Unrecognized";
			else {
				$campaign_title = $campaign_details["title"];
				if (strtolower($seller_id) != strtolower($this->options['skrill_id']) || $md5sig != $hash) $payment_status = "Unrecognized";
				else if (floatval($gross_total) < floatval($campaign_details["price"]) || $mc_currency != $campaign_details["currency"]) $payment_status = "Unrecognized";
			}

			$code_id = 0;
			if ($payment_status == "Completed") {
				$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE deleted = '0' AND blocked = '0' AND campaign_id = '".$item_number."'".(intval($campaign_details['allowed_sales']) > 0 ? " AND sales < '".intval($campaign_details['allowed_sales'])."'" : "")." ORDER BY sales ASC", ARRAY_A);
				if (!$code_details) {
					$payment_status = 'No codes available';
				} else {
					$payment_status = 'Completed';
					$code_id = $code_details['id'];
					$wpdb->query("UPDATE ".$wpdb->prefix."cs_codes SET sales = sales + 1 WHERE id = '".$code_id."'");
				}
			}
			$sql = "INSERT INTO ".$wpdb->prefix."cs_transactions (
				campaign_id, code_id, payer_name, payer_email, gross, currency, payment_status, transaction_type, txn_id, details, created) VALUES (
				'".$item_number."',
				'".$code_id."',
				'".esc_sql($payer_name)."',
				'".esc_sql($payer_id)."',
				'".floatval($gross_total)."',
				'".$mc_currency."',
				'".$payment_status."',
				'Skrill: ".$transaction_type."',
				'".$txn_id."',
				'".esc_sql($response)."',
				'".time()."'
			)";
			$wpdb->query($sql);
			
			if ($payment_status == "Completed") {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{code}", "{transaction_date}", "{gateway}");
				$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_details['title'], $code_details['code'], date("Y-m-d H:i:s")." (server time)", "Skrill");
				$body = str_replace($tags, $vals, $campaign_details['success_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}.', 'codeshop').PHP_EOL.__('Purchased code: {code}.', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
			} else {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{payment_status}", "{transaction_date}", "{gateway}");
				$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_title, $payment_status, date("Y-m-d H:i:s")." (server time)", "Skrill");
				$body = str_replace($tags, $vals, $campaign_details['failed_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}. This is non-completed payment.', 'codeshop').PHP_EOL.__('Payment status: {payment_status}', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
			}
			exit;
		} else if (isset($_GET['codeshop-ipn']) && $_GET['codeshop-ipn'] == 'authnet') {
			$request = "";
                        $uid=0;
			foreach ($_POST as $key => $value) {
				$value = urlencode(stripslashes($value));
				$request .= "&".$key."=".$value;
			}
			if(strlen($request) > 0) {

				$item_number = str_replace("ID", "", stripslashes($_POST['x_invoice_num']));
				if (($pos = strpos($item_number, "N")) !== false) $item_number = substr($item_number, 0, $pos);
				$item_number = intval($item_number);
				$item_name = stripslashes($_POST['x_description']);
				$payment_status = stripslashes($_POST['x_response_code']);
				$transaction_type = stripslashes($_POST['x_card_type']);
				$txn_id = stripslashes($_POST['x_trans_id']);
				//$seller_id = stripslashes($_POST['x_login']);
				$payer_id = stripslashes($_POST['x_email']);
				$payer_email = stripslashes($_POST['x_email']);
				$gross_total = stripslashes($_POST['x_amount']);
				$mc_currency = "USD";
				$payer_name = stripslashes($_POST['x_first_name']).' '.stripslashes($_POST['x_last_name']);
				$test_mode = strtolower(stripslashes($_POST['x_test_request']));
				$md5hash = strtolower(stripslashes($_POST['x_MD5_Hash']));
				if (empty($_POST['x_first_name']) && empty($_POST['x_last_name'])) {
					$payer_name = 'Unknown Payer';
				}

				if ($payment_status == "1") {
					$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE id = '".intval($item_number)."'", ARRAY_A);
					if (!$campaign_details) $payment_status = "Unrecognized";
					else {
						$campaign_title = $campaign_details["title"];
                                                $uid=$campaign_details['user_id'];
						if ($test_mode == "true" && get_option('codeshop_authnet_sandbox','',$uid) != 'on') $payment_status = "Test mode";
						if (floatval($gross_total) < floatval($campaign_details["price"]) || $mc_currency != $campaign_details["currency"]) $payment_status = "Unrecognized";
						else {
							$md5source = get_option('codeshop_authnet_md5hash','',$uid) . get_option('codeshop_authnet_login','',$uid) . $txn_id . $gross_total;
							$md5 = md5($md5source);
							if (strtolower($md5) !== $md5hash) $payment_status = "Invalid MD5 Hash";
						}
					}
				}
				if ($payment_status == "1") $payment_status = "Completed";
				
				$code_id = 0;
				if ($payment_status == "Completed") {
					$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE deleted = '0' AND blocked = '0' AND campaign_id = '".$item_number."'".(intval($campaign_details['allowed_sales']) > 0 ? " AND sales < '".intval($campaign_details['allowed_sales'])."'" : "")." ORDER BY sales ASC", ARRAY_A);
					if (!$code_details) {
						$payment_status = 'No codes available';
					} else {
						$payment_status = 'Completed';
						$code_id = $code_details['id'];
						$wpdb->query("UPDATE ".$wpdb->prefix."cs_codes SET sales = sales + 1 WHERE id = '".$code_id."'");
					}
				}
				$sql = "INSERT INTO ".$wpdb->prefix."cs_transactions (
					user_id,campaign_id, code_id, payer_name, payer_email, gross, currency, payment_status, transaction_type, txn_id, details, created) VALUES (
					'".$uid."',
					'".$item_number."',
					'".$code_id."',
					'".esc_sql($payer_name)."',
					'".esc_sql($payer_id)."',
					'".floatval($gross_total)."',
					'".$mc_currency."',
					'".$payment_status."',
					'Autorize.Net: ".$transaction_type."',
					'".$txn_id."',
					'".esc_sql($request)."',
					'".time()."'
				)";
				$wpdb->query($sql);
				
				if ($payment_status == "Completed") {
					$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{code}", "{transaction_date}", "{gateway}");
					$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_details['title'], $code_details['code'], date("Y-m-d H:i:s")." (server time)", "Autorize.Net");
					$body = str_replace($tags, $vals, $campaign_details['success_email']);
					$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
					$mail_headers .= "From: ".get_option('codeshop_from_name','',$uid)." <".get_option('codeshop_from_email','',$uid).">\r\n";
					$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
					wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
					$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}.', 'codeshop').PHP_EOL.__('Purchased code: {code}.', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
					$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
					$mail_headers .= "From: ".get_option('codeshop_from_name','',$uid)." <".get_option('codeshop_from_email','',$uid).">\r\n";
					$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
					wp_mail(get_option('codeshop_admin_email','',$uid), $campaign_details['title'], $body, $mail_headers);
					echo '<!DOCTYPE html><html><head><meta charset="utf-8"><title>'.__('Transaction completed', 'codeshop').'</title></head><body><div class="page codeshop_returnbox">'.__('Payment successfully completed.', 'codeshop').'<br /><a class="codeshop_return" href="'.$campaign_details['success_url'].'">'.__('Return to Merchant', 'codeshop').'</a></div></body></html>';
				} else {
					$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{payment_status}", "{transaction_date}", "{gateway}");
					$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_title, $payment_status, date("Y-m-d H:i:s")." (server time)", "Autorize.Net");
					$body = str_replace($tags, $vals, $campaign_details['failed_email']);
					$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
					$mail_headers .= "From: ".get_option('codeshop_from_name','',$uid)." <".get_option('codeshop_from_email','',$uid).">\r\n";
					$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
					wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
					$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}. This is non-completed payment.', 'codeshop').PHP_EOL.__('Payment status: {payment_status}', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
					$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
					$mail_headers .= "From: ".get_option('codeshop_from_name','',$uid)." <".get_option('codeshop_from_email','',$uid).">\r\n";
					$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
					wp_mail(get_option('codeshop_admin_email','',$uid), $campaign_details['title'], $body, $mail_headers);
					echo '<!DOCTYPE html><html><head><meta charset="utf-8"><title>'.__('Error', 'codeshop').'</title></head><body><div class="page codeshop_returnbox">'.__('Payment failed. Please contact Merchant about transaction', 'codeshop').' <strong>'.$txn_id.'</strong>.<br /><a class="codeshop_return" href="'.$campaign_details['failed_url'].'">'.__('Return to Merchant', 'codeshop').'</a></div></body></html>';
				}
			}
			exit;
		} else if (isset($_GET['codeshop-ipn']) && $_GET['codeshop-ipn'] == 'interkassa') {
			if (isset($_POST['ik_co_id'])) {
				$request = '';
				$data = array();
				foreach ($_POST as $key => $value) {
					$request .= "&".$key."=".$value;
					if (strtolower(substr($key, 0, 3)) == 'ik_' && $key != 'ik_sign') $data[$key] = $value;
				}
				$item_number = $_POST['ik_pm_no'];
				if (($pos = strpos($item_number, "-")) !== false) $item_number = intval(substr($item_number, 0, $pos));
				$item_name = $_POST['ik_desc'];
				$payment_status = $_POST['ik_inv_st'];
				$transaction_type = $_POST['ik_pw_via'];
				$txn_id = $_POST['ik_co_prs_id'];
				$seller_id = $_POST['ik_co_id'];
				$payer_id = $_POST['ik_x_email'];
				$payer_email = $_POST['ik_x_email'];
				$gross_total = $_POST['ik_am'];
				$mc_currency = $_POST['ik_cur'];
				$payer_name = $payer_id;

				if ($payment_status == "success") {
					$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE id = '".intval($item_number)."'", ARRAY_A);
					if (!$campaign_details) $payment_status = "Unrecognized: N01";
					else if (strtolower($seller_id) != strtolower($this->options['interkassa_checkout_id'])) $payment_status = "Unrecognized: N02";
					else if (floatval($gross_total) < floatval($campaign_details["price"])) $payment_status = "Unrecognized: N03";
					else if (!empty($mc_currency) && $mc_currency != $campaign_details["currency"]) $payment_status = "Unrecognized: N04";
					else {
						ksort($data, SORT_STRING);
						array_push($data, $this->options['interkassa_secret_key']);
						$signString = implode(':', $data);
						$sign = base64_encode(md5($signString, true));
						if ($_POST['ik_sign'] != $sign) $payment_status = "Unrecognized: N05";
					}
				}
				
				$code_id = 0;
				if ($payment_status == "success") {
					$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE deleted = '0' AND blocked = '0' AND campaign_id = '".$item_number."'".(intval($campaign_details['allowed_sales']) > 0 ? " AND sales < '".intval($campaign_details['allowed_sales'])."'" : "")." ORDER BY sales ASC", ARRAY_A);
					if (!$code_details) {
						$payment_status = 'No codes available';
					} else {
						$payment_status = 'Completed';
						$code_id = $code_details['id'];
						$wpdb->query("UPDATE ".$wpdb->prefix."cs_codes SET sales = sales + 1 WHERE id = '".$code_id."'");
					}
				}
				$sql = "INSERT INTO ".$wpdb->prefix."cs_transactions (
					user_id,campaign_id, code_id, payer_name, payer_email, gross, currency, payment_status, transaction_type, txn_id, details, created) VALUES (
					'".$_SESSION['user_id']."',
					'".$item_number."',
					'".$code_id."',
					'".esc_sql($payer_name)."',
					'".esc_sql($payer_id)."',
					'".floatval($gross_total)."',
					'".$mc_currency."',
					'".$payment_status."',
					'InterKassa: ".$transaction_type."',
					'".$txn_id."',
					'".esc_sql($request)."',
					'".time()."'
				)";
				$wpdb->query($sql);
				if ($payment_status == "Completed") {
					$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{code}", "{transaction_date}", "{gateway}");
					$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_details['title'], $code_details['code'], date("Y-m-d H:i:s")." (server time)", "InterKassa");
					$body = str_replace($tags, $vals, $campaign_details['success_email']);
					$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
					$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
					$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
					wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
					$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}.', 'codeshop').PHP_EOL.__('Purchased code: {code}.', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
					$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
					$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
					$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
					wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
				} else {
					$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{payment_status}", "{transaction_date}", "{gateway}");
					$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_title, $payment_status, date("Y-m-d H:i:s")." (server time)", "InterKassa");
					$body = str_replace($tags, $vals, $campaign_details['failed_email']);
					$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
					$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
					$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
					wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
					$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}. This is non-completed payment.', 'codeshop').PHP_EOL.__('Payment status: {payment_status}', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
					$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
					$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
					$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
					wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
				}
			}
			exit;
		} else if (isset($_GET['codeshop-ipn']) && $_GET['codeshop-ipn'] == 'bitpay') {
			$json = file_get_contents("php://input");
			if (empty($json)) die();
			
			$post = json_decode($json, true);
			if (!is_array($post)) die();

			$txn_id = stripslashes($post['id']);
			//$curl = curl_init('https://bitpay.com/api/invoice/'.$txn_id);
			$curl = curl_init('https://test.bitpay.com/api/invoice/'.$txn_id);
			
			$header = array(
				'Content-Type: application/json',
				'Content-Length: 0',
				'Authorization: Basic '.base64_encode($this->options['bitpay_key']),
				);

			curl_setopt($curl, CURLOPT_PORT, 443);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
			curl_setopt($curl, CURLOPT_TIMEOUT, 10);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1); // verify certificate
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // check existence of CN and verify that it matches hostname
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
			curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
				
			$json = curl_exec($curl);

			curl_close($curl);
			
			if ($json === false) die();
			
			$post = json_decode($json, true);
			if (!$post) die();
			if (isset($post['error'])) die();
			
			$response = "";
			foreach ($post as $key => $value) {
				$value = urlencode(stripslashes($value));
				$response .= "&".$key."=".$value;
			}

			$posData = json_decode($post['posData'], true);

			$payment_status = stripslashes($post['status']);
			$item_number = intval($posData['campaign_id']);
			$payer_id = $posData['payer_email'];
			$payer_email = $posData['payer_email'];
			$transaction_type = "bitcoins";
			$payer_name = $posData['payer_email'];
			$mc_currency = $post['currency'];
			$gross_total = $mc_currency == 'BTC' ? number_format($post['price'], 4, '.', '') : number_format($post['price'], 2, '.', '');

			$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE id = '".intval($item_number)."'", ARRAY_A);
			if (!$campaign_details) $payment_status = "Unrecognized";
			else {
				$campaign_title = $campaign_details["title"];
				if (floatval($gross_total) < floatval($campaign_details["price"]) || $mc_currency != $campaign_details["currency"]) $payment_status = "Unrecognized";
			}

			$code_id = 0;
			if ($payment_status == "confirmed" || $payment_status == "complete") {
				$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE deleted = '0' AND blocked = '0' AND campaign_id = '".$item_number."'".(intval($campaign_details['allowed_sales']) > 0 ? " AND sales < '".intval($campaign_details['allowed_sales'])."'" : "")." ORDER BY sales ASC", ARRAY_A);
				if (!$code_details) {
					$payment_status = 'No codes available';
				} else {
					$payment_status = 'Completed';
					$code_id = $code_details['id'];
					$wpdb->query("UPDATE ".$wpdb->prefix."cs_codes SET sales = sales + 1 WHERE id = '".$code_id."'");
				}
			}
			$sql = "INSERT INTO ".$wpdb->prefix."cs_transactions (
				user_id,campaign_id, code_id, payer_name, payer_email, gross, currency, payment_status, transaction_type, txn_id, details, created) VALUES (
				'".$campaign_details['user_id']."',
				'".$item_number."',
				'".$code_id."',
				'".esc_sql($payer_name)."',
				'".esc_sql($payer_id)."',
				'".floatval($gross_total)."',
				'".$mc_currency."',
				'".$payment_status."',
				'BitPay: ".$transaction_type."',
				'".$txn_id."',
				'".esc_sql($response)."',
				'".time()."'
			)";
			$wpdb->query($sql);
			
			if ($payment_status == "Completed") {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{code}", "{transaction_date}", "{gateway}");
				$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_details['title'], $code_details['code'], date("Y-m-d H:i:s")." (server time)", "BitPay");
				$body = str_replace($tags, $vals, $campaign_details['success_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}.', 'codeshop').PHP_EOL.__('Purchased code: {code}.', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
			} else {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{payment_status}", "{transaction_date}", "{gateway}");
				$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_title, $payment_status, date("Y-m-d H:i:s")." (server time)", "BitPay");
				$body = str_replace($tags, $vals, $campaign_details['failed_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}. This is non-completed payment.', 'codeshop').PHP_EOL.__('Payment status: {payment_status}', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
			}
			exit;
		} else if (isset($_GET['codeshop-ipn']) && $_GET['codeshop-ipn'] == 'blockchain') {
			if (empty($_GET['transaction_hash']) || empty($_GET['secret'])) die();
			
			$response = 'operator=blockchain';
			foreach ($_GET as $key => $value) {
				$response .= "&".$key."=".$value;
			}
			
			$item_number = intval($_GET['campaign_id']);
			$payer_id = base64_decode(trim(stripslashes($_GET['email'])));
			$payer_email = $payer_id;
			$transaction_type = "bitcoins";
			$payer_name = $payer_id;
			$gross_total = intval($_GET['value'])/100000000;
			$amount = floatval($_GET['amount']);
			$confirmations = intval($_GET['confirmations']);
			$destination_address = urldecode($_GET['destination_address']);
			$txn_id = urldecode($_GET['transaction_hash']);
			$secret = urldecode($_GET['secret']);
			$mc_currency = 'BTC';
			$payment_status = "Confirmed";
			
			if ($confirmations < $this->options['blockchain_confirmations']) exit;

			$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE id = '".$item_number."'", ARRAY_A);
			if (!$campaign_details) $payment_status = "Unrecognized N01";
			else {
				$campaign_title = $campaign_details["title"];
				if (floatval($gross_total) < floatval($amount)) $payment_status = "Unrecognized N02";
				else if ($destination_address != $this->options['blockchain_address']) $payment_status = "Unrecognized N03";
				else if ($secret != $this->options['blockchain_secret']) $payment_status = "Unrecognized N04";
				else if (isset($_GET['test'])) $payment_status = "Test";
			}

			$code_id = 0;
			if ($payment_status == "Confirmed") {
				$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE deleted = '0' AND blocked = '0' AND campaign_id = '".$item_number."'".(intval($campaign_details['allowed_sales']) > 0 ? " AND sales < '".intval($campaign_details['allowed_sales'])."'" : "")." ORDER BY sales ASC", ARRAY_A);
				if (!$code_details) {
					$payment_status = 'No codes available';
				} else {
					$payment_status = 'Completed';
					$code_id = $code_details['id'];
					$wpdb->query("UPDATE ".$wpdb->prefix."cs_codes SET sales = sales + 1 WHERE id = '".$code_id."'");
				}
			}
			$sql = "INSERT INTO ".$wpdb->prefix."cs_transactions (
				campaign_id, code_id, payer_name, payer_email, gross, currency, payment_status, transaction_type, txn_id, details, created) VALUES (
				'".$item_number."',
				'".$code_id."',
				'".esc_sql($payer_name)."',
				'".esc_sql($payer_id)."',
				'".floatval($gross_total)."',
				'".$mc_currency."',
				'".$payment_status."',
				'Blockchain.info: ".$transaction_type."',
				'".$txn_id."',
				'".esc_sql($response)."',
				'".time()."'
			)";
			$wpdb->query($sql);
			
			if ($payment_status == "Completed") {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{code}", "{transaction_date}", "{gateway}");
				$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_details['title'], $code_details['code'], date("Y-m-d H:i:s")." (server time)", "Blockchain.info");
				$body = str_replace($tags, $vals, $campaign_details['success_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}.', 'codeshop').PHP_EOL.__('Purchased code: {code}.', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
			} else {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{payment_status}", "{transaction_date}", "{gateway}");
				$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_title, $payment_status, date("Y-m-d H:i:s")." (server time)", "Blockchain.info");
				$body = str_replace($tags, $vals, $campaign_details['failed_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}. This is non-completed payment.', 'codeshop').PHP_EOL.__('Payment status: {payment_status}', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
			}
			echo '*ok*';
			exit;
		} else if (isset($_GET['codeshop-ipn'])) {
			$request = "cmd=_notify-validate";

			foreach ($_POST as $key => $value) {
				$value = urlencode(stripslashes($value));
				$request .= "&".$key."=".$value;
			}
			
			//$paypalurl = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
			$paypalurl = 'https://www.paypal.com/cgi-bin/webscr';
			$ch = curl_init($paypalurl);
			curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
			curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Connection: Close', 'User-Agent: Social Suite'));
			$result = curl_exec($ch);
			curl_close($ch);                
			if (substr(trim($result), 0, 8) != "VERIFIED") die();
			
			$uid = stripslashes($_POST['uid']);
			$item_number = stripslashes($_POST['item_number']);
			$item_name = stripslashes($_POST['item_name']);
			$payment_status = stripslashes($_POST['payment_status']);
			$transaction_type = stripslashes($_POST['txn_type']);
			$txn_id = stripslashes($_POST['txn_id']);
			$seller_paypal = stripslashes($_POST['business']);
			$seller_id = stripslashes($_POST['receiver_id']);
			$payer_id = stripslashes($_POST['payer_email']);
			$payer_email = stripslashes($_POST['custom']);
			$gross_total = stripslashes($_POST['mc_gross']);
			$mc_currency = stripslashes($_POST['mc_currency']);
			$payer_name = stripslashes($_POST['first_name']).' '.stripslashes($_POST['last_name']);

			$payer_status = stripslashes($_POST['payer_status']);
					
			if ($transaction_type == "web_accept" && $payment_status == "Completed") {
				$campaign_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_campaigns WHERE id = '".intval($item_number)."'", ARRAY_A);
				if (!$campaign_details) $payment_status = "Unrecognized";
				else {
                                    $uid=$campaign_details['user_id'];
					$campaign_title = $campaign_details["title"];
					if (empty($seller_paypal)) {
						$tx_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_transactions WHERE details LIKE '%txn_id=".$txn_id."%' AND payment_status != 'Unrecognized'", ARRAY_A);
						if (intval($tx_details["id"]) != 0) $seller_paypal = get_option('codeshop_paypal_id','',$uid);
					}
					if ((strtolower($seller_paypal) != strtolower(get_option('codeshop_paypal_id','',$uid))) && (strtolower($seller_id) != strtolower(get_option('codeshop_paypal_id','',$uid)))) $payment_status = "Unrecognized";
					else {
						if (floatval($gross_total) < floatval($campaign_details["price"]) || $mc_currency != $campaign_details["currency"]) $payment_status = "Unrecognized";
					}
				}
			}
			
			$code_id = 0;
			if ($payment_status == "Completed") {
				$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE deleted = '0' AND blocked = '0' AND campaign_id = '".$item_number."'".(intval($campaign_details['allowed_sales']) > 0 ? " AND sales < '".intval($campaign_details['allowed_sales'])."'" : "")." ORDER BY sales ASC", ARRAY_A);
				if (!$code_details) {
					$payment_status = 'No codes available';
				} else {
					$payment_status = 'Completed';
					$code_id = $code_details['id'];
					$wpdb->query("UPDATE ".$wpdb->prefix."cs_codes SET sales = sales + 1 WHERE id = '".$code_id."'");
				}
			}
			$sql = "INSERT INTO ".$wpdb->prefix."cs_transactions (
				user_id,campaign_id, code_id, payer_name, payer_email, gross, currency, payment_status, transaction_type, txn_id, details, created) VALUES (
				'".$uid."',
				'".$item_number."',
				'".$code_id."',
				'".esc_sql($payer_name)."',
				'".esc_sql($payer_id)."',
				'".floatval($gross_total)."',
				'".$mc_currency."',
				'".$payment_status."',
				'PayPal: ".$transaction_type."',
				'".$txn_id."',
				'".esc_sql($request)."',
				'".time()."'
			)";
			$wpdb->query($sql);
			
			if ($payment_status == "Completed") {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{code}", "{transaction_date}", "{gateway}");
				$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_details['title'], $code_details['code'], date("Y-m-d H:i:s")." (server time)", "PayPal");
				$body = str_replace($tags, $vals, $campaign_details['success_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".get_option('codeshop_from_name','',$uid)." <".get_option('codeshop_from_email','',$uid).">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}.', 'codeshop').PHP_EOL.__('Purchased code: {code}.', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".get_option('codeshop_from_name','',$uid)." <".get_option('codeshop_from_email','',$uid).">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail(get_option('codeshop_admin_email','',$uid), $campaign_details['title'], $body, $mail_headers);
			} else {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{payment_status}", "{transaction_date}", "{gateway}");
				$vals = array($payer_name, $payer_id, $gross_total, $mc_currency, $campaign_title, $payment_status, date("Y-m-d H:i:s")." (server time)", "PayPal");
				$body = str_replace($tags, $vals, $campaign_details['failed_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".get_option('codeshop_from_name','',$uid)." <".get_option('codeshop_from_email','',$uid).">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($payer_email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" via {gateway} on {transaction_date}. This is non-completed payment.', 'codeshop').PHP_EOL.__('Payment status: {payment_status}', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".get_option('codeshop_from_name','',$uid)." <".get_option('codeshop_from_email','',$uid).">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail(get_option('codeshop_admin_email','',$uid), $campaign_details['title'], $body, $mail_headers);
			}
			exit;
		}
	}

	function shortcode_handler($_atts) {
		global $post, $wpdb, $current_user;
		$form = '';
                $uid = intval($_atts["uid"]);
                $this->user_get_options($uid);
		$tmp_options = $this->options;
		if ($this->check_options() === true) {
			if (isset($_atts['id'])) {
				$campaign_id = intval($_atts["id"]);
				
				$campaign_details = $wpdb->get_row("SELECT t1.*, t2.total_sales, t2.total_codes FROM ".$wpdb->prefix."cs_campaigns t1 LEFT JOIN (SELECT campaign_id, SUM(sales) AS total_sales, COUNT(*) AS total_codes FROM ".$wpdb->prefix."cs_codes WHERE blocked = '0' AND deleted = '0' GROUP BY campaign_id) t2 ON t2.campaign_id = t1.id WHERE t1.id = '".$campaign_id."' AND t1.deleted = '0' AND t1.blocked = '0'", ARRAY_A);
				if (!$campaign_details) return '';
				
				if (intval($campaign_details['total_codes']) == 0) {
					return '<div class="codeshop-mainbox"><div class="codeshop-row codeshop-center codeshop-margin-top-10">'.$campaign_details['no_codes'].'</div></div>';
				} else if ($campaign_details['allowed_sales'] > 0) {
					$tmp = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE blocked = '0' AND deleted = '0' AND campaign_id = '".$campaign_id."' AND sales < '".$campaign_details['allowed_sales']."'", ARRAY_A);
					if (!$tmp) return '<div class="codeshop-mainbox"><div class="codeshop-row codeshop-center codeshop-margin-top-10">'.$campaign_details['no_codes'].'</div></div>';
				}
				if (!in_array($campaign_details["currency"], $this->interkassa_currency_list)) $this->options['enable_interkassa'] = "off";
				if (!in_array($campaign_details["currency"], $this->paypal_currency_list)) $this->options['enable_paypal'] = "off";
				if (!in_array($campaign_details["currency"], $this->payza_currency_list)) $this->options['enable_payza'] = "off";
				if (!in_array($campaign_details["currency"], $this->egopay_currency_list)) $this->options['enable_egopay'] = "off";
				if (!in_array($campaign_details["currency"], $this->skrill_currency_list)) $this->options['enable_skrill'] = "off";
				if (!in_array($campaign_details["currency"], $this->bitpay_currency_list)) $this->options['enable_bitpay'] = "off";
				if (!in_array($campaign_details['currency'], $this->blockchain_currency_list)) $this->options['enable_blockchain'] = "off";
				if (!in_array($campaign_details["currency"], $this->stripe_currency_list)) $this->options['enable_stripe'] = "off";
				if (!in_array($campaign_details["currency"], $this->authnet_currency_list)) $this->options['enable_authnet'] = "off";
				$methods = 0;
				if ($this->options['enable_paypal'] == "on") $methods++;
				if ($this->options['enable_payza'] == "on") $methods++;
				if ($this->options['enable_interkassa'] == "on") $methods++;
				if ($this->options['enable_authnet'] == "on") $methods++;
				if ($this->options['enable_egopay'] == "on") $methods++;
				if ($this->options['enable_skrill'] == "on") $methods++;
				if ($this->options['enable_bitpay'] == "on") $methods++;
				if ($this->options['enable_blockchain'] == "on") $methods++;
				if ($this->options['enable_stripe'] == "on") $methods++;
				if ($methods == 0 && ($campaign_details['price'] > 0 || ($campaign_details['is_flexible'] == 1))) {
					$this->options = $tmp_options;
					return '
				<div class="codeshop-mainbox">
					<div class="codeshop-row codeshop-center codeshop-margin-top-10">
						'.__('<strong>Attention!</strong> Administrator must enable at least one payment gateway!', 'codeshop').'
					</div>
				</div>';
				}
				
				if (isset($_atts["title"])) $intro = do_shortcode(trim($_atts["title"]));
				else $intro = do_shortcode($campaign_details['intro']);
				$intro = str_replace(array("\n", "\r", "{price}", "{currency}"), array("<br />", "", ($campaign_details['currency'] == 'BTC' ? number_format($campaign_details['price'], 4, '.', '') : number_format($campaign_details['price'], 2, '.', '')), $campaign_details['currency']), $intro);
				if (strlen($intro) > 0) $intro = '<div class="codeshop-row codeshop-description">'.$intro.'</div>';

				$success_url = "";
				if (!empty($_atts["success_url"])) {
					$success_url = $_atts["success_url"];
					if (!preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $success_url) || strlen($success_url) == 0) $success_url = "";
				}
				if (empty($success_url)) $success_url = $campaign_details['success_url'];
				if (empty($success_url)) {
					if (defined('UAP_CORE')) $success_url = $_SERVER["HTTP_REFERER"];
					else $success_url = 'http://'.$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
				}

				$failed_url = "";
				if (!empty($_atts["failed_url"])) {
					$failed_url = $_atts["failed_url"];
					if (!preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $failed_url) || strlen($failed_url) == 0) $failed_url = "";
				}
				if (empty($failed_url)) $failed_url = $campaign_details['failed_url'];
				if (empty($failed_url)) {
					if (defined('UAP_CORE')) $failed_url = $_SERVER["HTTP_REFERER"];
					else $failed_url = 'http://'.$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"];
				}
				$tac = '';
				$terms = esc_html($campaign_details['terms']);
				$terms = str_replace("\n", "<br />", $terms);
				$terms = str_replace("\r", "", $terms);
				if (!empty($campaign_details['terms'])) {
					$tac = '
					<div class="codeshop-row codeshop-description">
						'.__('By clicking the button above, I agree with the', 'codeshop').' <a href="#" onclick="jQuery(this).parent().children(\'.codeshop-terms\').slideToggle(100);return false;">'.__('Terms & Conditions', 'codeshop').'</a>.
						<div class="codeshop-terms">'.$terms.'</div>
					</div>';
				}
				
				if ($this->options['enable_paypal'] == "on") $active_method = 'paypal';
				else if ($this->options['enable_payza'] == "on") $active_method = 'payza';
				else if ($this->options['enable_interkassa'] == "on") $active_method = 'interkassa';
				else if ($this->options['enable_authnet'] == "on") $active_method = 'authnet';
				else if ($this->options['enable_egopay'] == "on") $active_method = 'egopay';
				else if ($this->options['enable_skrill'] == "on") $active_method = 'skrill';
				else if ($this->options['enable_bitpay'] == "on") $active_method = 'bitpay';
				else if ($this->options['enable_blockchain'] == "on") $active_method = 'blockchain';
				else if ($this->options['enable_stripe'] == "on") $active_method = 'stripe';
				else $active_method = '';

				$form = '
<div class="codeshop-mainbox">
	<div class="codeshop-payment-form">
		'.$intro.'
		'.($campaign_details["is_flexible"] != 1 ? '
                <div class="codeshop-row">
                    <input class="codeshop-input" name="codeshop-noc" type="text" placeholder="'.__('How many codes do you want? (mandatory)', 'codeshop').'">
                </div>
		<div class="codeshop-row">
			<input class="codeshop-input" name="codeshop-email" type="text" placeholder="'.__('Where should we email the codes? (mandatory)', 'codeshop').'" />
			<input type="hidden" name="codeshop-amount" value="'.$campaign_details['price'].'" />
		</div>' : '
		<div class="codeshop-row codeshop-table">
			<div class="codeshop-table-cell codeshop-table-cell-100">
				<input class="codeshop-input" name="codeshop-email" type="text" placeholder="'.__('Where should we email the codes? (mandatory)', 'codeshop').'">
			</div>
			<div class="codeshop-table-cell">
				<input class="codeshop-input codeshop-input-number" name="codeshop-amount" type="text" placeholder="'.$campaign_details["currency"].'">
			</div>
		</div>');
                                if($campaign_details["is_flexible"] == 1)
                                    $form .='<input type="hidden" name="codeshop-is_flexible" value="1" />';
                                else
                                    $form .='<input type="hidden" name="codeshop-is_flexible" value="0" />';

				if ($campaign_details['price'] > 0 || $campaign_details["is_flexible"] == 1) {
					$form .= '
		<div class="codeshop-row codeshop-table">
			<div class="codeshop-table-cell codeshop-table-cell-100">
				<div class="codeshop-payment-seclector">
					<div class="codeshop-payment-selected" onclick="jQuery(this).parent().children(\'.codeshop-payment-providers\').slideToggle(200);">
						<span>?</span>
						<img class="codeshop-payment-selected-logo" src="'.plugins_url('/images/logo_'.$active_method.'.png', __FILE__).'" />
					</div>
					<div class="codeshop-payment-providers'.($methods > 4 ? ' codeshop-vertical-scroll' : '').'">';
					if ($this->options['enable_paypal'] == "on") {
						$form .= '
						<a href="#" onclick="return codeshop_set_provider(this, \'paypal\');"><img src="'.plugins_url('/images/logo_paypal.png', __FILE__).'" /></a>';
					}
					if ($this->options['enable_payza'] == "on") {
						$form .= '
						<a href="#" onclick="return codeshop_set_provider(this, \'payza\');"><img src="'.plugins_url('/images/logo_payza.png', __FILE__).'" /></a>';
					}
					if ($this->options['enable_skrill'] == "on") {
						$form .= '
						<a href="#" onclick="return codeshop_set_provider(this, \'skrill\');"><img src="'.plugins_url('/images/logo_skrill.png', __FILE__).'" /></a>';
					}
					if ($this->options['enable_interkassa'] == "on") {
						$form .= '
						<a href="#" onclick="return codeshop_set_provider(this, \'interkassa\');"><img src="'.plugins_url('/images/logo_interkassa.png', __FILE__).'" /></a>';
					}
					if ($this->options['enable_authnet'] == "on") {
						$form .= '
						<a href="#" onclick="return codeshop_set_provider(this, \'authnet\');"><img src="'.plugins_url('/images/logo_authnet.png', __FILE__).'" /></a>';
					}
					if ($this->options['enable_egopay'] == "on") {
						$form .= '
						<a href="#" onclick="return codeshop_set_provider(this, \'egopay\');"><img src="'.plugins_url('/images/logo_egopay.png', __FILE__).'" /></a>';
					}
					if ($this->options['enable_bitpay'] == "on") {
						$form .= '
						<a href="#" onclick="return codeshop_set_provider(this, \'bitpay\');"><img src="'.plugins_url('/images/logo_bitpay.png', __FILE__).'" /></a>';
					}
					if ($this->options['enable_blockchain'] == "on") {
						$form .= '
						<a href="#" onclick="return codeshop_set_provider(this, \'blockchain\');"><img src="'.plugins_url('/images/logo_blockchain.png', __FILE__).'" /></a>';
					}
					if ($this->options['enable_stripe'] == "on") {
						$form .= '
						<a href="#" onclick="return codeshop_set_provider(this, \'stripe\');"><img src="'.plugins_url('/images/logo_stripe.png', __FILE__).'" /></a>';
					}
					$form .= '
					</div>
				</div>
			</div>
                        
			<div class="codeshop-table-cell">
                                
				<a class="codeshop-button" href="#" onclick="return codeshop_continue(this);">'.__('Continue', 'codeshop').'</a>
			</div>
		</div>';
				} else {
					$form .= '
		<div class="codeshop-row codeshop-table">
                        <input type="hidden" name="codeshop-is_flexible" value="0" />
			<a class="codeshop-button" href="#" onclick="return codeshop_continue(this);">'.__('Continue', 'codeshop').'</a>
		</div>';
				}
				$form .= $tac.'
		<input type="hidden" name="codeshop-uid" value="'.$uid.'" />
		<input type="hidden" name="codeshop-gateway" value="'.$active_method.'" />
		<input type="hidden" name="codeshop-campaign" value="'.$campaign_details['id'].'" />
		<input type="hidden" name="codeshop-success-url" value="'.$success_url.'" />
		<input type="hidden" name="codeshop-failed-url" value="'.$failed_url.'" />
	</div>
	<div class="codeshop-confirmation"></div>
	<div class="codeshop-error"></div>
	<div class="codeshop-loading-overlay"></div>
	<div class="codeshop-loading-spinner"></div>
</div>';
			}
		}
		$this->options = $tmp_options;
		return $form;
	}

	function codeshop_continue() {
		global $wpdb;
		if (isset($_REQUEST['callback'])) {
			header("Content-type: text/javascript");
			$jsonp_enabled = true;
			$jsonp_callback = $_REQUEST['callback'];
		} else exit;	
                $is_flexible = base64_decode(trim(stripslashes($_REQUEST['is_flexible'])));
		if (isset($_REQUEST['uid'])){
                    $uid = base64_decode(trim(stripslashes($_REQUEST['uid'])));
                    $this->user_get_options($uid);
                }else{
                    $uid = 0;
                }

                if (isset($_REQUEST['noc'])) $noc = base64_decode(trim(stripslashes($_REQUEST['noc'])));
		else $noc = '';

		if (isset($_REQUEST['email'])) $email = base64_decode(trim(stripslashes($_REQUEST['email'])));
		//if (isset($_REQUEST['email'])) $email = trim(stripslashes($_REQUEST['email']));
		else $email = '';
		if (isset($_REQUEST['amount'])) $amount = floatval(base64_decode(trim(stripslashes($_REQUEST['amount']))));
		else $amount = 0;
		if (isset($_REQUEST['gateway'])) $payment_method = base64_decode(trim(stripslashes($_REQUEST['gateway'])));
		else $payment_method = '';
		if (isset($_REQUEST['campaign'])) $campaign_id = intval(base64_decode(trim(stripslashes($_REQUEST['campaign']))));
		else $campaign_id = 0;
		if (isset($_REQUEST['success_url'])) $success_url = base64_decode(trim(stripslashes($_REQUEST['success_url'])));
		else $success_url = '';
		if (isset($_REQUEST['failed_url'])) $failed_url = base64_decode(trim(stripslashes($_REQUEST['failed_url'])));
		else $failed_url = '';
		$campaign_details = $wpdb->get_row("SELECT t1.*, t2.total_sales, t2.total_codes FROM ".$wpdb->prefix."cs_campaigns t1 LEFT JOIN (SELECT campaign_id, SUM(sales) AS total_sales, COUNT(*) AS total_codes FROM ".$wpdb->prefix."cs_codes WHERE blocked = '0' AND deleted = '0' GROUP BY campaign_id) t2 ON t2.campaign_id = t1.id WHERE t1.deleted = '0' AND t1.blocked = '0' AND t1.id = '".$campaign_id."'", ARRAY_A);
                if($is_flexible==1){
                    $amount = $amount;
                }else{
                    $amount = ($noc * $campaign_details['price']);
                }
                

		$errors = array();
		if (!$campaign_details) {
			$errors[] = __('Campaign not found.', 'codeshop');
		} else {
			if (intval($campaign_details['total_codes']) == 0) {
				$errors[] = $campaign_details['no_codes'];
			} else if ($campaign_details['allowed_sales'] > 0) {
				$tmp = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE blocked = '0' AND deleted = '0' AND campaign_id = '".$campaign_id."' AND sales < '".$campaign_details['allowed_sales']."'");
				if (!$tmp) $errors[] = $campaign_details['no_codes'];
			}
			if (empty($errors)) {
                            if(empty($uid)) $errors[] = __('Something went wrong!', 'codeshop');
                            if(empty($noc)) $errors[] = __('Number of codes are required.', 'codeshop');
                            if($noc > $campaign_details['maximum_codes']) $errors[] =  __('You can purchase maximum '.$campaign_details['maximum_codes'].' codes.', 'codeshop');
                            if($noc < $campaign_details['minimum_codes']) $errors[] = __('You need to purchase minimum '.$campaign_details['minimum_codes'].' codes.', 'codeshop');

                                if (empty($email)) $errors[] = __('Your email address is required.', 'codeshop');
				else if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,9})$/i", $email)) $errors[] = __('You have entered an invalid e-mail address.', 'codeshop');
                                //elseif(!filter_var($email, FILTER_VALIDATE_EMAIL)) $errors[] = __('You have entered an invalid e-mail address.', 'codeshop');
                                if($_REQUEST['is_flexible']==1){
                                    if (!is_numeric($amount) || $amount < $campaign_details['price']) $errors[] = __('Amount must be at least', 'codeshop').' '.($campaign_details['currency'] == 'BTC' ? number_format($campaign_details['price'], 4, ".", "") : number_format($campaign_details['price'], 2, ".", "")).' '.$campaign_details['currency'].'.';
                                }
				if ($payment_method == 'interkassa' && $this->options['enable_interkassa'] != 'on') $errors[] = __('Payment method not supported.', 'codeshop');
				else if ($payment_method == 'payza' && $this->options['enable_payza'] != 'on') $errors[] = __('Payment method not supported.', 'codeshop');
				else if ($payment_method == 'authnet' && $this->options['enable_authnet'] != 'on') $errors[] = __('Payment method not supported.', 'codeshop');
				else if ($payment_method == 'skrill' && $this->options['enable_skrill'] != 'on') $errors[] = __('Payment method not supported.', 'codeshop');
				else if ($payment_method == 'egopay' && $this->options['enable_egopay'] != 'on') $errors[] = __('Payment method not supported.', 'codeshop');
				else if ($payment_method == 'bitpay' && $this->options['enable_bitpay'] != 'on') $errors[] = __('Payment method not supported.', 'codeshop');
				else if ($payment_method == 'blockchain' && $this->options['enable_blockchain'] != 'on') $errors[] = __('Payment method not supported.', 'codeshop');
				else if ($payment_method == 'stripe' && $this->options['enable_stripe'] != 'on') $errors[] = __('Payment method not supported.', 'codeshop');
				else if ($payment_method == 'paypal' && $this->options['enable_paypal'] != 'on') $errors[] = __('Payment method not supported.', 'codeshop');
			}
		}
		
		if (!empty($errors)) {
			$return_data = array();
			$return_data['status'] = 'ERROR';
			$return_data['html'] = implode('<br />', $errors);
			echo $jsonp_callback.'('.json_encode($return_data).')';
			exit;
		} else {
			if (empty($success_url)) $success_url = $campaign_details['success_url'];
			if (empty($success_url)) $success_url = $_SERVER["HTTP_REFERER"];
			if (empty($failed_url)) $failed_url = $campaign_details['failed_url'];
			if (empty($failed_url)) $failed_url = $_SERVER["HTTP_REFERER"];
		
			$html = '';
                        $tot_camp_amount=  $amount;
                        $stripe_pkey=  $this->options['stripe_publishable'];
                        $html.='<input type="hidden" name="codeshop-total-amout-camp" value="'.$tot_camp_amount.'" />';
			if ($amount == 0) {
				$html .= '
		<div class="codeshop-row codeshop-row-dashed codeshop-table">
			<div class="codeshop-table-cell">
				<div class="codeshop-confirmation-label">'.__('E-Mail', 'codeshop').':</div>
			</div>
			<div class="codeshop-table-cell codeshop-table-cell-100">
				<div class="codeshop-confirmation-value">'.esc_html($email).'</div>
			</div>
		</div>
		<div class="codeshop-row">
			<a class="codeshop-button codeshop-button-small" href="#" onclick="return codeshop_getcode(this);">'.__('Get The Code', 'codeshop').'</a>
			<a class="codeshop-button codeshop-button-small" href="#" onclick="return codeshop_edit(this);">'.__('Edit Info', 'codeshop').'</a>
		</div>';			
			} else {
				if ($campaign_details['currency'] == 'BTC') $amount = number_format($amount, 4, ".", "");
				else $amount = number_format($amount, 2, ".", "");
				$html .= '
		<div class="codeshop-row codeshop-row-dashed codeshop-table">
			<div class="codeshop-table-cell">
				<div class="codeshop-confirmation-label">'.__('E-Mail', 'codeshop').':</div>
			</div>
			<div class="codeshop-table-cell codeshop-table-cell-100">
				<div class="codeshop-confirmation-value">'.esc_html($email).'</div>
			</div>
		</div>
		<div class="codeshop-row codeshop-row-dashed codeshop-table">
			<div class="codeshop-table-cell">
				<div class="codeshop-confirmation-label">'.__('Amount', 'codeshop').':</div>
			</div>
			<div class="codeshop-table-cell codeshop-table-cell-100">
				<div class="codeshop-confirmation-value">'.$amount.' '.$campaign_details['currency'].'</div>
			</div>
		</div>
		<div class="codeshop-row codeshop-row-dashed codeshop-table">
			<div class="codeshop-table-cell">
				<div class="codeshop-confirmation-label">'.__('Payment gateway', 'codeshop').':</div>
			</div>
			<div class="codeshop-table-cell codeshop-table-cell-100">
				<div class="codeshop-confirmation-value"><img src="'.plugins_url('/images/logo_'.$payment_method.'.png', __FILE__).'" alt="'.esc_html($campaign_details['title']).'" /></div>
			</div>
		</div>
		<div class="codeshop-row">';
				if ($payment_method == 'bitpay') {
					$html .= '
			<a class="codeshop-button codeshop-button-small" href="#" onclick="return codeshop_bitpay(this);">'.__('Confirm And Pay', 'codeshop').'</a>
			<a class="codeshop-button codeshop-button-small" href="#" onclick="return codeshop_edit(this);">'.__('Edit Info', 'codeshop').'</a>';
				} else if ($payment_method == 'blockchain') {
					$html .= '
			<a class="codeshop-button codeshop-button-small" href="#" onclick="return codeshop_blockchain(this);">'.__('Confirm And Pay', 'codeshop').'</a>
			<a class="codeshop-button codeshop-button-small" href="#" onclick="return codeshop_edit(this);">'.__('Edit Info', 'codeshop').'</a>';
				} else if ($payment_method == 'stripe') {
					$html .= '
			<a class="codeshop-button codeshop-button-small" href="#" onclick="return codeshop_stripe(this,\''.$uid.'\' , \''.$campaign_details['currency'].'\', \''.str_replace(array('"', "'"), array("", ""), $campaign_details['title']).'\');">'.__('Confirm And Pay', 'codeshop').'</a>
			<a class="codeshop-button codeshop-button-small" href="#" onclick="return codeshop_edit(this);">'.__('Edit Info', 'codeshop').'</a>';
				} else {
					$html .= '
			<a class="codeshop-button codeshop-button-small" href="#" onclick="return codeshop_pay(this);">'.__('Confirm And Pay', 'codeshop').'</a>
			<a class="codeshop-button codeshop-button-small" href="#" onclick="return codeshop_edit(this);">'.__('Edit Info', 'codeshop').'</a>';
				}
				$html .= '
		</div>';
				if ($payment_method == 'interkassa') {
					$params = array();
					$params['ik_co_id'] = $this->options['interkassa_checkout_id'];
					$params['ik_am'] = number_format($amount, 2, ".", "");
					$params['ik_cur'] = $campaign_details["currency"];
					$params['ik_pm_no'] = $campaign_details["id"].'-'.time();
					$params['ik_desc'] = htmlspecialchars($campaign_details["title"], ENT_QUOTES);
					$params['ik_ia_u'] = defined('UAP_CORE') ? admin_url('do.php').'?codeshop-ipn=interkassa' : get_bloginfo("url").'/?codeshop-ipn=interkassa';
					$params['ik_ia_m'] = 'POST';
					$params['ik_suc_u'] = $success_url;
					$params['ik_suc_m'] = 'GET';
					$params['ik_fal_u'] = $failed_url;
					$params['ik_fal_m'] = 'GET';
					$params['ik_x_email'] = $email;
					$html .= '
	<form action="https://sci.interkassa.com/" method="post" target="_top" style="display:none;">';
					foreach($params as $key => $value) {
						$html .= '
		<input type="hidden" name="'.$key.'" value="'.$value.'">';
					}
					$html .= '
		<input type="submit" class="codeshop-pay" value="Submit">
	</form>';
				} else if ($payment_method == 'payza') {
                                    //$payzaurl='https://sandbox.payza.com/sandbox/payprocess.aspx';
                                    $payzaurl='https://secure.payza.com/PayProcess.aspx';
                                    /*if($this->options['payza_sandbox'] == 'on')
                                        $payzaurl='https://sandbox.payza.com/sandbox/payprocess.aspx';
                                    else
                                        $payzaurl='https://secure.payza.com/PayProcess.aspx';*/
					$html .= '
	<form action="'.$payzaurl.'" target="_top" method="post" style="display:none;">
		<input type="hidden" name="ap_merchant" value="'.$this->options['payza_id'].'">
		<input type="hidden" name="ap_purchasetype" value="item">
		<input type="hidden" name="ap_itemname" value="'.esc_html($campaign_details['title']).'">
		<input type="hidden" name="ap_amount" value="'.$amount.'">
		<input type="hidden" name="ap_currency" value="'.$campaign_details["currency"].'">
		<input type="hidden" name="apc_1" value="'.$email.'">
		<input type="hidden" name="ap_itemcode" value="ID'.$campaign_id.'">
		<input type="hidden" name="ap_uid" value="'.$uid.'">
		<input type="hidden" name="ap_returnurl" value="'.esc_html($success_url).'">
		<input type="hidden" name="ap_cancelurl" value="'.esc_html($failed_url).'">
		<input type="hidden" name="ap_alerturl" value="'.(defined('UAP_CORE') ? esc_html(admin_url('do.php').'?codeshop-ipn=payza') : esc_html(get_bloginfo('url').'/?codeshop-ipn=payza')).'">
		<input type="hidden" name="ap_ipnversion" value="2">
		<input type="submit" class="codeshop-pay" value="Submit">
	</form>';
				} else if ($payment_method == 'authnet') {
					$fp_timestamp = time();
					$fp_sequence = $campaign_details["id"].time();
					$fingerprint = $this->get_fingerprint(get_option('codeshop_authnet_login','',$uid),get_option('codeshop_authnet_key','',$uid) , number_format($amount, 2, ".", ""), $fp_sequence, $fp_timestamp);
					$html .= '
	<form action="'.((get_option('codeshop_authnet_sandbox','',$uid)== "on") ? 'https://test.authorize.net/gateway/transact.dll' : 'https://secure.authorize.net/gateway/transact.dll').'" method="post" target="_top" style="display:none;">';
					if (get_option('codeshop_authnet_sandbox','',$uid) == "on") {
						$html .= '
		<input type="hidden" name="x_test_request" value="true">';
					} else {
						$html .= '
		<input type="hidden" name="x_test_request" value="false">';
					}
					$html .= '
		<input type="hidden" name="x_version" value="3.1">
		<input type="hidden" name="x_show_form" value="payment_form">
		<input type="hidden" name="x_relay_response" value="true">
		<input type="hidden" name="x_method" value="cc">
		<input type="hidden" name="x_fp_hash" value="'.$fingerprint.'">
		<input type="hidden" name="x_fp_timestamp" value="'.$fp_timestamp.'">
		<input type="hidden" name="x_fp_sequence" value="'.$fp_sequence.'">
		<input type="hidden" name="x_receipt_link_url" value="'.esc_html($success_url).'">
		<input type="hidden" name="x_login" value="'.get_option('codeshop_authnet_login','',$uid).'">
		<input type="hidden" name="x_email" value="'.$email.'">
		<input type="hidden" name="x_relay_url" value="'.(defined('UAP_CORE') ? esc_html(admin_url('do.php').'?codeshop-ipn=authnet') : esc_html(get_bloginfo("url").'/?codeshop-ipn=authnet')).'">
		<input type="hidden" name="x_description" value="'.esc_html($campaign_details['title']).'">
		<input type="hidden" name="x_amount" value="'.$amount.'">
		<!--<input type="hidden" name="x_currency_code" value="'.$campaign_details["currency"].'">-->
		<input type="hidden" name="x_invoice_num" value="ID'.$campaign_id.'N'.time().'">
		<input type="hidden" name="x_uid" value="'.$uid.'">
		<input type="submit" class="codeshop-pay" value="Submit">
	</form>';
				} else if ($payment_method == 'skrill') {
					$html .= '
	<form action="https://www.moneybookers.com/app/payment.pl" method="post" target="_top" style="display:none;">
		<input type="hidden" name="pay_to_email" value="'.$this->options['skrill_id'].'">
		<input type="hidden" name="return_url" value="'.esc_html($success_url).'">
		<input type="hidden" name="cancel_url" value="'.esc_html($failed_url).'">
		<input type="hidden" name="status_url" value="'.(defined('UAP_CORE') ? esc_html(admin_url('do.php').'?codeshop-ipn=skrill') : esc_html(get_bloginfo("url").'/?codeshop-ipn=skrill')).'">
		<input type="hidden" name="language" value="EN">
		<input type="hidden" name="amount" value="'.$amount.'">
		<input type="hidden" name="currency" value="'.$campaign_details["currency"].'">
		<input type="hidden" name="detail1_description" value="'.__('Payment:', 'codeshop').'">
		<input type="hidden" name="detail1_text" value="'.esc_html($campaign_details["title"]).'">
		<input type="hidden" name="merchant_fields" value="campaign_id,payer_email">
		<input type="hidden" name="campaign_id" value="'.$campaign_id.'">
		<input type="hidden" name="uid" value="'.$uid.'">
		<input type="hidden" name="payer_email" value="'.$email.'">
		<input type="submit" class="codeshop-pay" value="Submit">
	</form>';
				} else if ($payment_method == 'egopay') {
					$hash = hash('sha256', $this->options['egopay_store_pass'].'|'.number_format($amount, 2, ".", "").'|'.$campaign_id.'|'.$email.'|'.$campaign_details["currency"]);
					$html .= '
	<form action="https://www.egopay.com/payments/pay/form" method="post" target="_top" style="display:none;">
		<input type="hidden" name="store_id" value="'.$this->options['egopay_store_id'].'">
		<input type="hidden" name="amount" value="'.$amount.'">
		<input type="hidden" name="currency" value="'.$campaign_details["currency"].'">
		<input type="hidden" name="description" value="'.esc_html($campaign_details["title"]).'">
		<input type="hidden" name="cf_1" value="'.$campaign_id.'">
		<input type="hidden" name="cf_2" value="'.$email.'">
		<input type="hidden" name="uid" value="'.$uid.'">
		<input type="hidden" name="success_url" value="'.esc_html($success_url).'">
		<input type="hidden" name="fail_url" value="'.esc_html($failed_url).'">
		<input type="hidden" name="callback_url" value="'.(defined('UAP_CORE') ? esc_html(admin_url('do.php').'?codeshop-ipn=egopay') : esc_html(get_bloginfo("url").'/?codeshop-ipn=egopay')).'">
		<input type="hidden" name="verify" value="'.$hash.'">
		<input type="submit" class="codeshop-pay" value="Submit">
	</form>';
				} else if ($payment_method == 'bitpay') {
				} else if ($payment_method == 'blockchain') {
				} else if ($payment_method == 'stripe') {
                                    $html.='<input type="hidden" name="codeshop-stripe-pkey" value="'.$stripe_pkey.'" />';
				} else if ($payment_method == 'paypal') {
					$html .= '
	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top" style="display: none;">
		<input type="hidden" name="cmd" value="_xclick">
		<input type="hidden" name="charset" value="utf-8">					
		<input type="hidden" name="business" value="'.get_option('codeshop_paypal_id','',$uid).'">
		<input type="hidden" name="no_shipping" value="1">
		<input type="hidden" name="rm" value="2">
		<input type="hidden" name="item_name" value="'.esc_html($campaign_details['title']).'">
		<input type="hidden" name="item_number" value="'.$campaign_id.'">
		<input type="hidden" name="uid" value="'.$uid.'">
		<input type="hidden" name="amount" value="'.$amount.'">
		<input type="hidden" name="currency_code" value="'.$campaign_details["currency"].'">
		<input type="hidden" name="custom" value="'.$email.'">
		<input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest">
		<input type="hidden" name="return" value="'.esc_html($success_url).'">
		<input type="hidden" name="cancel_return" value="'.esc_html($failed_url).'">
		<input type="hidden" name="notify_url" value="'.(defined('UAP_CORE') ? esc_html(admin_url('do.php').'?codeshop-ipn=paypal') : esc_html(get_bloginfo('url').'/?codeshop-ipn=paypal')).'">
		<input type="submit" class="codeshop-pay" value="Submit">
	</form>';
				}
			}
			$return_data = array();
			$return_data['status'] = 'OK';
			$return_data['html'] = $html;
			echo $jsonp_callback.'('.json_encode($return_data).')';
		}
		exit;
	}

	function codeshop_sendcode() {
		global $wpdb;
		if (isset($_REQUEST['callback'])) {
			header("Content-type: text/javascript");
			$jsonp_enabled = true;
			$jsonp_callback = $_REQUEST['callback'];
		} else exit;		
		if (isset($_REQUEST['email'])) $email = base64_decode(trim(stripslashes($_REQUEST['email'])));
		else $email = '';
		if (isset($_REQUEST['campaign'])) $campaign_id = intval(base64_decode(trim(stripslashes($_REQUEST['campaign']))));
		else $campaign_id = 0;

		$campaign_details = $wpdb->get_row("SELECT t1.*, t2.total_sales, t2.total_codes FROM ".$wpdb->prefix."cs_campaigns t1 LEFT JOIN (SELECT campaign_id, SUM(sales) AS total_sales, COUNT(*) AS total_codes FROM ".$wpdb->prefix."cs_codes WHERE blocked = '0' AND deleted = '0' GROUP BY campaign_id) t2 ON t2.campaign_id = t1.id WHERE t1.deleted = '0' AND t1.blocked = '0' AND t1.id = '".$campaign_id."'", ARRAY_A);

		$errors = array();
		if (!$campaign_details || $campaign_details["price"] > 0) $errors[] = __('Invalid campaign.', 'codeshop');
		else {
			if (intval($campaign_details['total_codes']) == 0) $errors[] = $campaign_details['no_codes'];
			else if ($campaign_details['allowed_sales'] > 0) {
				$tmp = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE blocked = '0' AND deleted = '0' AND campaign_id = '".$campaign_id."' AND sales < '".$campaign_details['allowed_sales']."'", ARRAY_A);
				if (!$tmp) $errors[] = $campaign_details['no_codes'];
			}
			if (empty($email)) $errors[] = __('Your email address is required.', 'codeshop');
			else if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,19})$/i", $email)) $errors[] = __('You have entered an invalid e-mail address.', 'codeshop');
		}
		if (!empty($errors)) {
			$return_data = array();
			$return_data['status'] = 'ERROR';
			$return_data['html'] = implode('<br />', $errors);
			echo $jsonp_callback.'('.json_encode($return_data).')';
			exit;
		} else {
			$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE deleted = '0' AND blocked = '0' AND campaign_id = '".$campaign_id."'".(intval($campaign_details['allowed_sales']) > 0 ? " AND sales < '".intval($campaign_details['allowed_sales'])."'" : "")." ORDER BY sales ASC", ARRAY_A);
			if (!$code_details) {
				$status = 'No codes available';
				$code_id = 0;
			} else {
				$status = 'Completed';
				$code_id = $code_details['id'];
				$wpdb->query("UPDATE ".$wpdb->prefix."cs_codes SET sales = sales + 1 WHERE id = '".$code_id."'");
			}
			$sql = "INSERT INTO ".$wpdb->prefix."cs_transactions (
				campaign_id, code_id, payer_name, payer_email, gross, currency, payment_status, transaction_type, txn_id, details, created) VALUES (
				'".$campaign_id."',
				'".$code_id."',
				'".esc_sql($email)."',
				'".esc_sql($email)."',
				'0',
				'".$campaign_details['currency']."',
				'".$status."',
				'Free',
				'',
				'".esc_sql('type=free')."',
				'".time()."'
			)";
			$wpdb->query($sql);
			if ($status == 'Completed') {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{code}", "{transaction_date}", "{gateway}");
				$vals = array($email, $email, '0.00', $campaign_details['currency'], $campaign_details["title"], $code_details['code'], date("Y-m-d H:i:s")." (server time)", "Free");
				$body = str_replace($tags, $vals, $campaign_details['success_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) requested FREE code for "{campaign_title}" on {transaction_date}.', 'codeshop').PHP_EOL.'Received code: {code}.'.PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
				$html = '
					<div class="codeshop-row codeshop-center codeshop-margin-top-10">
						'.__('Information was sent to', 'codeshop').' <strong>'.$email.'</strong>.
					</div>';
			} else {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{payment_status}", "{transaction_date}", "{gateway}");
				$vals = array($email, $email, 0, $campaign_details['currency'], $status, date("Y-m-d H:i:s")." (server time)", "Free");
				$body = str_replace($tags, $vals, $campaign_details['failed_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) requested FREE code for "{page_title}" on {transaction_date}. The request can not be completed.', 'codeshop').PHP_EOL.__('Status: {payment_status}', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".$this->options['from_name']." <".$this->options['from_email'].">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($this->options['admin_email'], $campaign_details['title'], $body, $mail_headers);
				$html = '
					<div class="codeshop-row codeshop-center codeshop-margin-top-10">
						'.__('Information was sent to', 'codeshop').' <strong>'.$email.'</strong>.
					</div>';
			}
			$return_data = array();
			$return_data['status'] = 'OK';
			$return_data['html'] = $html;
			echo $jsonp_callback.'('.json_encode($return_data).')';
			exit;
		}
		exit;
	}

	function codeshop_getbitpayurl() {
		global $wpdb;
		if (isset($_REQUEST['callback'])) {
			header("Content-type: text/javascript");
			$jsonp_enabled = true;
			$jsonp_callback = $_REQUEST['callback'];
		} else exit;		
		if (isset($_REQUEST['email'])) $email = base64_decode(trim(stripslashes($_REQUEST['email'])));
		else $email = '';
		if (isset($_REQUEST['amount'])) $amount = floatval(base64_decode(trim(stripslashes($_REQUEST['amount']))));
		else $amount = 0;
		if (isset($_REQUEST['campaign'])) $campaign_id = intval(base64_decode(trim(stripslashes($_REQUEST['campaign']))));
		else $campaign_id = 0;
		if (isset($_REQUEST['success_url'])) $success_url = base64_decode(trim(stripslashes($_REQUEST['success_url'])));
		else $success_url = '';

		$campaign_details = $wpdb->get_row("SELECT t1.*, t2.total_sales, t2.total_codes FROM ".$wpdb->prefix."cs_campaigns t1 LEFT JOIN (SELECT campaign_id, SUM(sales) AS total_sales, COUNT(*) AS total_codes FROM ".$wpdb->prefix."cs_codes WHERE blocked = '0' AND deleted = '0' GROUP BY campaign_id) t2 ON t2.campaign_id = t1.id WHERE t1.deleted = '0' AND t1.blocked = '0' AND t1.id = '".$campaign_id."'", ARRAY_A);
                $uid=$campaign_details['user_id'];
                
		$errors = array();
		if (!$campaign_details) $errors[] = __('Invalid campaign.', 'codeshop');
		else {
			if (intval($campaign_details['total_codes']) == 0) $errors[] = $campaign_details['no_codes'];
			else if ($campaign_details['allowed_sales'] > 0) {
				$tmp = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE blocked = '0' AND deleted = '0' AND campaign_id = '".$campaign_id."' AND sales < '".$campaign_details['allowed_sales']."'", ARRAY_A);
				if (!$tmp) $errors[] = $campaign_details['no_codes'];
			}
			if (empty($email)) $errors[] = __('Your email address is required.', 'codeshop');
			else if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,19})$/i", $email)) $errors[] = __('You have entered an invalid e-mail address.', 'codeshop');
			if ($campaign_details['is_flexible'] == 1) {
				if ($amount < $campaign_details['price']) {
					$errors[] = __('You must pay at least', 'codeshop').' '.($campaign_details['currency'] == 'BTC' ? number_format($campaign_details['price'], 4, ".", "") : number_format($campaign_details['price'], 2, ".", "")).' '.$campaign_details['currency'];
				}
			}
		}
		
		if (!empty($errors)) {
			$return_data = array();
			$return_data['status'] = 'ERROR';
			$return_data['html'] = implode('<br />', $errors);
			echo $jsonp_callback.'('.json_encode($return_data).')';
			exit;
		} else {
			if (empty($success_url)) $success_url = $campaign_details['success_url'];
			if (empty($success_url)) $success_url = $_SERVER["HTTP_REFERER"];
			$amount = ($campaign_details['currency'] == 'BTC' ? number_format($campaign_details['price'], 4, ".", "") : number_format($campaign_details['price'], 2, ".", ""));
			
			$bitpay_options['orderID'] = $campaign_details['id'].'N'.time();
			$bitpay_options['itemDesc'] = $campaign_details['title'];
			$bitpay_options['itemCode'] = $campaign_details['id'];
			$bitpay_options['notificationURL'] = defined('UAP_CORE') ? admin_url('do.php').'?codeshop-ipn=bitpay' : get_bloginfo('url').'/?codeshop-ipn=bitpay';
			$bitpay_options['price'] = $amount;
			$bitpay_options['currency'] = $campaign_details['currency'];
			$bitpay_options['physical'] = 'false';
			$bitpay_options['transactionSpeed'] = get_option('codeshop_bitpay_speed','',$uid);
			$bitpay_options['fullNotifications'] = 'false';
			$bitpay_options['redirectURL'] = $success_url;
			$bitpay_options['posData'] = '{"campaign_id" : "'.$campaign_details['id'].'", "payer_email" : "'.$email.'"}';

			$post = json_encode($bitpay_options);

			//$curl = curl_init('https://bitpay.com/api/invoice/');
			$curl = curl_init('https://test.bitpay.com/api/invoice/');
			curl_setopt($curl, CURLOPT_POST, 1);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
				
			$header = array(
				'Content-Type: application/json',
				'Content-Length: '.strlen($post),
				'Authorization: Basic '.base64_encode(get_option('codeshop_bitpay_key','',$uid)),
				);

			//curl_setopt($curl, CURLOPT_PORT, 443);
			curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
			curl_setopt($curl, CURLOPT_TIMEOUT, 10);
			curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC ) ;
			//curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 1); // verify certificate
			//curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2); // check existence of CN and verify that it matches hostname
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
			curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
					
			$json = curl_exec($curl);
			curl_close($curl);

			if ($json === false) {
				$return_data = array();
				$return_data['status'] = 'ERROR';
				$return_data['html'] = __('Payment gateway is not available now (error 1). Try again later.', 'codeshop');
				echo $jsonp_callback.'('.json_encode($return_data).')';
				exit;
			}
			$post = json_decode($json, true);
			if (!$post) {
				$return_data = array();
				$return_data['status'] = 'ERROR';
				$return_data['html'] = __('Payment gateway is not available now (error 2). Try again later.', 'codeshop');
				echo $jsonp_callback.'('.json_encode($return_data).')';
				exit;
			}
			if (isset($post['error'])) {
				$return_data = array();
				$return_data['status'] = 'ERROR';
				$return_data['html'] = ucfirst($post['error']['message']);
				echo $jsonp_callback.'('.json_encode($return_data).')';
				exit;
			} else if ($post['status'] != 'new') {
				$return_data = array();
				$return_data['status'] = 'ERROR';
				$return_data['html'] = __('Payment gateway is not available now (error 3). Try again later.', 'codeshop');
				echo $jsonp_callback.'('.json_encode($return_data).')';
				exit;
			}
			$return_data = array();
			$return_data['status'] = 'OK';
			$return_data['url'] = $post['url'];
			echo $jsonp_callback.'('.json_encode($return_data).')';
			exit;
		}
		exit;
	}

	function codeshop_getblockchainaddress() {
		global $wpdb;
		if (isset($_REQUEST['callback'])) {
			header("Content-type: text/javascript");
			$jsonp_enabled = true;
			$jsonp_callback = $_REQUEST['callback'];
		} else exit;		
		if (isset($_REQUEST['email'])) $email = base64_decode(trim(stripslashes($_REQUEST['email'])));
		else $email = '';
		if (isset($_REQUEST['amount'])) $amount = floatval(base64_decode(trim(stripslashes($_REQUEST['amount']))));
		else $amount = 0;
		if (isset($_REQUEST['campaign'])) $campaign_id = intval(base64_decode(trim(stripslashes($_REQUEST['campaign']))));
		else $campaign_id = 0;

		$campaign_details = $wpdb->get_row("SELECT t1.*, t2.total_sales, t2.total_codes FROM ".$wpdb->prefix."cs_campaigns t1 LEFT JOIN (SELECT campaign_id, SUM(sales) AS total_sales, COUNT(*) AS total_codes FROM ".$wpdb->prefix."cs_codes WHERE blocked = '0' AND deleted = '0' GROUP BY campaign_id) t2 ON t2.campaign_id = t1.id WHERE t1.deleted = '0' AND t1.blocked = '0' AND t1.id = '".$campaign_id."'", ARRAY_A);
		
		$errors = array();
		if (!$campaign_details) $errors[] = __('Invalid campaign.', 'codeshop');
		else {
			if (intval($campaign_details['total_codes']) == 0) $errors[] = $campaign_details['no_codes'];
			else if ($campaign_details['allowed_sales'] > 0) {
				$tmp = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE blocked = '0' AND deleted = '0' AND campaign_id = '".$campaign_id."' AND sales < '".$campaign_details['allowed_sales']."'", ARRAY_A);
				if (!$tmp) $errors[] = $campaign_details['no_codes'];
			}
			if (empty($email)) $errors[] = __('Your email address is required.', 'codeshop');
			else if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,19})$/i", $email)) $errors[] = __('You have entered an invalid e-mail address.', 'codeshop');
			if ($amount < $campaign_details['price']) {
				$errors[] = __('You must pay at least', 'codeshop').' '.($campaign_details['currency'] == 'BTC' ? number_format($campaign_details['price'], 4, ".", "") : number_format($campaign_details['price'], 2, ".", "")).' '.$campaign_details['currency'];
			}
		}
		
		if (!empty($errors)) {
			$return_data = array();
			$return_data['status'] = 'ERROR';
			$return_data['html'] = implode('<br />', $errors);
			echo $jsonp_callback.'('.json_encode($return_data).')';
			exit;
		} else {
			$btc_amount = $amount;
			if ($campaign_details['currency'] != 'BTC') {
				$curl = curl_init('https://blockchain.info/tobtc?currency='.$campaign_details['currency'].'&value='.number_format($amount, 2, ".", ""));
				curl_setopt($curl, CURLOPT_POST, 0);
				curl_setopt($curl, CURLOPT_TIMEOUT, 10);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
				curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
				$btc_amount = curl_exec($curl);
				curl_close($curl);
				if (is_numeric($btc_amount)) {
					$btc_amount = number_format(floatval($btc_amount), 4, ".", "");
				}
				if ($btc_amount == 0) {
					$return_data = array();
					$return_data['status'] = 'ERROR';
					$return_data['html'] = __('Unable to convert amount into BTC. Try again later.', 'codeshop');
					echo $jsonp_callback.'('.json_encode($return_data).')';
					exit;
				}
			}
			$callback_base = defined('UAP_CORE') ? admin_url('do.php') : get_bloginfo('url').'/';
			$url = 'https://blockchain.info/api/receive?method=create&address='.$this->options['blockchain_address'].'&callback='.urlencode($callback_base.'?codeshop-ipn=blockchain&campaign_id='.$campaign_id.'&email='.base64_encode($email).'&btc_amount='.number_format($btc_amount, 4, ".", "").'&amount='.number_format($btc_amount, 4, ".", "").'&secret='.$this->options['blockchain_secret']);
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_POST, 0);
			curl_setopt($curl, CURLOPT_TIMEOUT, 10);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
			curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
				
			$json = curl_exec($curl);
			curl_close($curl);

			if ($json === false) {
				$return_data = array();
				$return_data['status'] = 'ERROR';
				$return_data['html'] = __('Payment gateway is not available now (error 1). Try again later.', 'codeshop');
				echo $jsonp_callback.'('.json_encode($return_data).')';
				exit;
			}
			$post = json_decode($json, true);
			if (!$post) {
				$return_data = array();
				$return_data['status'] = 'ERROR';
				$return_data['html'] = __('Payment gateway is not available now (error 2). Try again later.', 'codeshop');
				echo $jsonp_callback.'('.json_encode($return_data).')';
				exit;
			}
			if (!array_key_exists('input_address', $post)) {
				$return_data = array();
				$return_data['status'] = 'ERROR';
				$return_data['html'] = ucfirst($post['error']['message']);
				echo $jsonp_callback.'('.json_encode($return_data).')';
				exit;
			}
			$return_data = array();
			$return_data['status'] = 'OK';
			$return_data['html'] = '
					<div class="codeshop-row codeshop-center codeshop-margin-top-10">
						'.__('Please send', 'codeshop').' <strong>'.number_format($btc_amount, 4, ".", "").' BTC</strong> '.__('to address', 'codeshop').' <strong>'.$post['input_address'].'</strong> '.__('to complete transaction', 'codeshop').'.
					</div>';
			echo $jsonp_callback.'('.json_encode($return_data).')';
			exit;
		}
		exit;
	}
	
	function codeshop_charge() {
		global $wpdb;
                $uid=0;
		if (isset($_REQUEST['callback'])) {
			header("Content-type: text/javascript");
			$jsonp_enabled = true;
			$jsonp_callback = $_REQUEST['callback'];
		} else exit;
                
		if (isset($_REQUEST['uid'])){
                    $uid = base64_decode(trim(stripslashes($_REQUEST['uid'])));
                    $this->user_get_options($uid);
                }else{
                    echo 'user not found!';
                    exit;
                }
		if (isset($_REQUEST['email'])) $email = base64_decode(trim(stripslashes($_REQUEST['email'])));
		else $email = '';
		if (isset($_REQUEST['token'])) $token = base64_decode(trim(stripslashes($_REQUEST['token'])));
		else $token = '';
		if (isset($_REQUEST['amount'])) $amount = floatval(base64_decode(trim(stripslashes($_REQUEST['amount']))));
		else $amount = 0;
		if (isset($_REQUEST['campaign'])) $campaign_id = intval(base64_decode(trim(stripslashes($_REQUEST['campaign']))));
		else $campaign_id = 0;
		
		$campaign_details = $wpdb->get_row("SELECT t1.*, t2.total_sales, t2.total_codes FROM ".$wpdb->prefix."cs_campaigns t1 LEFT JOIN (SELECT campaign_id, SUM(sales) AS total_sales, COUNT(*) AS total_codes FROM ".$wpdb->prefix."cs_codes WHERE blocked = '0' AND deleted = '0' GROUP BY campaign_id) t2 ON t2.campaign_id = t1.id WHERE t1.deleted = '0' AND t1.blocked = '0' AND t1.id = '".$campaign_id."'", ARRAY_A);

		$errors = array();
		if (!$campaign_details) $errors[] = __('Invalid campaign.', 'codeshop');
		else {
			if (intval($campaign_details['total_codes']) == 0) $errors[] = $campaign_details['no_codes'];
			else if ($campaign_details['allowed_sales'] > 0) {
				$tmp = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE blocked = '0' AND deleted = '0' AND campaign_id = '".$campaign_id."' AND sales < '".$campaign_details['allowed_sales']."'", ARRAY_A);
				if (!$tmp) $errors[] = $campaign_details['no_codes'];
			}
			if (empty($email)) $errors[] = __('Your email address is required.', 'codeshop');
			else if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,19})$/i", $email)) $errors[] = __('You have entered an invalid e-mail address.', 'codeshop');
			if ($campaign_details['is_flexible'] == 1) {
				if ($amount < $campaign_details['price']) {
					$errors[] = __('You must pay at least', 'codeshop').' '.number_format($campaign_details['price'], 2, ".", "").' '.$campaign_details['currency'];
				}
			}
		}
		
		if (!empty($errors)) {
			$return_data = array();
			$return_data['status'] = 'ERROR';
			$return_data['html'] = implode('<br />', $errors);
			echo $jsonp_callback.'('.json_encode($return_data).')';
			exit;
		}

		require_once(dirname(__FILE__).'/lib/Stripe.php');
		error_reporting(0);

		try {
			Stripe::setApiKey($this->options['stripe_secret']);
					
			$charge = Stripe_Charge::create(array(
				"amount" => intval($amount*100),
				"currency" => $campaign_details['currency'],
				"card" => $token,
				"description" => $email)
			);

			$post = json_decode($charge, true);
					
			$response = 'token='.$token;
			foreach ($post as $key => $value) {
				if (is_array($value)) $response .= "&".$key."=".urlencode(str_replace('&', ' ', serialize($value)));
				else $response .= "&".$key."=".urlencode($value);
			}

			$code_details = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."cs_codes WHERE deleted = '0' AND blocked = '0' AND campaign_id = '".$campaign_id."'".(intval($campaign_details['allowed_sales']) > 0 ? " AND sales < '".intval($campaign_details['allowed_sales'])."'" : "")." ORDER BY sales ASC", ARRAY_A);
			if (!$code_details) {
				$status = 'No codes available';
				$code_id = 0;
			} else {
				$status = 'Completed';
				$code_id = $code_details['id'];
				$wpdb->query("UPDATE ".$wpdb->prefix."cs_codes SET sales = sales + 1 WHERE id = '".$code_id."'");
			}

			$sql = "INSERT INTO ".$wpdb->prefix."cs_transactions (
				user_id,campaign_id, code_id, payer_name, payer_email, gross, currency, payment_status, transaction_type, txn_id, details, created) VALUES (
				'".$uid."',
				'".$campaign_id."',
				'".$code_id."',
				'".esc_sql($post['card']['name'])."',
				'".esc_sql($email)."',
				'".floatval($amount)."',
				'".$campaign_details['currency']."',
				'".$status."',
				'Stripe: ".$post['card']['type']."',
				'".$token."',
				'".esc_sql($response)."',
				'".time()."'
			)";
			$wpdb->query($sql);
					
			if ($status == 'Completed') {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{campaign_title}", "{code}", "{transaction_date}", "{gateway}");
				$vals = array($post['card']['name'], $email, $amount, $campaign_details['currency'], $campaign_details["title"], $code_details['code'], date("Y-m-d H:i:s")." (server time)", "Stripe");
				$body = str_replace($tags, $vals, $campaign_details['success_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".get_option('codeshop_from_name','',$uid)." <".get_option('codeshop_from_email','',$uid).">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" using {gateway} on {transaction_date}.', 'codeshop').PHP_EOL.'Received code: {code}.'.PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".get_option('codeshop_from_name','',$uid)." <".get_option('codeshop_from_email','',$uid).">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail(get_option('codeshop_admin_email','',$uid), $campaign_details['title'], $body, $mail_headers);
				$html = '
					<div class="codeshop-row codeshop-center codeshop-margin-top-10">
						'.__('Payment successfully <strong>completed</strong>. Information was sent to', 'codeshop').' <strong>'.$email.'</strong>.
					</div>';
			} else {
				$tags = array("{payer_name}", "{payer_email}", "{amount}", "{currency}", "{payment_status}", "{transaction_date}", "{gateway}");
				$vals = array($post['card']['name'], $email, $amount, $campaign_details['currency'], $status, date("Y-m-d H:i:s")." (server time)", "Stripe");
				$body = str_replace($tags, $vals, $campaign_details['failed_email']);
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".get_option('codeshop_from_name','',$uid)." <".get_option('codeshop_from_email','',$uid).">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail($email, $campaign_details['title'], $body, $mail_headers);
				$body = str_replace($tags, $vals, __('Dear Administrator!', 'codeshop').PHP_EOL.PHP_EOL.__('We would like to inform you that {payer_name} ({payer_email}) paid {amount} {currency} for "{campaign_title}" using {gateway} on {transaction_date}. The request can not be completed.', 'codeshop').PHP_EOL.__('Status: {payment_status}', 'codeshop').PHP_EOL.PHP_EOL.__('Thanks,', 'codeshop').PHP_EOL.'Code Shop');
				$mail_headers = "Content-Type: text/plain; charset=utf-8\r\n";
				$mail_headers .= "From: ".get_option('codeshop_from_name','',$uid)." <".get_option('codeshop_from_email','',$uid).">\r\n";
				$mail_headers .= "X-Mailer: PHP/".phpversion()."\r\n";
				wp_mail(get_option('codeshop_admin_email','',$uid), $campaign_details['title'], $body, $mail_headers);
				$html = '
					<div class="codeshop-row codeshop-center codeshop-margin-top-10">
						'.__('Information was sent to', 'codeshop').' <strong>'.$email.'</strong>.
					</div>';
			}
			$return_data = array();
			$return_data['status'] = 'OK';
			$return_data['html'] = $html;
			echo $jsonp_callback.'('.json_encode($return_data).')';
			exit;
		} catch(Exception $e) {
			$body = $e->getJsonBody();
			$err  = $body['error'];
			$return_data = array();
			$return_data['status'] = 'ERROR';
			$return_data['html'] = $err['message'];
			echo $jsonp_callback.'('.json_encode($return_data).')';
			exit;
		}
		exit;
	}

	function remote_init() {
		global $wpdb;
		if (isset($_REQUEST['callback'])) {
			header("Content-type: text/javascript");
			$jsonp_callback = $_REQUEST['callback'];
		} else die("JSONP is not supported!");
                
		$return_data = array();
		$return_data['status'] = 'OK';
		$return_data['resources']['stripe'] = 'on';
		if (isset($_REQUEST['data'])) {
			$items = json_decode(base64_decode(trim(stripslashes($_REQUEST['data']))), true);
			if (sizeof($items) > 0) {                            
				foreach($items as $item) {
                                $uid=$item['uid'];
                                $stripekey=get_option('codeshop_stripe_publishable','',$uid);
                                $return_data['stripe_publishable'] = $stripekey;
                                
					if (!empty($item)) {
						$atts = array('id' => $item['id'],'uid'=>$item['uid']);
						if (array_key_exists('success_url', $item)) $atts['success_url'] = $item['success_url'];
						if (array_key_exists('failed_url', $item)) $atts['failed_url'] = $item['failed_url'];
						if (array_key_exists('intro', $item)) $atts['title'] = $item['intro'];

						$return_data['forms'][$item['html_id']] = $this->shortcode_handler($atts);
					}
				}
			}
		}
		
		echo $jsonp_callback.'('.json_encode($return_data).')';
		exit;
	}
	
	function random_string($_length = 16) {
		$symbols = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$str_id = "";
		for ($i=0; $i<$_length; $i++) {
			$str_id .= $symbols[rand(0, strlen($symbols)-1)];
		}
		return $str_id;
	}

	function page_switcher ($_urlbase, $_currentpage, $_totalpages) {
		$pageswitcher = "";
		if ($_totalpages > 1) {
			$pageswitcher = '<div class="tablenav bottom"><div class="tablenav-pages">'.__('Pages:', 'codeshop').' <span class="pagiation-links">';
			if (strpos($_urlbase,"?") !== false) $_urlbase .= "&amp;";
			else $_urlbase .= "?";
			if ($_currentpage == 1) $pageswitcher .= "<a class='page disabled'>1</a> ";
			else $pageswitcher .= " <a class='page' href='".$_urlbase."p=1'>1</a> ";

			$start = max($_currentpage-3, 2);
			$end = min(max($_currentpage+3,$start+6), $_totalpages-1);
			$start = max(min($start,$end-6), 2);
			if ($start > 2) $pageswitcher .= " <b>...</b> ";
			for ($i=$start; $i<=$end; $i++) {
				if ($_currentpage == $i) $pageswitcher .= " <a class='page disabled'>".$i."</a> ";
				else $pageswitcher .= " <a class='page' href='".$_urlbase."p=".$i."'>".$i."</a> ";
			}
			if ($end < $_totalpages-1) $pageswitcher .= " <b>...</b> ";

			if ($_currentpage == $_totalpages) $pageswitcher .= " <a class='page disabled'>".$_totalpages."</a> ";
			else $pageswitcher .= " <a class='page' href='".$_urlbase."p=".$_totalpages."'>".$_totalpages."</a> ";
			$pageswitcher .= "</span></div></div>";
		}
		return $pageswitcher;
	}
	
	function get_fingerprint($api_login_id, $transaction_key, $amount, $fp_sequence, $fp_timestamp) {
        if (function_exists('hash_hmac')) {
            return hash_hmac("md5", $api_login_id . "^" . $fp_sequence . "^" . $fp_timestamp . "^" . $amount . "^", $transaction_key); 
        }
        return bin2hex(mhash(MHASH_MD5, $api_login_id . "^" . $fp_sequence . "^" . $fp_timestamp . "^" . $amount . "^", $transaction_key));
    }
}
$codeshop = new codeshop_class();
?>