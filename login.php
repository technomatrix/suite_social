<?php
include_once(dirname(__FILE__) . '/inc/common.php');
include_once(dirname(__FILE__) . '/inc/icdb.php');
include_once(dirname(__FILE__) . '/inc/functions.php');
$wpdb = null;
$ready = false;

if (file_exists(dirname(__FILE__) . '/inc/config.php')) {
    include_once(dirname(__FILE__) . '/inc/config.php');
    try {
        $wpdb = new ICDB(UAP_DB_HOST, UAP_DB_HOST_PORT, UAP_DB_NAME, UAP_DB_USER, UAP_DB_PASSWORD, UAP_TABLE_PREFIX);
        create_tables();
        get_options();
        if (!empty($options['login']) && !empty($options['password']) && !empty($options['url']))
            $ready = true;
    } catch (Exception $e) {
        //die($e->getMessage());
    }
}
if (!$ready) {
    if (isset($_POST['action'])) {
        $return_object = array();
        $return_object['status'] = 'ERROR';
        $return_object['message'] = 'Please install Admin Panel properly.';
        echo json_encode($return_object);
        exit;
    }
    //header('Location: '.admin_url('install.php'));
    exit;
}
$is_logged = false;
$session_id = '';
if (isset($_COOKIE['uap-auth'])) {
    $session_id = preg_replace('/[^a-zA-Z0-9]/', '', $_COOKIE['uap-auth']);
    $session_details = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "sessions WHERE session_id = '" . $session_id . "' AND registered + valid_period > '" . time() . "'");
    if ($session_details) {
        $wpdb->query("UPDATE " . $wpdb->prefix . "sessions SET registered = '" . time() . "', ip = '" . $_SERVER['REMOTE_ADDR'] . "' WHERE session_id = '" . $session_id . "'");
        $is_logged = true;
    }
}
if (isset($_GET['logout'])) {
    session_destroy();
    $is_logged = false;
}
if ($is_logged === true) {
    if (isset($_GET['logout'])) {
        echo 'testtt';
        if (!empty($session_id)) {
            //$wpdb->query("UPDATE ".$wpdb->prefix."sessions SET valid_period = '0' WHERE session_id = '".$session_id."'");
        }
        $is_logged = false;
    } else if (isset($_POST['action'])) {
        $return_object = array();
        $return_object['status'] = 'OK';
        $return_object['url'] = admin_url('admin.php');
        echo json_encode($return_object);
        exit;
    } else {
        //header('Location: '.admin_url('admin.php'));
        //exit;
    }
}
if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'login':
            if (isset($_POST['password']))
                $password = trim(stripslashes($_POST['password']));
            else
                $password = '';
            if (isset($_POST['login']))
                $login = trim(stripslashes($_POST['login']));
            else
                $login = '';
            $return_object = array();
            if ($login == $options['login'] && md5($password) == $options['password']) {
                $session_id = random_string(16);
                $wpdb->query("INSERT INTO " . $wpdb->prefix . "sessions (ip, session_id, registered, valid_period) VALUES ('" . $_SERVER['REMOTE_ADDR'] . "', '" . $session_id . "', '" . time() . "', '7200')");
                setcookie('uap-auth', $session_id, time() + 3600 * 24 * 180);
                $_SESSION['ok'] = 'Welcome to admin panel!' . (UAP_DEMO_MODE ? ' Admin Panel operates in <strong>demo mode</strong> for security reasons.' : '');
                $return_object['status'] = 'OK';
                $return_object['url'] = admin_url('admin.php');
            } else {
                $return_object['status'] = 'ERROR';
                $return_object['message'] = 'Invalid email or password!';
            }

            echo json_encode($return_object);
            exit;
            break;

        case 'reset-password':
            if (isset($_POST['login']))
                $login = strtolower(trim(stripslashes($_POST['login'])));
            else
                $login = '';
            if ($login != $options['login']) {
                $return_object = array();
                $return_object['status'] = 'ERROR';
                $return_object['message'] = 'Invalid e-mail address.';
                echo json_encode($return_object);
                exit;
            }
            $new_password = random_string(12);
            $wpdb->query("UPDATE " . $wpdb->prefix . "options SET options_value = '" . $wpdb->escape_string(md5($new_password)) . "' WHERE options_key = 'password'");
            $message = 'Hi ' . $login . ',<br /><br />I\'ve requested new password to access Admin Panel. Here it is:<br /><br />' . $new_password . '<br /><br />Regards,<br />Admin Panel';
            if (wp_mail($login, 'New password for Admin Panel', $message)) {
                $return_object = array();
                $return_object['status'] = 'OK';
                $return_object['html'] = 'E-mail with new password for Admin Panel has been sent successfully. Check your inbox and <a class="switch-to-login" href="#" onclick="return switch_reset();">enter Admin Panel</a>.';
                echo json_encode($return_object);
                exit;
            } else {
                $return_object = array();
                $return_object['status'] = 'ERROR';
                $return_object['message'] = 'Hm. Something went wrong. We couldn\'t send e-mail with new password.';
                echo json_encode($return_object);
                exit;
            }
            break;

        default:
            echo 'We don\'t have to be here. Never.';
            exit;
    }
}
include_once(dirname(__FILE__) . '/inc/login_header.php');
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="css/tm_style.css">
    <title>Suite Social</title>
</head>
<body>
<div id="logreg-forms">
    <form class="form-signin">
        <h1 class="h3 mb-3 font-weight-normal" style="text-align: center"> Sign in</h1>
        <div class="social-login">
            <button class="btn facebook-btn social-btn" type="button"><span><i class="fab fa-facebook-f"></i> Sign in with Facebook</span> </button>
            <button class="btn google-btn social-btn" type="button"><span><i class="fab fa-google-plus-g"></i> Sign in with Google+</span> </button>
        </div>
        <p style="text-align:center"> OR  </p>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required="" autofocus="">
        <input type="password" id="inputPassword" class="form-control" placeholder="Password" required="">

        <button class="btn btn-success btn-block" type="submit"><i class="fas fa-sign-in-alt"></i> Sign in</button>
        <a href="#" id="forgot_pswd">Forgot password?</a>
        <hr>
        <!-- <p>Don't have an account!</p>  -->
        <button class="btn btn-primary btn-block" type="button" id="btn-signup"><i class="fas fa-user-plus"></i> Sign up New Account</button>
    </form>

    <form action="/reset/password/" class="form-reset">
        <input type="email" id="resetEmail" class="form-control" placeholder="Email address" required="" autofocus="">
        <button class="btn btn-primary btn-block" type="submit">Reset Password</button>
        <a href="#" id="cancel_reset"><i class="fas fa-angle-left"></i> Back</a>
    </form>

    <form id="signupForm"  class="form-signup">
        <div class="social-login">
            <button class="btn facebook-btn social-btn" type="button"><span><i class="fab fa-facebook-f"></i> Sign up with Facebook</span> </button>
        </div>
        <div class="social-login">
            <button class="btn google-btn social-btn" type="button"><span><i class="fab fa-google-plus-g"></i> Sign up with Google+</span> </button>
        </div>

        <p style="text-align:center">OR</p>

        <input value="asd" type="text" id="user-name" class="form-control" placeholder="Full name" required="" autofocus="">
        <input value="asd@a.com" type="email" id="user-email" class="form-control" placeholder="Email address" required autofocus="">
        <input value="asd" type="password" id="user-pass" class="form-control" placeholder="Password" required autofocus="">
        <input value="asd" type="password" id="user-repeatpass" class="form-control" placeholder="Repeat Password" required autofocus="">

        <input class="btn btn-primary btn-block signup" value="Sign Up" type="button"> </input>
        <a href="#" id="cancel_signup"><i class="fas fa-angle-left"></i> Back</a>
    </form>
    <br>

</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
 </body>


<!--<br>
<br>
<p align="center"><a href="//suite.social/training" target="_blank"><img style="padding:3px; border:3px solid #8ec657;background-color:#1f1f1f" src="//suite.social/images/ad/training.jpg" alt="Advert"></a></p>	
<br>
<br>-->

<?php
include_once(dirname(__FILE__) . '/inc/login_footer.php');
?>
